package com.glam.utils;

import android.content.Context;

import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.ObjectData;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.transport.ImagesArrayResponseData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.NativeImage;

public class ImageCache {

    public static ImageCache instance = null;

    public static ImageCache getInstance(Context context) {
        if (instance == null) {
            instance = new ImageCache(context);
        }
        return instance;
    }

    private int cacheSize = 80;
    private OfertaDigitalDataSource dao;


    public ImageCache(Context context) {
        this.dao = new OfertaDigitalDataSource(context);
    }

    public ImageCache(OfertaDigitalDataSource dao) {
        this.dao = dao;
    }

    public void releaseCacheSize() throws Exception {
        int counter = this.dao.countObjects(NativeImage.class);
        if (counter > cacheSize) {
            List<String> imagesIds = this.dao.loadImages((int) cacheSize / 2);
            for (String id : imagesIds) {
                this.dao.deleteObject(id);
            }
        }

    }

    public void cacheImage(NativeImage nativeImage) throws Exception {
       // this.releaseCacheSize();

        if (this.dao.loadObject(nativeImage.getId(), nativeImage.getClass()) == null) {
            this.dao.persitImage(nativeImage.getId(), nativeImage.getSourcestream());
            MyLogger.info("persist image: " + nativeImage.getId());
        }
    }

    public void downloadImages(List<String> nativeImageIds) throws Exception {

    }

    public boolean isCached(String nativeImageId) {
        try {
            return this.dao.existRegisterWithId(nativeImageId);
        } catch (Exception e) {
            MyLogger.log(e);
        }

        return false;
    }

    public byte[] getImage(String nativeImageId) {
        try {
            return this.dao.loadImage(nativeImageId);
        } catch (Exception e) {
            MyLogger.log(e);
        }
        return null;
    }

    private boolean mustBeDownload(MediaDesc media) {
        if (MediaDesc.MEDIA_TYPE.IMAGE.equals(media.getType()) &&
                media.getNativeimage_id() != null &&
                !this.isCached(media.getNativeimage_id())) {
            return true;
        }

        return false;
    }

    public void requestImages(List<MediaDesc> mediasList) throws Exception {
        MyLogger.info("start requestImages");
        List<String> nativeImagesIds = new ArrayList<>();

        for (MediaDesc media : mediasList) {
            if (this.mustBeDownload(media) && !nativeImagesIds.contains(media.getNativeimage_id())) {
                nativeImagesIds.add(media.getNativeimage_id());
            }
        }


        ImagesArrayResponseData images = OfertaDigitalRestClient.getRef().requestImages(nativeImagesIds);
        Collection<NativeImage> nativeImages = images.getImages().values();
        for (NativeImage nativeImage : nativeImages) {
            MyLogger.info("caching: " + nativeImage.getId() + " bytes: " + nativeImage.getSourcestream().length);
            this.cacheImage(nativeImage);
        }

        MyLogger.info("end called requestImages");
    }
}
