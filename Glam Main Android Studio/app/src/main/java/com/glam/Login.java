package com.glam;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.glam.custom.CustomActivity;

import com.glam.dialog.MyDialogs;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;
import com.glam.http.OfertaDigitalRestClient;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.UUID;

import br.com.innove.ofertadigital.persistence.NativeImage;
import br.com.innove.ofertadigital.rest.transport.LoginResponseData;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONObject;


/**
 * The Activity Login is launched after the Home screen. You need to write your
 * logic for actual Login. You also need to implement Facebook Login if
 * required.
 */
public class Login extends CustomActivity {

    private OfertaDigitalDataSource dao;

    /**
     * The pager.
     */
    private ViewPager pager;

    private MainActivity s;
    private CallbackManager callbackManager;

    /**
     * The view that hold dots.
     */
    private LinearLayout vDots;
    private EditText loginText;

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // FacebookSdk.sdkInitialize(this.getApplicationContext());

        setContentView(R.layout.login);

        setupView();

        this.dao = new OfertaDigitalDataSource(this);

        final EditText loginText = (EditText) this.findViewById(R.id.loginText1);
        final EditText passwordText = (EditText) this.findViewById(R.id.passwordText2);

        loginText.setCursorVisible(true);
        passwordText.setCursorVisible(true);

        Button btnLogin = (Button) this.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // test
                //   if ("marcelo@gmail.com".equals(loginText.getText().toString()) &&
                //        "abc123".equals(passwordText.getText().toString())) {

                final String username = loginText.getText().toString();
                final String password = passwordText.getText().toString();


                AsyncTask<Void, Void, Void> loginTask = new AsyncTask<Void, Void, Void>() {
                    private Exception e;

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {

                            OfertaDigitalRestClient client = OfertaDigitalRestClient.getRef();
                            LoginResponseData loginresponse = client.autenthicate(username, password, "serradalto", null);
                            Login.this.dao.persitObject("loginfo", loginresponse);
                            Login.this.dao.persitObject("pwdinfo", password);
                            LoginResponseData response = Login.this.dao.loadObject("loginfo", LoginResponseData.class);
                            MyLogger.info("token deszerialized: " + response.getToken());
                        } catch (Exception e) {
                            this.e = e;
                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {

                        if (this.e != null) {
                            MyLogger.log(this.e);
                            MyDialogs.showError(Login.this, "Erro no Login", this.e.getLocalizedMessage());
                        } else {
                           /* Intent i = new Intent(Login.this, MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();*/

                            Intent i = new Intent(Login.this, StoreSelectActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();

                        }

                    }
                };

                loginTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                // } else {
                //     MyDialogs.showError(Login.this, "Erro no Login", "Usuario ou senha não confere");
                // }
            }
        });


        Button btnFg = (Button) this.findViewById(R.id.btnFb);
        btnFg.setOnClickListener(new View.OnClickListener()

        {


            @Override
            public void onClick(View view) {
            }
        });

        LoginButton loginButton = (LoginButton) this.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));

        this.callbackManager = CallbackManager.Factory.create();

        // Callback registration
        loginButton.registerCallback(this.callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                MyLogger.info("onSuccess: " + loginResult);

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            private ProfileTracker mProfileTracker;

                            @Override
                            public void onCompleted(
                                    final JSONObject object,
                                    final GraphResponse response) {

                                try {
                                    // Application code
                                    MyLogger.info("json: " + object);
                                    MyLogger.info("response: " + response.getRawResponse());
                                    MyLogger.info("permissions: " + AccessToken.getCurrentAccessToken().getPermissions());
                                    MyLogger.info("profile: " + Profile.getCurrentProfile());


                                    if (Profile.getCurrentProfile() == null) {
                                        mProfileTracker = new ProfileTracker() {
                                            @Override
                                            protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                                                makeFBLogin(profile2, object);
                                                mProfileTracker.stopTracking();
                                            }
                                        };

                                        mProfileTracker.startTracking();
                                        MyLogger.info("profile tracker started");
                                    } else {
                                        makeFBLogin(Profile.getCurrentProfile(), object);
                                    }
                                } catch (Exception e) {
                                    MyLogger.log(e);
                                }
                            }

                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                MyLogger.info("onCancel");
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                MyLogger.log(exception);
            }
        });


        Button btnReg = (Button) this.findViewById(R.id.btnReg);
        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, UserRegisterActivity.class);
              //  i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
               // finish();
            }
        });

        CardView cardMessage = (CardView) this.findViewById(R.id.first_message);
        ImageView imageMessage = (ImageView) this.findViewById(R.id.first_image);



    }

    public void makeFBLogin(Profile profile2, JSONObject object) {
        try {

            MyLogger.info("profile tracker onCurrentProfileChanged: " + profile2);

            final String username = (object.isNull("email") ? object.getString("id") : object.getString("email"));

            final String id = object.getString("id");
            final String store_id = "serradalto";
            final Hashtable<String, String> fbInfo = new Hashtable<String, String>();
            fbInfo.put("object", object.toString());
            fbInfo.put("name", profile2.getName());

            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                public Exception e;

                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        OfertaDigitalRestClient.getRef().autenthicateWithFB(username, store_id, AccessToken.getCurrentAccessToken().getToken(), fbInfo);
                    } catch (Exception e) {
                        MyLogger.log(e);
                        this.e = e;
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {

                    if (this.e != null) {
                        MyLogger.log(this.e);
                        MyDialogs.showError(Login.this, "Erro no Login", this.e.getLocalizedMessage());
                    } else {
                        Intent i = new Intent(Login.this, StoreSelectActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    }

                }
            };
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        } catch (Exception e) {
            MyLogger.log(e);
        }

    }


    /**
     * Setup the click & other events listeners for the view components of this
     * screen. You can add your logic for Binding the data to TextViews and
     * other views as per your need.
     */

    private void setupView() {
        Button b = (Button) setTouchNClick(R.id.btnReg);
        b.setText(Html.fromHtml(getString(R.string.sign_up)));

        setTouchNClick(R.id.btnLogin);
        setTouchNClick(R.id.btnForget);
        setTouchNClick(R.id.btnFb);

        // initPager();
    }

    /**
     * Inits the pager view.
     */
    private void initPager() {
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setPageMargin(10);

        pager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                if (vDots == null || vDots.getTag() == null)
                    return;
                ((ImageView) vDots.getTag())
                        .setImageResource(R.drawable.dot_gray);
                ((ImageView) vDots.getChildAt(pos))
                        .setImageResource(R.drawable.dot_blue);
                vDots.setTag(vDots.getChildAt(pos));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        vDots = (LinearLayout) findViewById(R.id.vDots);

        pager.setAdapter(new PageAdapter());
        setupDotbar();
    }

    /**
     * Setup the dotbar to show dots for pages of view pager with one dot as
     * selected to represent current page position.
     */
    private void setupDotbar() {

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        param.setMargins(10, 0, 0, 0);
        vDots.removeAllViews();
        for (int i = 0; i < 5; i++) {
            ImageView img = new ImageView(this);
            img.setImageResource(i == 0 ? R.drawable.dot_blue
                    : R.drawable.dot_gray);
            vDots.addView(img, param);
            if (i == 0) {
                vDots.setTag(img);
            }

        }
    }

    @Override
    protected void onDestroy() {
        MyLogger.info("onDestroy: " + this);
        super.onDestroy();
        this.pager = null;

        try {
            this.dao.close();
            this.dao = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }


    private class PageAdapter extends PagerAdapter {

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#getCount()
         */
        @Override
        public int getCount() {
            return 5;
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#instantiateItem(android.view.ViewGroup, int)
         */
        @Override
        public Object instantiateItem(ViewGroup container, int arg0) {
            final ImageView img = (ImageView) getLayoutInflater().inflate(
                    R.layout.img, null);

            img.setImageResource(R.drawable.oferta_digital_advertisment_1);

            container.addView(img,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT);
            return img;
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#destroyItem(android.view.ViewGroup, int, java.lang.Object)
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            try {
                // super.destroyItem(container, position, object);
                // if(container.getChildAt(position)!=null)
                // container.removeViewAt(position);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#isViewFromObject(android.view.View, java.lang.Object)
         */
        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

    }

    /* (non-Javadoc)
     * @see com.taxi.custom.CustomActivity#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        MyLogger.info("onClick: " + v.getId());
        super.onClick(v);
        if (v.getId() == R.id.btnLogin || v.getId() == R.id.btnFb) {
            Intent i = new Intent(this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MyLogger.info("requestCode: " + requestCode + " data: " + data);
        this.callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}
