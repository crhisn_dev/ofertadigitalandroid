package com.glam;

import java.util.ArrayList;
import java.util.List;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.glam.custom.CustomActivity;
import com.glam.custom.CustomFragment;
import com.glam.dialog.StoreSelectDialog;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.model.Data;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.services.RegistrationIntentService;
import com.glam.transport.OfferStoreInfo;
import com.glam.ui.BuyListFragment;
import com.glam.ui.Checkout;
import com.glam.ui.DeliveryAddressFragment;
import com.glam.ui.DeliveryInfoFragment;
import com.glam.ui.JornalImageFragment;
import com.glam.ui.LeftNavAdapter;
import com.glam.ui.MainFragment;
import com.glam.ui.OfferProductFragment;
import com.glam.ui.OnSale;
import com.glam.ui.OrderListFragment;
import com.glam.ui.RecipentInfoFragment;
import com.glam.ui.ShowCategoriesFragment;
import com.glam.ui.ShowCategoriesHorizontalFragment;
import com.glam.ui.VideosFragment;
import com.glam.utils.MyLogger;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import br.com.innove.ofertadigital.persistence.JornalStoreInfo;
import br.com.innove.ofertadigital.persistence.StoreInfo;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.youtube.player.internal.d;

import static com.glam.R.id.back_btnMenuProduct;


/**
 * The Activity MainActivity will launched after the Login and it is the
 * Home/Base activity of the app which holds all the Fragments and also show the
 * Sliding Navigation drawer. You can write your code for displaying actual
 * items on Drawer layout.
 */
@SuppressLint("InlinedApi")
public class MainActivity extends CustomActivity {

    /**
     * The drawer layout.
     */
    private DrawerLayout drawerLayout;


    /**
     * ListView for left side drawer.
     */
    private ListView drawerLeft;

    /**
     * The drawer toggle.
     */
    private ActionBarDrawerToggle drawerToggle;

    /**
     * The m action bar auto hide enabled.
     */
    private boolean mActionBarAutoHideEnabled = true;

    /**
     * The m action bar shown.
     */
    private boolean mActionBarShown = true;

    /**
     * The m status bar color animator.
     */
    private ObjectAnimator mStatusBarColorAnimator;

    /**
     * The Constant ARGB_EVALUATOR.
     */
    @SuppressWarnings("rawtypes")
    private static final TypeEvaluator ARGB_EVALUATOR = new ArgbEvaluator();

    /**
     * The m action bar auto hide sensivity.
     */
    private int mActionBarAutoHideSensivity = 0;

    /**
     * The m action bar auto hide min y.
     */
    private int mActionBarAutoHideMinY = 0;

    /**
     * The m action bar auto hide signal.
     */
    private int mActionBarAutoHideSignal = 0;

    /**
     * The toolbar.
     */
    public Toolbar toolbar;
    private LeftNavAdapter adp;

    private StoreInfo currentStore;
    private LinearLayout back_btnMenuProduct;
    private LinearLayout back_btnMenuLista;
    private LinearLayout back_btnMenuPedidos;
    private LinearLayout back_btnMenuJornal;
    private LinearLayout back_btnMenuOfertas;



    private void allBlack(){
        this.back_btnMenuProduct.setBackgroundColor(getResources().getColor(R.color.black));
        this.back_btnMenuLista.setBackgroundColor(getResources().getColor(R.color.black));
        this.back_btnMenuPedidos.setBackgroundColor(getResources().getColor(R.color.black));
        this.back_btnMenuJornal.setBackgroundColor(getResources().getColor(R.color.black));
        this.back_btnMenuOfertas.setBackgroundColor(getResources().getColor(R.color.black));

    }
    /* (non-Javadoc)
     * @see com.newsfeeder.custom.CustomActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("storeinfo")) {
            this.currentStore = (StoreInfo) intent.getSerializableExtra("storeinfo");

            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        OfertaDigitalRestClient.getRef().registerStoreInfo(currentStore);
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }
                    return null;
                }
            };
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        int fragmentPosition = 1;
        if (intent.hasExtra("promotions")) {
            MyLogger.info("promotions: " + intent.getSerializableExtra("promotions"));
            fragmentPosition = 4;
        }

        if (intent.hasExtra("order_status")) {
            MyLogger.info("order_status: " + intent.getSerializableExtra("order_status"));
            fragmentPosition = 8;
        }

        if (intent.hasExtra("jornal")) {
            MyLogger.info("jornal: " + intent.getSerializableExtra("jornal"));
            fragmentPosition = 5;
        }


        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        setupDrawer();
        setupContainer(fragmentPosition);

        this.back_btnMenuProduct = (LinearLayout) findViewById(R.id.back_btnMenuProduct);
        this.back_btnMenuLista = (LinearLayout) findViewById(R.id.back_btnMenuLista);
        this.back_btnMenuPedidos = (LinearLayout) findViewById(R.id.back_btnMenuPedidos);
        this.back_btnMenuOfertas = (LinearLayout) findViewById(R.id.back_btnMenuOfertas);
        this.back_btnMenuJornal = (LinearLayout) findViewById(R.id.back_btnMenuJornal);

        Button btnMenuProduct = (Button) findViewById(R.id.btnMenuProduct);
        btnMenuProduct.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setupContainer(1);
                        allBlack();
                        back_btnMenuProduct.setBackgroundColor(getResources().getColor(R.color.main_grey_dk));
                    }
                }
        );

        Button btnMenuLista = (Button) findViewById(R.id.btnMenuLista);
        btnMenuLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupContainer(2);
                allBlack();
                back_btnMenuLista.setBackgroundColor(getResources().getColor(R.color.main_grey_dk));
            }
        });

        Button btnMenuOfertas = (Button) findViewById(R.id.btnMenuOfertas);
        btnMenuOfertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupContainer(4);
                allBlack();
                back_btnMenuOfertas.setBackgroundColor(getResources().getColor(R.color.main_grey_dk));
            }
        });


        Button btnMenuJornal = (Button) findViewById(R.id.btnMenuJornal);
        btnMenuJornal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupContainer(5);
                allBlack();
                back_btnMenuJornal.setBackgroundColor(getResources().getColor(R.color.main_grey_dk));
            }
        });


        Button btnMenuPedidos = (Button) findViewById(R.id.btnMenuPedidos);
        btnMenuPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupContainer(8);
                allBlack();
                back_btnMenuPedidos.setBackgroundColor(getResources().getColor(R.color.main_grey_dk));
            }
        });


        if (this.checkPlayServices()) {
            Intent startIntent = new Intent(this, RegistrationIntentService.class);
            startService(startIntent);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //  drawerToggle.syncState();
    }


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                MyLogger.info("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }



    /**
     * Setup the drawer layout. This method also includes the method calls for
     * setting up the Left & Right side drawers.
     */
    private void setupDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);
        drawerLayout.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View view) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }
        };
        drawerToggle.syncState();

        drawerLayout.setDrawerListener(drawerToggle);
        drawerLayout.closeDrawers();


        setupLeftNavDrawer();

        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                MyLogger.info("onDrawerClosed");
                onNavDrawerStateChanged(false, false);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                MyLogger.info("onDrawerOpened");
                onNavDrawerStateChanged(true, false);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                onNavDrawerStateChanged(
                        drawerLayout.isDrawerOpen(GravityCompat.START),
                        newState != DrawerLayout.STATE_IDLE);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }
        });


    }

    /**
     * Called On navigation drawer state changed.
     *
     * @param isOpen      true if drawer is open
     * @param isAnimating true of drawer is animating
     */
    private void onNavDrawerStateChanged(boolean isOpen, boolean isAnimating) {
        if (mActionBarAutoHideEnabled && isOpen) {
            autoShowOrHideActionBar(true);
        }
    }

    /**
     * Auto show or hide action bar.
     *
     * @param show true to show the bar
     */
    private void autoShowOrHideActionBar(boolean show) {
        if (show == mActionBarShown) {
            return;
        }

        mActionBarShown = show;
        onActionBarAutoShowOrHide(show);
    }

    /**
     * Called On action bar auto show or hide.
     *
     * @param shown true is action bar is showing
     */
    @SuppressLint("NewApi")
    private void onActionBarAutoShowOrHide(final boolean shown) {
        if (mStatusBarColorAnimator != null) {
            mStatusBarColorAnimator.cancel();
        }
        mStatusBarColorAnimator = ObjectAnimator.ofInt(
                drawerLayout,
                drawerLayout != null ? "statusBarBackgroundColor"
                        : "statusBarColor",
                shown ? getResources().getColor(R.color.main_color_dk)
                        : getResources().getColor(R.color.main_color),
                shown ? getResources().getColor(R.color.main_color)
                        : getResources().getColor(R.color.main_color_dk))
                .setDuration(250);
        if (drawerLayout != null) {
            mStatusBarColorAnimator
                    .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(
                                ValueAnimator valueAnimator) {
                            ViewCompat.postInvalidateOnAnimation(drawerLayout);
                            if (shown) {
                                getSupportActionBar().show();
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    getWindow().setStatusBarColor(
                                            getResources().getColor(
                                                    R.color.main_color_dk));
                                }
                            } else {
                                getSupportActionBar().hide();
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    getWindow().setStatusBarColor(
                                            getResources().getColor(
                                                    R.color.main_color));
                                }
                            }
                        }
                    });
        }
        mStatusBarColorAnimator.setEvaluator(ARGB_EVALUATOR);
        mStatusBarColorAnimator.start();

    }

    /**
     * Enable action bar auto hide.
     *
     * @param recList the RecyclerView list
     */
    public void enableActionBarAutoHide(final RecyclerView recList) {
        initActionBarAutoHide();
        recList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            final static int ITEMS_THRESHOLD = 3;
            int lastFvi = 0;

            @Override
            public void onScrollStateChanged(RecyclerView view, int scrollState) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                // autoShowOrHideActionBar(recyclerView.getScrollY()<=100);

                try {
                    LinearLayoutManager llm = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    int firstVisibleItem = llm.findFirstVisibleItemPosition();
                    onMainContentScrolled(
                            firstVisibleItem <= ITEMS_THRESHOLD ? 0
                                    : Integer.MAX_VALUE, lastFvi
                                    - firstVisibleItem > 0 ? Integer.MIN_VALUE
                                    : lastFvi == firstVisibleItem ? 0
                                    : Integer.MAX_VALUE);
                    lastFvi = firstVisibleItem;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

			/* @Override
             public void onScroll(RecyclerView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			     onMainContentScrolled(firstVisibleItem <= ITEMS_THRESHOLD ? 0 : Integer.MAX_VALUE,
			             lastFvi - firstVisibleItem > 0 ? Integer.MIN_VALUE :
			                     lastFvi == firstVisibleItem ? 0 : Integer.MAX_VALUE
			     );
			     lastFvi = firstVisibleItem;
			 }*/
        });
    }

    /**
     * Indicates that the main content has scrolled (for the purposes of
     * showing/hiding the action bar for the "action bar auto hide" effect).
     * currentY and deltaY may be exact (if the underlying view supports it) or
     * may be approximate indications: deltaY may be INT_MAX to mean
     * "scrolled forward indeterminately" and INT_MIN to mean
     * "scrolled backward indeterminately". currentY may be 0 to mean "somewhere
     * close to the start of the list" and INT_MAX to mean "we don't know, but
     * not at the start of the list"
     *
     * @param currentY the current y
     * @param deltaY   the delta y
     */
    private void onMainContentScrolled(int currentY, int deltaY) {
        if (deltaY > mActionBarAutoHideSensivity) {
            deltaY = mActionBarAutoHideSensivity;
        } else if (deltaY < -mActionBarAutoHideSensivity) {
            deltaY = -mActionBarAutoHideSensivity;
        }

        if (Math.signum(deltaY) * Math.signum(mActionBarAutoHideSignal) < 0) {
            // deltaY is a motion opposite to the accumulated signal, so reset
            // signal
            mActionBarAutoHideSignal = deltaY;
        } else {
            // add to accumulated signal
            mActionBarAutoHideSignal += deltaY;
        }

        boolean shouldShow = currentY < mActionBarAutoHideMinY
                || mActionBarAutoHideSignal <= -mActionBarAutoHideSensivity;
        autoShowOrHideActionBar(shouldShow);
    }

    /**
     * Initializes the Action Bar auto-hide (aka Quick Recall) effect.
     */
    public void initActionBarAutoHide() {
        mActionBarAutoHideEnabled = true;
        mActionBarAutoHideMinY = getResources().getDimensionPixelSize(
                R.dimen.action_bar_auto_hide_min_y);
        mActionBarAutoHideSensivity = getResources().getDimensionPixelSize(
                R.dimen.action_bar_auto_hide_sensivity);
    }

    /**
     * Setup the left navigation drawer/slider. You can add your logic to load
     * the contents to be displayed on the left side drawer. It will also setup
     * the Header and Footer contents of left drawer. This method also apply the
     * Theme for components of Left drawer.
     */
    @SuppressLint("InflateParams")
    private void setupLeftNavDrawer() {
        drawerLeft = (ListView) findViewById(R.id.left_drawer);
        drawerLeft.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        View header = getLayoutInflater().inflate(R.layout.left_nav_header,
                null);
        TextView textUserName = (TextView) header.findViewById(R.id.textUserName);


        if (OfertaDigitalRestClient.getRef().getUserId() != null) {
            textUserName.setText(OfertaDigitalRestClient.getRef().getUserName());
        }

        /*
        {
            final String selectStr = "Selecionar Outro";
            final Spinner opSpiner = (Spinner) header.findViewById(R.id.spinnerOptions);

            String[] arraySpinner = new String[]{
                    currentStore.getNomeFantasia(), selectStr
            };
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, arraySpinner);
            opSpiner.setAdapter(adapter);
            opSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    String sel = (String) opSpiner.getSelectedItem();
                    if (selectStr.equals(sel)) {
                        final StoreSelectDialog dialog = new StoreSelectDialog();
                        dialog.setListener(new StoreSelectDialog.StoreListAdapter.ClickListener() {
                            @Override
                            public void process(StoreInfo info) {

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                                        android.R.layout.simple_spinner_item, new String[]{
                                        info.getNomeFantasia(), selectStr
                                });
                                currentStore = info;

                                MyLogger.info("store_id: " + currentStore.getId());

                                opSpiner.setAdapter(adapter);
                                dialog.close();
                            }
                        });
                        dialog.show(MainActivity.this);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }*/

        drawerLeft.addHeaderView(header);

        final ArrayList<Data> al = new ArrayList<Data>();
        al.add(new Data(new String[]{"Produtos"}, new int[]{
                R.drawable.menu_produtos, R.drawable.menu_produtos}));

        al.add(new Data(new String[]{"Lista"}, new int[]{
                R.drawable.menu_lista_compras, R.drawable.menu_lista_compras}));

        al.add(new Data(new String[]{"Carrinho"}, new int[]{
                R.drawable.menu_carrinho_compras, R.drawable.menu_carrinho_compras}));

        al.add(new Data(new String[]{"Ofertas"}, new int[]{
                R.drawable.menu_ofertas_diarias, R.drawable.menu_ofertas_diarias}));

        al.add(new Data(new String[]{"Jornal"}, new int[]{
                R.drawable.menu_jornal_ofertas, R.drawable.menu_jornal_ofertas}));

        al.add(new Data(new String[]{"Entrega"}, new int[]{
                R.drawable.menu_entregas, R.drawable.menu_entregas}));

        al.add(new Data(new String[]{"Dicas de Saúde"}, new int[]{
                R.drawable.menu_dicas_saude, R.drawable.menu_dicas_saude}));

        al.add(new Data(new String[]{"Pedidos"}, new int[]{
                R.drawable.menu_pedidos, R.drawable.menu_pedidos}));

        al.add(new Data(new String[]{"Videos"}, new int[]{
                R.drawable.menu_videos, R.drawable.menu_videos}));

        al.add(new Data(new String[]{"Preferências"}, new int[]{
                R.drawable.menu_preferencias, R.drawable.menu_preferencias}));

        al.add(new Data(new String[]{"Sair"}, new int[]{
                R.drawable.ic_nav5, R.drawable.ic_nav5_sel}));

        this.adp = new LeftNavAdapter(this, al);

        // default started
        adp.setSelection(1);
        drawerLeft.setAdapter(adp);
        drawerLeft.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> l, View arg1, int pos,
                                    long arg3) {
                if (pos != 0) {
                    adp.setSelection(pos - 1);
                }
                drawerLayout.closeDrawers();
                setupContainer(pos);
            }
        });

    }

    /**
     * Setup the container fragment for drawer layout. This method will setup
     * the grid view display of main contents. You can customize this method as
     * per your need to display specific content.
     *
     * @param pos the new up container
     */
    public void setupContainer(int pos) {

		/*if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			toolbar.setBackgroundColor(getResources().getColor(R.color.main_color));
		}*/

        if (this.adp != null) {
            this.adp.setSelection(pos - 1);
        }

        CustomFragment f = null;
        String title = null;

        Bundle args = new Bundle();
        args.putSerializable("storeinfo", this.currentStore);


        ShowCategoriesHorizontalFragment buyCartFragment = null;

        MyLogger.info("setupContainer pos: " + pos);
        if (pos == 0) {
            return;
        } else if (pos == 1) {
            if (buyCartFragment == null) {
                buyCartFragment = new ShowCategoriesHorizontalFragment();
            }
            f = buyCartFragment;

            args.putBoolean("show_categories", true);
            args.putString("title", "Produtos");

            f.setArguments(args);
            title = "Produtos";
        } else if (pos == 2) {
            f = new BuyListFragment();
            f.setArguments(args);
            title = "Lista de Compras";
        } else if (pos == 3) {
            if (buyCartFragment == null) {
                buyCartFragment = new ShowCategoriesHorizontalFragment();
            }
            f = buyCartFragment;

            args.putBoolean("show_categories", false);
            args.putString("title", "Carrinho de Compras");

            f.setArguments(args);
            title = "Carrinho de Compras";
        } else if (pos == 4) {
            f = new OfferProductFragment();
            title = "Oferta Diária";
        } else if (pos == 5) {
            f = new JornalImageFragment();
            f.setArguments(args);
            title = "Jornal";
        } else if (pos == 6) {
            f = new DeliveryInfoFragment();
            f.setArguments(args);
            title = "Entrega";
        } else if (pos == 7) {
            f = new RecipentInfoFragment();
            f.setArguments(args);
            title = "Receitas";
        } else if (pos == 8) {
            f = new OrderListFragment();
            f.setArguments(args);
            title = "Pedidos";
        } else if (pos == 9) {
            f = new VideosFragment();
            f.setArguments(args);
            title = "Videos";
        } else if (pos == 10) {
            f = new com.glam.ui.Settings();
            title = "Preferências";
        } else if (pos == 11) {
            {
                try {
                    LoginManager.getInstance().logOut();
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                try {
                    OfertaDigitalDataSource dao = new OfertaDigitalDataSource(this);

                    dao.deleteObject("loginfo");
                    dao.deleteObject("pwdinfo");
                    dao.deleteObject("storeinfo");

                    dao.close();

                    dao = null;
                } catch (Exception e) {
                    MyLogger.log(e);
                }

            }

            startActivity(new Intent(this, Login.class));
            finish();
        }

        if (f == null) {
            return;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, f).commit();
        if (getSupportActionBar() != null) {
            MyLogger.info("setting title: " + title);
            getSupportActionBar().setTitle(title);
        }
    }


    @Override
    public void onBackPressed() {
        MyLogger.info("onBackPressed");

        MyLogger.info("isOpen: " + drawerLayout.isDrawerOpen(GravityCompat.START));

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            MyLogger.info("super.onBackPressed()");
            super.onBackPressed();
        }

        android.support.v4.app.Fragment fragment = getVisibleFragment();
        MyLogger.info("onBackPressed fragment visible: " + fragment);

        if (fragment instanceof ShowCategoriesHorizontalFragment) {
            try {
                ((ShowCategoriesHorizontalFragment) fragment).updateTotal();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.gc();

    }


    public android.support.v4.app.Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<android.support.v4.app.Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (android.support.v4.app.Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    /* (non-Javadoc)
         * @see android.app.Activity#onPostCreate(android.os.Bundle)
         */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggle
        drawerToggle.onConfigurationChanged(newConfig);
        drawerToggle.syncState();
    }

    /* (non-Javadoc)
     * @see com.whatshere.custom.CustomActivity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MyLogger.info("MainActivity onOptionsItemSelected: " + +item.getItemId());

        if (item.getItemId() == android.R.id.home) {
            getSupportFragmentManager().popBackStack();
        }

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyLogger.info("onResume: " + this);
        this.registerReceiver(notifReceiver, new IntentFilter("of_notification"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyLogger.info("onPause: " + this);
        this.unregisterReceiver(notifReceiver);
    }

    private BroadcastReceiver notifReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.hasExtra("jornal")) {
                JornalStoreInfo offer = (JornalStoreInfo) intent.getSerializableExtra("jornal");
                android.support.v4.app.Fragment fragment = getVisibleFragment();
                if (fragment instanceof OfferProductFragment) {
                    OfferProductFragment offerFragment = (OfferProductFragment) fragment;

                }
            }
        }
    };


}
