package com.glam.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.glam.utils.MyLogger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.NativeImage;

public class OfertaDigitalDataSource {

    private SQLiteDatabase database;
    private OfertaDigitalSQLiteHelper helper;

    public OfertaDigitalDataSource(Context context) {
        this.helper = new OfertaDigitalSQLiteHelper(context);
        this.database = this.helper.getReadableDatabase();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public <T> void persitObject(String id, T obj) throws Exception {
        byte[] objBytes = this.serialize(obj);
        ContentValues values = new ContentValues();
        values.put("ID", id);
        values.put("CLASSTYPE", obj.getClass().getCanonicalName());
        values.put("OBJECTDATA", objBytes);
        values.put("INSERTDATE", getDateTime());
        this.database.insert(ObjectData.class.getSimpleName(), null, values);
    }

    public <T> void persitImage(String id, byte[] image) throws Exception {
        ContentValues values = new ContentValues();
        values.put("ID", id);
        values.put("CLASSTYPE", NativeImage.class.getCanonicalName());
        values.put("OBJECTDATA", image);
        values.put("INSERTDATE", getDateTime());
        this.database.insert(ObjectData.class.getSimpleName(), null, values);
    }


    public <T> void updateObject(String id, T obj) throws Exception {
        byte[] objBytes = this.serialize(obj);
        ContentValues values = new ContentValues();
        //   values.put("ID", id);
        values.put("CLASSTYPE", obj.getClass().getCanonicalName());
        values.put("OBJECTDATA", objBytes);
        values.put("INSERTDATE", getDateTime());
        this.database.update(ObjectData.class.getSimpleName(), values, " ID = '" + id + "' ", null);
    }

    public void deleteObject(String id) throws Exception {
        this.database.delete(ObjectData.class.getSimpleName(), " ID ='" + id + "' ", null);
    }

    public int countObjects(Class c) throws Exception {
        Cursor cursor = database.rawQuery("select count(*) from OBJECTDATA where CLASSTYPE = '" + c.getCanonicalName() + "'", new String[]{});

        if (cursor.moveToFirst()) {
            return cursor.getInt(0);
        } else {
            return -1;
        }

    }

    public boolean existRegisterWithId(String id) throws Exception {
        Cursor cursor = database.query(ObjectData.class.getSimpleName(),
                new String[]{"ID"},
                " ID = '" + id + "' ", null, null, null, null);
        return cursor.moveToFirst();
    }

    public <T> T loadObject(String id, Class<T> classs) throws Exception {
        Cursor cursor = database.query(ObjectData.class.getSimpleName(),
                new String[]{"OBJECTDATA", "CLASSTYPE"},
                " ID = '" + id + "' ", null, null, null, null);
        if (cursor.moveToFirst()) {
            byte[] bytesObj = cursor.getBlob(0);
            cursor.close();
            return (T) this.deserialize(bytesObj);
        }
        cursor.close();

        return null;
    }

    public byte[] loadImage(String id) throws Exception {
        Cursor cursor = database.query(ObjectData.class.getSimpleName(),
                new String[]{"ID", "OBJECTDATA", "CLASSTYPE", "INSERTDATE"},
                " CLASSTYPE = '" + NativeImage.class.getCanonicalName() + "' AND ID ='" + id + "' ", null, null, null, null, null);

        List<String> results = new ArrayList<>();
        if (cursor.moveToFirst()) {
            byte[] imgblob = cursor.getBlob(1);
            cursor.close();
            return imgblob;
        } else {
            cursor.close();
            return null;
        }
    }

    public <T> List<String> loadImages(int counter) throws Exception {
        Cursor cursor = database.query(ObjectData.class.getSimpleName(),
                new String[]{"ID", "OBJECTDATA", "CLASSTYPE", "INSERTDATE"},
                " CLASSTYPE = '" + NativeImage.class.getCanonicalName() + "' ", null, null, null, " DATE(INSERTDATE) ASC ", String.valueOf(counter));

        List<String> results = new ArrayList<>();
        while (cursor.moveToNext()) {
            // byte[] bytesObj = cursor.getBlob(0);
            MyLogger.info(cursor.getString(3));
            results.add(cursor.getString(0));
        }
        cursor.close();
        return results.isEmpty() ? null : results;
    }

    public <T> List<T> loadMultipleObjects(String suffix, Class<T> classs) throws Exception {
        Cursor cursor = database.query(ObjectData.class.getSimpleName(),
                new String[]{"OBJECTDATA", "CLASSTYPE"},
                "ID like '%" + suffix + "%' ", null, null, null, null);

        List<T> results = new ArrayList<>();
        while (cursor.moveToFirst()) {
            byte[] bytesObj = cursor.getBlob(0);
            cursor.close();
            results.add((T) this.deserialize(bytesObj));
        }
        return results.isEmpty() ? null : results;
    }


    private byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    private Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }

    public void close() {
        this.database.close();

        this.helper = null;
        this.database = null;
    }


}
