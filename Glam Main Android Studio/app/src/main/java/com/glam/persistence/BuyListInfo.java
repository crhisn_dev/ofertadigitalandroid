package com.glam.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.innove.ofertadigital.persistence.ItemProductQuantityInfo;

@Entity
public class BuyListInfo implements Serializable {

	public static enum BUYLIST_STATUS {
		OK, DELETED
	}

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private BuyListId buyListId;

	private String name;

	private String description;

	@OneToMany(fetch = FetchType.EAGER)
	private List<ItemProductQuantityInfo> itemsProduct;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	private BUYLIST_STATUS currentStatus;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BuyListId getBuyListId() {
		return buyListId;
	}

	public void setBuyListId(BuyListId buyListId) {
		this.buyListId = buyListId;
	}

	public List<ItemProductQuantityInfo> getItemsProduct() {
		return itemsProduct;
	}

	public void setItemsProduct(List<ItemProductQuantityInfo> itemsProduct) {
		this.itemsProduct = itemsProduct;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BUYLIST_STATUS getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(BUYLIST_STATUS currentStatus) {
		this.currentStatus = currentStatus;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BuyListInfo that = (BuyListInfo) o;

		return buyListId != null ? buyListId.equals(that.buyListId) : that.buyListId == null;

	}

	@Override
	public int hashCode() {
		return buyListId != null ? buyListId.hashCode() : 0;
	}
}
