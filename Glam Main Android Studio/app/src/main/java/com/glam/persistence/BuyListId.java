package com.glam.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

import br.com.innove.ofertadigital.persistence.UserId;

@Embeddable
public class BuyListId implements Serializable {

	private static final long serialVersionUID = 1L;
	private String buyListId;
	private UserId userId;

	public String getBuyListId() {
		return buyListId;
	}

	public void setBuyListId(String buyListId) {
		this.buyListId = buyListId;
	}

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buyListId == null) ? 0 : buyListId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuyListId other = (BuyListId) obj;
		if (buyListId == null) {
			if (other.buyListId != null)
				return false;
		} else if (!buyListId.equals(other.buyListId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
