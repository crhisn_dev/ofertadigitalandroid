package com.glam.persistence;


public class ObjectData  {

    public static String CREATE_TABLE = "CREATE TABLE OBJECTDATA (ID VARCHAR PRIMARY KEY, CLASSTYPE VARCHAR, INSERTDATE TEXT DEFAULT CURRENT_TIMESTAMP, OBJECTDATA BLOB);";

    private String id;
    private String classType;
    private long insertDate;
    private byte[] objectData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(long insertDate) {
        this.insertDate = insertDate;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public byte[] getObjectData() {
        return objectData;
    }

    public void setObjectData(byte[] objectData) {
        this.objectData = objectData;
    }
}
