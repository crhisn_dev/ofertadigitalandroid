package com.glam.persistence;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.glam.utils.MyLogger;

public class OfertaDigitalSQLiteHelper extends SQLiteOpenHelper {

    public OfertaDigitalSQLiteHelper(Context context) {
        super(context, "ofertadigital", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(ObjectData.CREATE_TABLE);
        MyLogger.info("banco de dados criado!");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i1) {

    }
}
