package com.glam;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.transport.ImagesArrayResponseData;
import com.glam.ui.CardAdapterForCategory;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.innove.ofertadigital.persistence.JornalStoreInfo;
import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.NativeImage;
import br.com.innove.ofertadigital.persistence.ProductInfo;


public class JornalViewerActivity extends FragmentActivity {

    private OfertaDigitalDataSource dao;
    private ImageCache cache;

    private JornalStoreInfo jornal;
    private ViewPager pager;

    private List<Bitmap> bitmaps;
    private RelativeLayout linlaHeaderProgress;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.dao = new OfertaDigitalDataSource(this);
        this.cache = new ImageCache(this.dao);
        this.bitmaps = new ArrayList<>();

        this.jornal = (getIntent() != null && getIntent().hasExtra("jornal")) ?
                (JornalStoreInfo) getIntent().getSerializableExtra("jornal") :
                new JornalStoreInfo();

        setContentView(R.layout.main_container);

        this.linlaHeaderProgress = (RelativeLayout) this.findViewById(R.id.linlaHeaderProgress);


        this.pager = (ViewPager) findViewById(R.id.pager);
        this.pager.setOffscreenPageLimit(1);
        this.pager.setAdapter(new ScreenSlidePagerAdapter(new ArrayList<MediaDesc>()));


        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    requestImages(jornal.getMediasList());
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                pager.setAdapter(new ScreenSlidePagerAdapter(jornal.getMediasList()));
                linlaHeaderProgress.setVisibility(View.GONE);
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    private boolean mustBeDownload(MediaDesc media) {
        if (MediaDesc.MEDIA_TYPE.IMAGE.equals(media.getType()) &&
                media.getNativeimage_id() != null &&
                !this.cache.isCached(media.getNativeimage_id())) {
            return true;
        }

        return false;
    }

    public void requestImages(List<MediaDesc> mediasList) throws Exception {
        MyLogger.info("start requestImages");
        List<String> nativeImagesIds = new ArrayList<>();

        for (MediaDesc media : mediasList) {
            if (this.mustBeDownload(media) && !nativeImagesIds.contains(media.getNativeimage_id())) {
                nativeImagesIds.add(media.getNativeimage_id());
            }
        }


        ImagesArrayResponseData images = OfertaDigitalRestClient.getRef().requestImages(nativeImagesIds);
        Collection<NativeImage> nativeImages = images.getImages().values();
        for (NativeImage nativeImage : nativeImages) {
            MyLogger.info("caching: " + nativeImage.getId() + " bytes: " + nativeImage.getSourcestream().length);
            this.cache.cacheImage(nativeImage);
        }

        MyLogger.info("end called requestImages");
    }

    private class ScreenSlidePagerAdapter extends PagerAdapter {

        private List<MediaDesc> mediasList;

        public ScreenSlidePagerAdapter(List<MediaDesc> mediasList) {
            this.mediasList = mediasList;
        }

        @Override
        public int getCount() {
            return mediasList.size();
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            MyLogger.info("destroyItem: " + object);
            ((ViewPager) container).removeView((View) object);

            try{
                MyLogger.info("trying recycle image");
                bitmaps.get(position).recycle();
                Runtime.getRuntime().gc();
            }catch (Exception e){
                MyLogger.log(e);
            }

            if (object instanceof android.widget.LinearLayout) {
                android.widget.LinearLayout rr = (android.widget.LinearLayout) object;
                for (int i = 0; i < rr.getChildCount(); i++) {
                    Object vv = rr.getChildAt(i);
                    if (vv instanceof PhotoView) {
                        MyLogger.info("release PhotoView: " + vv);
                        PhotoView rview = (PhotoView) vv;
                        rview.setImageBitmap(null);
                    }
                }
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            MediaDesc media = this.mediasList.get(position);

            LinearLayout linearLayout = new LinearLayout(getBaseContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            PhotoView photoView = new PhotoView(getBaseContext());

            {
                byte[] image = cache.getImage(media.getNativeimage_id());

                if (image != null) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                    Bitmap imagebitmap = BitmapFactory.decodeByteArray(image, 0, image.length, options);

                    {
                        double scale = 0.7d;

                        imagebitmap = Bitmap.createScaledBitmap(imagebitmap, (int) (imagebitmap.getWidth() * scale), (int) (imagebitmap.getHeight() * scale), true);
                    }
                    photoView.setImageBitmap(imagebitmap);

                    bitmaps.add(imagebitmap);
                } else {
                    photoView.setImageResource(R.drawable.imagem_nao_encontrada);
                }
            }

            photoView.setLayoutParams(layoutParams);

            linearLayout.addView(photoView);

            container.addView(linearLayout,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT);

            return linearLayout;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return "Pagina " + (position + 1) + "/" + mediasList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MyLogger.info("onBackPressed: " + this);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        MyLogger.info("onDestroy: " + this);
        try {
            this.dao.close();
            this.dao = null;
            this.cache = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }

        this.pager = null;

        try {
            for (Bitmap b : bitmaps) {
                MyLogger.info("calling recycle bitmap");
                b.recycle();
                //  b = null;
            }
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            bitmaps.clear();
            bitmaps = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }


        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }
}
