package com.glam.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;


import com.glam.http.OfertaDigitalRestClient;
import com.glam.utils.MyLogger;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by cnoriega on 6/15/17.
 */

public class RegistrationIntentService extends IntentService {

    public static String LOG = "LOG";

    public RegistrationIntentService() {
        super(LOG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        synchronized (LOG) {
            try {
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(GoogleServicesCredentials.SENDER_ID,
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                        null);
                MyLogger.info("GCM token: " + token);
                OfertaDigitalRestClient.getRef().sendGCMToken(token);
            } catch (Exception e) {
                MyLogger.log(e);
            }


        }
    }
}
