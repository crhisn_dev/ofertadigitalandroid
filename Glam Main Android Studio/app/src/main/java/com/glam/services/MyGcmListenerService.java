package com.glam.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.fasterxml.jackson.core.type.TypeReference;
import com.glam.MainActivity;
import com.glam.R;
import com.glam.SplashScreen;
import com.glam.http.JSONConverter;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.transport.OfferStoreInfo;
import com.glam.utils.MyLogger;
import com.google.android.gms.gcm.GcmListenerService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import br.com.innove.ofertadigital.persistence.JornalStoreInfo;
import br.com.innove.ofertadigital.persistence.OrderStatusNotif;
import br.com.innove.ofertadigital.persistence.PromotionInfo;
import br.com.innove.ofertadigital.persistence.StoreInfo;

/**
 * Created by cnoriega on 6/15/17.
 */

public class MyGcmListenerService extends GcmListenerService {


    @Override
    public void onMessageReceived(String from, Bundle data) {
        MyLogger.info("### onMessageReceived for Oreo: " + data + " ###");

        try {
            String jsonString = data.getString("json");
            String class1 = data.getString("class1");
            String class2 = data.getString("class2");
            String mensagem = data.getString("mensagem");

            if (OrderStatusNotif.class.getCanonicalName().equals(class1)) {
                try {
                    JSONConverter conv = new JSONConverter();
                    OrderStatusNotif orderStatus = conv.toObject(jsonString, OrderStatusNotif.class);

                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_oferta_digital);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, orderStatus.getOrderId().getOrderId())
                            .setTicker("Atualização do pedido")
                            .setSmallIcon(R.drawable.entrega)
                            .setLargeIcon(largeIcon)
                            .setContentTitle("Seu pedido: " + orderStatus.getOrderId().getOrderId() + " foi atualizado")
                            .setAutoCancel(true)
                            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                            .setVibrate(new long[]{1000, 1000});


                    OfertaDigitalDataSource dao = new OfertaDigitalDataSource(this);
                    StoreInfo storeinfo = dao.loadObject("storeinfo", StoreInfo.class);
                    dao.close();

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("order_status", orderStatus);
                    intent.putExtra("storeinfo", storeinfo);
                    PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(pi);

                    NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
                    bigText.bigText("Atualização do pedido");
                    builder.setStyle(bigText);

                    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    {
                        String notification_id = orderStatus.getOrderId().getOrderId();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            NotificationChannel mChannel = nm.getNotificationChannel(notification_id);
                            if (mChannel == null) {
                                mChannel = new NotificationChannel(notification_id, "Atualização do pedido", NotificationManager.IMPORTANCE_HIGH);
                                mChannel.enableVibration(true);
                                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                                nm.createNotificationChannel(mChannel);
                            }
                        }
                        builder.setChannelId(notification_id);
                    }

                    nm.notify(orderStatus.getOrderId().getOrderId(), NotificationID.getID(), builder.build());

                } catch (Exception e) {
                    MyLogger.log(e);
                }
            }

            if (JornalStoreInfo.class.getCanonicalName().equals(class1)) {
                try {
                    JSONConverter conv = new JSONConverter();
                    JornalStoreInfo jornal = conv.toObject(jsonString, JornalStoreInfo.class);

                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_oferta_digital);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, jornal.getJornalId().getJornal_id())
                            .setTicker(jornal.getNamePromo())
                            .setSmallIcon(R.drawable.entrega)
                            .setLargeIcon(largeIcon)
                            .setContentTitle(jornal.getDescription())
                            .setAutoCancel(true)
                            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                            .setVibrate(new long[]{1000, 1000});


                    OfertaDigitalDataSource dao = new OfertaDigitalDataSource(this);
                    StoreInfo storeinfo = dao.loadObject("storeinfo", StoreInfo.class);
                    dao.close();


                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("jornal", jornal);
                    intent.putExtra("storeinfo", storeinfo);
                    PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(pi);

                    NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
                    bigText.bigText(jornal.getNamePromo());
                    builder.setStyle(bigText);

                    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    {
                        String notification_id = jornal.getJornalId().getJornal_id();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            NotificationChannel mChannel = nm.getNotificationChannel(notification_id);
                            if (mChannel == null) {
                                mChannel = new NotificationChannel(notification_id, jornal.getNamePromo(), NotificationManager.IMPORTANCE_HIGH);
                                mChannel.enableVibration(true);
                                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                                nm.createNotificationChannel(mChannel);
                            }
                        }
                        builder.setChannelId(notification_id);
                    }


                    nm.notify(jornal.getJornalId().getJornal_id(), NotificationID.getID(), builder.build());

                    try {
                        Intent intent_not = new Intent("of_notification");
                        intent_not.putExtra("jornal", jornal);
                        this.sendBroadcast(intent_not);
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }
                } catch (Exception e) {
                    MyLogger.log(e);
                }
            }

            if (Set.class.getCanonicalName().equals(class1)
                    &&
                    PromotionInfo.class.getCanonicalName().equals(class2)) {

                try {
                    JSONConverter conv = new JSONConverter();
                    ArrayList<PromotionInfo> promotions = conv.toObject(jsonString, new TypeReference<ArrayList<PromotionInfo>>() {
                    });

                    String descr = "";
                    for (PromotionInfo promo : promotions) {
                        descr = descr + promo.getProduct().getName() + " \n";
                    }

                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_oferta_digital);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, promotions.get(0).getPromoId().getPromo_id())
                            .setTicker("Produtos em Oferta")
                            .setSmallIcon(R.drawable.entrega)
                            .setLargeIcon(largeIcon)
                            .setContentTitle(mensagem)
                            .setAutoCancel(true)
                            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                            .setVibrate(new long[]{1000, 1000});

                    OfertaDigitalDataSource dao = new OfertaDigitalDataSource(this);
                    StoreInfo storeinfo = dao.loadObject("storeinfo", StoreInfo.class);
                    dao.close();

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("promotions", promotions);
                    intent.putExtra("storeinfo", storeinfo);
                    PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(pi);


                    NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                    inboxStyle.setSummaryText("Produtos em Oferta");
                    // int number = Integer.parseInt(data.getString("userTo_amount_new_messages"));

                    String notification_id = "";
                    for (PromotionInfo promo : promotions) {
                        String messsage = promo.getProduct().getName() + " " + promo.getPromoPrice().getValue1();
                        // int size = messsage.indexOf(":");
                        Spannable sb = new SpannableString(messsage);

                        sb.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        inboxStyle.addLine(sb);

                        notification_id = promo.getPromoId().getPromo_id();
                    }

                    inboxStyle.setBigContentTitle(mensagem + "\n" + promotions.size() + " novo produtos");
                    builder.setNumber(promotions.size());
                    builder.setStyle(inboxStyle);


                    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            NotificationChannel mChannel = nm.getNotificationChannel(notification_id);
                            if (mChannel == null) {
                                mChannel = new NotificationChannel(notification_id, "Produtos em Oferta", NotificationManager.IMPORTANCE_HIGH);
                                mChannel.enableVibration(true);
                                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                                nm.createNotificationChannel(mChannel);
                            }
                        }
                        builder.setChannelId(notification_id);
                    }

                    nm.notify(notification_id, NotificationID.getID(), builder.build());

                    try {
                        Intent intent_noti = new Intent("promo_notification");
                        intent_noti.putExtra("promotions", promotions);
                        this.sendBroadcast(intent_noti);
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }
                } catch (Exception e) {
                    MyLogger.log(e);
                }
            }
        } catch (Exception e) {
            MyLogger.log(e);
        }

    }

    public static class NotificationID {
        private final static AtomicInteger c = new AtomicInteger(0);

        public static int getID() {
            return c.incrementAndGet();
        }
    }
}
