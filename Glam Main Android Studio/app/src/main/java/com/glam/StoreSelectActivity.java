package com.glam;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.glam.buycart.BuyCartRemoteImpl;
import com.glam.dialog.StoreSelectDialog;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.utils.MyLogger;

import java.util.ArrayList;
import java.util.List;

import br.com.innove.ofertadigital.persistence.StoreInfo;


public class StoreSelectActivity extends Activity {

    private OfertaDigitalDataSource dao;
    private RecyclerView storeList;
    private LinearLayout linlaHeaderProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.store_select_layout);

        this.linlaHeaderProgress = (LinearLayout) this.findViewById(R.id.linlaHeaderProgress);


        this.dao = new OfertaDigitalDataSource(this);

        this.storeList = (RecyclerView) this.findViewById(R.id.storeList);
        this.storeList.setHasFixedSize(true);
        this.storeList.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.storeList.setLayoutManager(llm);

        StoreSelectDialog.StoreListAdapter ca = new StoreSelectDialog.StoreListAdapter(StoreSelectActivity.this, this.storeList, new ArrayList<StoreInfo>(), new StoreSelectDialog.StoreListAdapter.ClickListener() {
            @Override
            public void process(StoreInfo info) {
                try {
                    MyLogger.info("selected store:" + info.getRazaoSocial());
                    MyLogger.info("store_id: " + info.getId());

                    BuyCartRemoteImpl.getRef().clearBuyCart();

                    if (StoreSelectActivity.this.dao.existRegisterWithId("storeinfo")) {
                        StoreSelectActivity.this.dao.updateObject("storeinfo", info);
                    } else {
                        StoreSelectActivity.this.dao.persitObject("storeinfo", info);
                    }

                    Intent i = new Intent(StoreSelectActivity.this, MainActivity.class);
                    i.putExtra("storeinfo", info);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
            }
        });
        this.storeList.setAdapter(ca);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            private List<StoreInfo> stores;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    stores = OfertaDigitalRestClient.getRef().findStores();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (this.stores != null) {
                    StoreSelectDialog.StoreListAdapter adapter = (StoreSelectDialog.StoreListAdapter) storeList.getAdapter();
                    adapter.setStoreInfoList(stores);

                    adapter.notifyDataSetChanged();
                }
                linlaHeaderProgress.setVisibility(View.GONE);
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected void onDestroy() {
        MyLogger.info("onDestroy: " + this);
        super.onDestroy();
        this.storeList = null;

        try {
            this.dao.close();
            this.dao = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
