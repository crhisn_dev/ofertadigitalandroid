package com.glam.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.glam.R;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.ui.CardAdapterForCategory;
import com.glam.utils.MyLogger;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.persistence.StoreInfo;

public class StoreSelectDialog {

    private DecimalFormat n_br;

    // private CardAdapterForCategory.CardCallback callback;
    private RecyclerView storeList;

    private StoreListAdapter.ClickListener listener;
    private Dialog dialog;

    public StoreSelectDialog() {
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");
        this.listener = listener;

        // this.callback = callback;
    }

    public void setListener(StoreListAdapter.ClickListener listener) {
        this.listener = listener;
    }


    public void show(final Activity activity) {
        this.dialog = new Dialog(activity);
        //dialog.setTitle("Quantidade do Produto");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.store_select_layout);
        dialog.setCancelable(false);


        this.storeList = (RecyclerView) dialog.findViewById(R.id.storeList);

        this.storeList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.storeList.setLayoutManager(llm);


        List<StoreInfo> stores = new ArrayList<>();

        StoreSelectDialog.StoreListAdapter ca = new StoreSelectDialog.StoreListAdapter(activity, this.storeList, stores, this.listener);
        this.storeList.setAdapter(ca);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            private List<StoreInfo> stores;

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    this.stores = OfertaDigitalRestClient.getRef().findStores();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (this.stores != null) {
                    StoreSelectDialog.StoreListAdapter ca = new StoreSelectDialog.StoreListAdapter(activity, storeList, this.stores, listener);
                    storeList.setAdapter(ca);
                }
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        try {
            stores = OfertaDigitalRestClient.getRef().findStores();
        } catch (Exception e) {
            MyLogger.log(e);
        }


        dialog.show();
    }


    public void close() {
        this.dialog.dismiss();
    }


    public static class StoreListAdapter extends
            RecyclerView.Adapter<StoreSelectDialog.StoreListAdapter.CardViewHolder> {

        private List<StoreInfo> storeInfoList;
        private ClickListener listener;
        private RecyclerView recyclerView;
        private Activity activity;

        public StoreListAdapter(Activity activity, RecyclerView recyclerView, List<StoreInfo> storeList, ClickListener listener) {
            this.storeInfoList = storeList;
            this.listener = listener;
            this.recyclerView = recyclerView;
            this.activity = activity;
        }

        public List<StoreInfo> getStoreInfoList() {
            return storeInfoList;
        }

        public void setStoreInfoList(List<StoreInfo> storeInfoList) {
            this.storeInfoList = storeInfoList;
        }

        @Override
        public int getItemCount() {
            return storeInfoList.size();
        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder, int)
         */
        @Override
        public void onBindViewHolder(StoreSelectDialog.StoreListAdapter.CardViewHolder vh, final int i) {
            final StoreInfo item = storeInfoList.get(i);
            vh.lblName.setText(item.getNomeFantasia());
            vh.lblDesc.setText(item.getDescription());
            if (item.getAddress() != null) {
                vh.lblAddr.setText(item.getAddress().getXLgr());
            }
        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onCreateViewHolder(android.view.ViewGroup, int)
         */
        @Override
        public StoreSelectDialog.StoreListAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(this.activity)
                    .inflate(R.layout.store_item, viewGroup, false);

            /*if (itemView instanceof CardView) {
                MyLogger.info("inside cardView");
                CardView card = (CardView) itemView;

                int shaddowSize = 10;
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                card.setMaxCardElevation(10f);
                params.setMargins(shaddowSize, shaddowSize, shaddowSize,
                        shaddowSize);

                card.setCardElevation(shaddowSize);
                card.setLayoutParams(params);
            }*/

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int position = recyclerView.getChildPosition(v);
                    StoreInfo storeInfo = storeInfoList.get(position);

                    if (listener != null) {
                        listener.process(storeInfo);
                    }
                }
            });

            return new StoreSelectDialog.StoreListAdapter.CardViewHolder(itemView);
        }

        public void clearAdapter() {
            this.storeInfoList = null;
        }

        public class CardViewHolder extends RecyclerView.ViewHolder {
            protected TextView lblName, lblDesc, lblAddr;

            protected ImageView img;

            public CardViewHolder(View v) {
                super(v);
                lblName = (TextView) v.findViewById(R.id.lblStoreName);
                lblDesc = (TextView) v.findViewById(R.id.lblStoreDesc);
                lblAddr = (TextView) v.findViewById(R.id.lblStoreAddr);

            }
        }

        public interface ClickListener {
            public void process(StoreInfo info);
        }
    }


}
