package com.glam.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.glam.R;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.utils.MyLogger;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.ItemProductQuantityInfo;
import br.com.innove.ofertadigital.persistence.OrderInfo;
import br.com.innove.ofertadigital.persistence.StoreInfo;

public class ProductListDialog {

    private List<ItemProductQuantityInfo> products;
    private DecimalFormat n_br;

    private RecyclerView productList;
    private Dialog dialog;

    public ProductListDialog(OrderInfo orderInfo) {
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");
        this.products = orderInfo.getItemsProduct();
    }


    public void show(final Activity activity) {
        this.dialog = new Dialog(activity);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.product_list_layout);
        dialog.setCancelable(false);

        this.productList = (RecyclerView) dialog.findViewById(R.id.productList);

        this.productList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.productList.setLayoutManager(llm);

        ProductListDialog.ProductListAdapter ca = new ProductListDialog.ProductListAdapter(this.productList, this.products);
        this.productList.setAdapter(ca);


        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }


    public void close() {
        this.dialog.dismiss();
    }


    public static class ProductListAdapter extends
            RecyclerView.Adapter<ProductListDialog.ProductListAdapter.CardViewHolder> {

        private DecimalFormat n_br;
        private List<ItemProductQuantityInfo> products;
        private RecyclerView recyclerView;


        public ProductListAdapter(RecyclerView recyclerView, List<ItemProductQuantityInfo> products) {
            this.products = products;
            this.recyclerView = recyclerView;

            this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
            this.n_br.applyPattern("###,###,##0.00");

        }

        @Override
        public int getItemCount() {
            return products.size();
        }

        @Override
        public void onBindViewHolder(ProductListDialog.ProductListAdapter.CardViewHolder vh, final int i) {
            final ItemProductQuantityInfo item = products.get(i);
            vh.lbl2.setText(item.getBuyProduct().getName());

            BigDecimal priceValue = item.getBuyProduct().getCurrentPrice().getValue1();
            double buyQtde = item.getBuyQuantity().getBuyQuantity();

            priceValue = priceValue.multiply(new BigDecimal(buyQtde));

            vh.lbl3.setText("R$ " + this.n_br.format(priceValue));
        }


        @Override
        public ProductListDialog.ProductListAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.cart_product_item, viewGroup, false);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int position = recyclerView.getChildPosition(v);

                }
            });

            return new ProductListDialog.ProductListAdapter.CardViewHolder(itemView);
        }

        public void clearAdapter() {
            this.products = null;
        }

        public class CardViewHolder extends RecyclerView.ViewHolder {
            protected TextView btnAdd, lbl2, lbl3;

            protected ImageView img;

            public CardViewHolder(View v) {
                super(v);
                img = (ImageView) v.findViewById(R.id.img);
                img.setVisibility(View.GONE);

                btnAdd = (TextView) v.findViewById(R.id.btnAdd);
                btnAdd.setVisibility(View.GONE);

                lbl2 = (TextView) v.findViewById(R.id.lbl2);
                lbl3 = (TextView) v.findViewById(R.id.lbl3);

            }
        }

    }


}
