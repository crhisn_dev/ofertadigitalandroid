package com.glam.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.glam.R;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.transport.CEPInfo;
import com.glam.utils.MyLogger;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.AddressInfo;
import br.com.innove.ofertadigital.persistence.StoreInfo;

public class AddressRegisterDialog {

    private Dialog dialog;

    private AddressInfo addressInfo;
    private AddressListener listener;

    private EditText textName;
    private EditText textBairro;
    private EditText textNumero;
    private EditText textCidade;
    private EditText textCEP;

    public AddressRegisterDialog(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
    }

    public void show(final Activity activity) {
        this.dialog = new Dialog(activity);
        //dialog.setTitle("Quantidade do Produto");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.delivery_register_layout);

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.FILL_PARENT;
        params.width = WindowManager.LayoutParams.FILL_PARENT;
        dialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        dialog.setCancelable(false);

        this.textCEP = (EditText) dialog.findViewById(R.id.textCEP);
        textCEP.setCursorVisible(true);

        textCEP.requestFocus();

        this.textName = (EditText) dialog.findViewById(R.id.textName);
        this.textBairro = (EditText) dialog.findViewById(R.id.textBairro);
        this.textNumero = (EditText) dialog.findViewById(R.id.textNumero);
        this.textCidade = (EditText) dialog.findViewById(R.id.textCidade);

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toBean();
                if (listener != null) {
                    listener.process(addressInfo);
                }
            }
        });


        textCEP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                final String cepStr = textCEP.getText().toString();

                if (cepStr.length() == 8) {
                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                        private CEPInfo cepinfo;

                        @Override
                        protected Void doInBackground(Void... voids) {

                            String cep = cepStr.replace("-", "");
                            cep = cep.replace(".", "");
                            cep = cep.replace(" ", "");

                            try {
                                cepinfo = OfertaDigitalRestClient.getRef().findCEP(cep);
                            } catch (Exception e) {
                                MyLogger.log(e);
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            if (cepinfo != null) {
                                textName.setText(cepinfo.getLogradouro());
                                textBairro.setText(cepinfo.getBairro());
                                textCidade.setText(cepinfo.getLocalidade());

                                textNumero.requestFocus();
                            } else {
                                textCEP.setError("CEP invalido ou não encontrado");
                            }
                        }
                    };
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        toControls();

        dialog.show();
    }

    public void toControls() {
        this.textName.setText(this.addressInfo.getAddress().getXLgr());
        this.textBairro.setText(this.addressInfo.getAddress().getXBairro());
        this.textNumero.setText(this.addressInfo.getAddress().getNro());
        this.textCEP.setText(this.addressInfo.getAddress().getCEP());

    }

    public void toBean() {
        this.addressInfo.getAddress().setXLgr(this.textName.getText().toString());
        this.addressInfo.getAddress().setXBairro(this.textBairro.getText().toString());
        this.addressInfo.getAddress().setNro(this.textNumero.getText().toString());
        this.addressInfo.getAddress().setCEP(this.textCEP.getText().toString());
    }


    public void close() {
        this.dialog.dismiss();
    }


    public void setListener(AddressListener listener) {
        this.listener = listener;
    }

    public static interface AddressListener {
        public void process(AddressInfo addressInfo);
    }


}
