package com.glam.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.glam.R;
import com.glam.ui.CardAdapterForCategory;
import com.glam.utils.MyLogger;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.persistence.UnitInfo;

public class BuyQtdeDialog {

    private DecimalFormat n_br;
    private ProductInfo product;
    private QuantityOrder qtde;
    private CardAdapterForCategory.CardCallback callback;
    private Bitmap imagebitmap;


    public BuyQtdeDialog(ProductInfo product, QuantityOrder qtde, CardAdapterForCategory.CardCallback callback, Bitmap imagebitmap) {
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");

        this.product = product;
        this.qtde = qtde;
        this.callback = callback;
        this.imagebitmap = imagebitmap;
    }


    public void updateSubtotal(int i1, TextView subTotal) {
        try {
            BigDecimal priceValue = product.getCurrentPrice().getValue1();
            priceValue = priceValue.multiply(new BigDecimal(i1));

            subTotal.setText("Subtotal:   R$ " + n_br.format(priceValue));

            qtde.setBuyPrice(product.getCurrentPrice());
            qtde.setBuyQuantity(Double.parseDouble(i1 + ""));
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    public void show(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        //dialog.setTitle("Quantidade do Produto");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.picker_layout);
        dialog.setCancelable(false);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        ImageView img = (ImageView) dialog.findViewById(R.id.img);
        if (this.imagebitmap != null) {
            img.setImageBitmap(this.imagebitmap);
        }

        TextView productName = (TextView) dialog.findViewById(R.id.textNameProduct);
        productName.setText(product.getName());

        UnitInfo unit = product.getCurrentPrice() != null ? product.getCurrentPrice().getUnit() : null;

        if (unit != null) {
            MyLogger.info("unit: " + unit);
            MyLogger.info("unit info: " + unit.getUnitName());
        } else {
            MyLogger.info("setting default unit");
            unit = new UnitInfo();
            unit.setUnitName("KG");
        }

        final TextView subTotal = (TextView) dialog.findViewById(R.id.subtotal);
        if (product.getCurrentPrice() != null) {
            subTotal.setText("   Subtotal: R$ " + n_br.format(product.getCurrentPrice().getValue1()));
        }

        {
            TextView price = (TextView) dialog.findViewById(R.id.textPrice);
            if (product.getCurrentPrice() != null) {
                price.setText("R$ " + n_br.format(product.getCurrentPrice().getValue1()) + (unit != null ? " / " + unit.getUnitName() : ""));
            }
        }

        {
            if (unit != null && ("UN".equals(unit.getUnitName().toUpperCase()) || "UM".equals(unit.getUnitName().toUpperCase()))) {
                final NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.numberPicker1);
                numberPicker.setVisibility(View.VISIBLE);

                setNumberPickerTextColor(numberPicker, Color.BLACK);
                numberPicker.setMaxValue(100);
                numberPicker.setMinValue(1);
                numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                        updateSubtotal(i1, subTotal);
                    }
                });


                if (qtde.getBuyQuantity() != 0d) {
                    MyLogger.info("qtde.getBuyQuantity(): " + qtde.getBuyQuantity());
                    numberPicker.setValue((int) qtde.getBuyQuantity());

                } else {
                    qtde.setBuyQuantity(1d);
                    numberPicker.setValue(1);
                }

                updateSubtotal((int) qtde.getBuyQuantity(), subTotal);

                numberPicker.requestFocus();
            }
        }
        {
            if (unit != null && ("KG".equals(unit.getUnitName().toUpperCase()) || "GR".equals(unit.getUnitName().toUpperCase()))) {
                final TextView textQtde = (TextView) dialog.findViewById(R.id.textQteGr);
                textQtde.setVisibility(View.VISIBLE);

                if (qtde.getBuyQuantity() != 0) {
                    textQtde.setText(qtde.getBuyQuantity() + "");
                }

                textQtde.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        try {
                            BigDecimal priceValue = product.getCurrentPrice().getValue1();
                            priceValue = priceValue.multiply(new BigDecimal(textQtde.getText().toString()));

                            subTotal.setText("Subtotal:   R$ " + n_br.format(priceValue));

                            qtde.setBuyPrice(product.getCurrentPrice());
                            qtde.setBuyQuantity(Double.parseDouble(textQtde.getText().toString()));
                        } catch (Exception e) {
                            MyLogger.log(e);
                        }
                    }
                });

                textQtde.requestFocus();
            }
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        final UnitInfo unit2 = unit;
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    if (unit2 != null && ("KG".equals(unit2.getUnitName().toUpperCase()) || "GR".equals(unit2.getUnitName().toUpperCase()))) {
                        MyDialogs.showInformation(activity, "Adicionar Produto", "Pode existir uma divergencia nos valores cobrados por causa do peso do produto", new MyDialogs.MyDialogListener() {
                            @Override
                            public void processClose() {
                                if (callback != null) {
                                    try {
                                        callback.process(product, qtde);
                                    } catch (Exception e) {
                                        MyLogger.log(e);
                                    }
                                }

                                dialog.dismiss();
                            }
                        });
                    } else {
                        if (callback != null) {
                            callback.process(product, qtde);
                        }

                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    MyLogger.log(e);
                }
            }
        });


        dialog.show();
    }

    public boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {

                } catch (IllegalAccessException e) {

                } catch (IllegalArgumentException e) {

                }
            }
        }
        return false;
    }
}
