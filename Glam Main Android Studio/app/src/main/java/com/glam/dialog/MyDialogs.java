package com.glam.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.widget.TextView;

import com.glam.R;

public class MyDialogs {

    public static void showError(Context context, String title, String message) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyleRed).create();
        dialog.setTitle(title);
        dialog.setMessage(message);

        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.show();
    }

    public static void showConfirm(Context context, String title, String message, final Callback callback) {
        new AlertDialog.Builder(context, R.style.MyAlertDialogStyleBlue)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(title)
                .setMessage(message)

                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        callback.processNo();
                    }
                })
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.processYes();
                    }

                })
                .show();
    }

    public static interface MyDialogListener {
        public void processClose();
    }

    public static void showInformation(Context context, String title, String message, final MyDialogListener listener) {
        new AlertDialog.Builder(context, R.style.MyAlertDialogStyleBlue)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null) {
                            listener.processClose();
                        }
                    }

                })
                .show();
    }

    public static void showInformationCenter(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyAlertDialogStyleBlue);


        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }

        });
        AlertDialog dialog = builder.show();

        TextView messageView = (TextView) dialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }

    public static interface Callback {
        public void processYes();

        public void processNo();
    }
}
