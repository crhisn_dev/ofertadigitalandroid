package com.glam.buycart;

import java.util.Hashtable;
import java.util.List;

import br.com.innove.ofertadigital.persistence.ItemId;
import br.com.innove.ofertadigital.persistence.ItemProductQuantityInfo;
import br.com.innove.ofertadigital.persistence.OrderInfo.ORDER_TYPE;
import br.com.innove.ofertadigital.persistence.ProductId;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.persistence.UserInfo;

public interface BuyCartEJBLocal {

	public ItemId addProduct(ItemId itemId, ProductId product, QuantityOrder quantity) throws Exception;

	public List<ItemProductQuantityInfo> listCurrentItems() throws Exception;

	public void removeProduct(ItemId itemId) throws Exception;

	public void changeQuantity(ItemId itemId, QuantityOrder quantity) throws Exception;

	public void setUser(UserInfo user);

	public void initCurrentOrder() throws Exception;

	public void clearBuyCart() throws Exception;

	public void finishBuy(Hashtable<String, Object> params) throws Exception;

	public void deleteCart() throws Exception;

}
