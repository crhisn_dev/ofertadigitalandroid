package com.glam.buycart;

import br.com.innove.ofertadigital.persistence.ItemId;
import br.com.innove.ofertadigital.persistence.ProductId;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;

/**
 * Created by Administrador on 04/05/17.
 */

public class ItemProductIdQtde {

    private ItemId itemId;
    private ProductInfo product;
    private QuantityOrder qtde;

    public ItemId getItemId() {
        return itemId;
    }

    public void setItemId(ItemId itemId) {
        this.itemId = itemId;
    }


    public QuantityOrder getQtde() {
        return qtde;
    }

    public void setQtde(QuantityOrder qtde) {
        this.qtde = qtde;
    }

    public ProductInfo getProduct() {
        return product;
    }

    public void setProduct(ProductInfo product) {
        this.product = product;
    }
}
