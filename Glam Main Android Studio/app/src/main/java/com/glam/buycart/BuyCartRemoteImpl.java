package com.glam.buycart;


import android.os.AsyncTask;

import com.glam.http.OfertaDigitalRestClient;
import com.glam.transport.LocalConvenioInfo;
import com.glam.transport.ProductListRequest;
import com.glam.transport.SyncBuyCartResquest;
import com.glam.utils.MyLogger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import br.com.innove.ofertadigital.persistence.AddressInfo;
import br.com.innove.ofertadigital.persistence.ItemId;
import br.com.innove.ofertadigital.persistence.ItemProductQuantityInfo;
import br.com.innove.ofertadigital.persistence.PaymentInfo;
import br.com.innove.ofertadigital.persistence.ProductId;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.persistence.UserInfo;

public class BuyCartRemoteImpl implements BuyCartEJBLocal {

    private static BuyCartRemoteImpl instance;

    private OfertaDigitalRestClient http;
    private BigDecimal totalBuy;
    private boolean errorSent;

    public static BuyCartRemoteImpl getRef() {
        if (instance == null) {
            try {
                instance = new BuyCartRemoteImpl();
            } catch (Exception e) {
                MyLogger.log(e);
            }
        }
        return instance;
    }

    public static void refClear() {
        instance = null;
    }


    private List<ItemProductIdQtde> itemList;

    private BuyCartRemoteImpl() throws Exception {
        this.itemList = new ArrayList<>();
        this.http = OfertaDigitalRestClient.getRef();
        this.errorSent = false;
    }

    public void createBuyCart() throws Exception {
        this.http.createNewBuyCart();
    }

    private ItemId generateItemId() {
        ItemId itemId = new ItemId();
        itemId.setItemId("item" + UUID.randomUUID().toString());
        itemId.setUserId(OfertaDigitalRestClient.getRef().getUserId());
        return itemId;
    }


    public void calculateTotal() {
        BigDecimal total = new BigDecimal(0);
        for (ItemProductIdQtde item : this.itemList) {
            QuantityOrder qtde = item.getQtde();

            BigDecimal priceItem = qtde.getBuyPrice().getValue1();
            priceItem = priceItem.multiply(new BigDecimal(qtde.getBuyQuantity()));

            total = total.add(priceItem);
        }


        this.totalBuy = total;
    }

    public BigDecimal getTotalBuy() {
        return totalBuy;
    }

    public int totalItens() {
        return this.itemList.size();
    }

    public ItemId addProduct(ProductInfo product, QuantityOrder quantity) throws Exception {
        ItemProductIdQtde qtde = new ItemProductIdQtde();

        ItemId itemId = this.generateItemId();

        qtde.setItemId(itemId);
        qtde.setProduct(product);
        qtde.setQtde(quantity);

        // this.addProduct(itemId, product.getProdId(), quantity);

        ItemProductIdQtde item = new ItemProductIdQtde();
        item.setQtde(quantity);
        item.setItemId(itemId);
        item.setProduct(product);

        this.itemList.add(item);

        return qtde.getItemId();
    }

    private AddressInfo address;

    public void addAddress(AddressInfo address) {
        this.address = address;
    }

    public AddressInfo getAddress() {
        return address;
    }

    private PaymentInfo payInfo;

    private LocalConvenioInfo convenio;

    public LocalConvenioInfo getConvenio() {
        return convenio;
    }

    public void setConvenio(LocalConvenioInfo convenio) {
        this.convenio = convenio;
    }

    public void setPayInfo(PaymentInfo payInfo) {
        this.payInfo = payInfo;
    }

    public PaymentInfo getPayInfo() {
        return payInfo;
    }

    @Override
    public ItemId addProduct(ItemId itemId, final ProductId product, final QuantityOrder quantity) throws Exception {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    http.addProductToBuyCart(product, quantity);
                } catch (Exception e) {
                    errorSent = true;
                    MyLogger.log(e);
                }
                return null;
            }
        };
        task.execute();
        return itemId;
    }

    public List<ItemProductIdQtde> getItemList() {
        return itemList;
    }

    @Override
    public List<ItemProductQuantityInfo> listCurrentItems() throws Exception {
        return null;
    }

    @Override
    public void removeProduct(ItemId itemId) throws Exception {

    }

    @Override
    public void changeQuantity(ItemId itemId, QuantityOrder quantity) throws Exception {

    }

    @Override
    public void setUser(UserInfo user) {

    }

    @Override
    public void initCurrentOrder() throws Exception {

    }

    @Override
    public void clearBuyCart() throws Exception {
        this.itemList = new ArrayList<>();
    }

    @Override
    public void finishBuy(Hashtable<String, Object> params) throws Exception {
        List<ProductListRequest.SyncItem> syncItens = new ArrayList<>();

        for (ItemProductIdQtde item : this.itemList) {

            ProductListRequest.SyncItem syncitem = new ProductListRequest.SyncItem();
            syncitem.setItemId(item.getItemId());
            syncitem.setProductId(item.getProduct().getProdId());
            syncitem.setQuantity(item.getQtde());

            syncItens.add(syncitem);
        }

        if (this.convenio != null) {
            this.payInfo.setC_b(this.convenio.getId());
        }

        this.http.syncBuyCart(syncItens);
        this.http.finalizeBuy(this.address, this.payInfo);

    }

    @Override
    public void deleteCart() throws Exception {

    }
}
