package com.glam;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.glam.buycart.BuyCartRemoteImpl;
import com.glam.custom.CustomActivity;
import com.glam.dialog.BuyQtdeDialog;
import com.glam.ui.CardAdapterForCategory;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;

/**
 * The Activity ProductDetail is launched when user select a product item from
 * product list or grid views in other sections of the app. Currently it shows
 * Dummy details of product with dummy pics. You need to write your own code to
 * load and display actual contents.
 */
public class ProductDetail extends CustomActivity {

    /**
     * The pager.
     */
    private ViewPager pager;

    /**
     * The view that hold dots.
     */
    private LinearLayout vDots;
    private ProductInfo product;

    private ImageCache cache;

    private DecimalFormat n_br;

    public ProductDetail() {
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.cache = ImageCache.getInstance(this);

        this.product = (ProductInfo) getIntent().getSerializableExtra("product");

        setContentView(R.layout.pr_detail);

        setupView();

        TextView textNameProduct = (TextView) this.findViewById(R.id.lbl_name_product);
        textNameProduct.setText(this.product.getName());


        TextView textPriceProduct = (TextView) this.findViewById(R.id.lbl_price_product);
        textPriceProduct.setText("R$ " + n_br.format(product.getCurrentPrice().getValue1()));

        Button btnCart = (Button) this.findViewById(R.id.fabCart);
        // btnCart.setVisibility(View.GONE);


        ImageView imageView = (ImageView) this.findViewById(R.id.imageView_recipe);

        byte[] imageBytes = this.getImage(this.product);

        Bitmap tmp_bitmap = null;

        if (imageBytes != null) {
            Bitmap imagebitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

           /* if (imagebitmap.getHeight() > 250) {
                double scale = 250d / imagebitmap.getHeight();
                MyLogger.info("w: " + imagebitmap.getWidth() + " h: " + imagebitmap.getHeight() + " scale: " + scale);
                imagebitmap = Bitmap.createScaledBitmap(imagebitmap, (int) (imagebitmap.getWidth() * scale), (int) (imagebitmap.getHeight() * scale), true);

                tmp_bitmap = imagebitmap;
            }*/

            imageView.setImageBitmap(imagebitmap);
        } else {
            imageView.setImageResource(R.drawable.imagem_nao_encontrada);
        }

        final Bitmap final_bitmap = tmp_bitmap;
        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final QuantityOrder qtde = new QuantityOrder();

                qtde.setBuyPrice(product.getCurrentPrice());
                // qtde.setBuyQuantity(Double.parseDouble("1"));

                BuyQtdeDialog dialog = new BuyQtdeDialog(product, qtde, new CardAdapterForCategory.CardCallback() {
                    @Override
                    public void process(ProductInfo prod, QuantityOrder qtde) throws Exception {
                        BuyCartRemoteImpl.getRef().addProduct(product, qtde);
                        ProductDetail.this.finish();
                    }
                }, final_bitmap);
                dialog.show(ProductDetail.this);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    private byte[] getImage(ProductInfo prod) {
        MyLogger.info("prod: " + prod.getName());
        List<MediaDesc> medias = (prod.getMediasList() == null ? new ArrayList<MediaDesc>() : prod.getMediasList());
        for (MediaDesc media : medias) {
            MyLogger.info("media: " + media.getNativeimage_id());
            if (MediaDesc.MEDIA_TYPE.IMAGE.equals(media.getType()) && media.getNativeimage_id() != null) {
                return cache.getImage(media.getNativeimage_id());
            }
        }

        return null;
    }

    /**
     * Setup the click & other events listeners for the view components of this
     * screen. You can add your logic for Binding the data to TextViews and
     * other views as per your need.
     */
    private void setupView() {
        setTouchNClick(R.id.fabCart);
        setTouchNClick(R.id.btnLike);
        setTouchNClick(R.id.btnComment);
        setTouchNClick(R.id.btnMore);

        getSupportActionBar().setTitle("Detalhes do Produto");

        initPager();
    }

    /**
     * Inits the pager view.
     */
    private void initPager() {
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setPageMargin(10);

        pager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                if (vDots == null || vDots.getTag() == null)
                    return;
                ((ImageView) vDots.getTag())
                        .setImageResource(R.drawable.dot_gray);
                ((ImageView) vDots.getChildAt(pos))
                        .setImageResource(R.drawable.dot_blue);
                vDots.setTag(vDots.getChildAt(pos));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        vDots = (LinearLayout) findViewById(R.id.vDots);

        pager.setAdapter(new PageAdapter());
        setupDotbar();
    }

    /**
     * Setup the dotbar to show dots for pages of view pager with one dot as
     * selected to represent current page position.
     */
    private void setupDotbar() {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        param.setMargins(10, 0, 0, 0);
        vDots.removeAllViews();
        for (int i = 0; i < 5; i++) {
            ImageView img = new ImageView(this);
            img.setImageResource(i == 0 ? R.drawable.dot_blue
                    : R.drawable.dot_gray);
            vDots.addView(img, param);
            if (i == 0) {
                vDots.setTag(img);
            }

        }
    }

    /**
     * The Class PageAdapter is adapter class for ViewPager and it simply holds
     * a Single image view with dummy images. You need to write logic for
     * loading actual images.
     */
    private class PageAdapter extends PagerAdapter {

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#getCount()
         */
        @Override
        public int getCount() {
            return 5;
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#instantiateItem(android.view.ViewGroup, int)
         */
        @Override
        public Object instantiateItem(ViewGroup container, int arg0) {
            final ImageView img = (ImageView) getLayoutInflater().inflate(
                    R.layout.img, null);

            // teste
            if (arg0 % 2 == 0) {
                img.setImageResource(R.drawable.logo_acougue);
            } else {
                img.setImageResource(R.drawable.logo_paes);
            }

            container.addView(img,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT);

            return img;
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#destroyItem(android.view.ViewGroup, int, java.lang.Object)
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            try {
                // super.destroyItem(container, position, object);
                // if(container.getChildAt(position)!=null)
                // container.removeViewAt(position);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#isViewFromObject(android.view.View, java.lang.Object)
         */
        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.cart, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* (non-Javadoc)
     * @see com.glam.custom.CustomActivity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_cart)
            startActivity(new Intent(this, CheckoutActivity.class));
        return super.onOptionsItemSelected(item);
    }
}
