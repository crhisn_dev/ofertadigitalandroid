package com.glam.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.glam.CheckoutActivity;
import com.glam.MainActivity;
import com.glam.ProductDetail;
import com.glam.R;
import com.glam.custom.CustomFragment;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.model.Data;
import com.glam.transport.CEPInfo;
import com.glam.utils.MyLogger;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class Checkout is the fragment that shows the list products for checkout
 * and show the credit card details as well. You need to load and display actual
 * contents.
 */
public class DeliveryAddressFragment extends CustomFragment {

    /**
     * The product list.
     */
    private ArrayList<Data> iList;

    private View mainView;

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @SuppressLint({"InflateParams", "InlinedApi"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.mainView = inflater.inflate(R.layout.delivery_register_layout, null);

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).toolbar.setTitle("Carrinho de Compras");
            ((MainActivity) getActivity()).toolbar.findViewById(
                    R.id.spinner_toolbar).setVisibility(View.GONE);
        } else {
            ((CheckoutActivity) getActivity()).getSupportActionBar().setTitle(
                    "Checkout");
        }

        final EditText textCEP = (EditText) this.mainView.findViewById(R.id.textCEP);
        textCEP.setCursorVisible(true);

        textCEP.requestFocus();

        final EditText textName = (EditText) this.mainView.findViewById(R.id.textName);

        InputMethodManager imm = (InputMethodManager) this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(textName, InputMethodManager.SHOW_IMPLICIT);

        final EditText textBairro = (EditText) this.mainView.findViewById(R.id.textBairro);
        final EditText textNumero = (EditText) this.mainView.findViewById(R.id.textNumero);
        final EditText textCidade = (EditText) this.mainView.findViewById(R.id.textCidade);

        textCEP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                final String cepStr = textCEP.getText().toString();

                if (cepStr.length() == 8) {
                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                        private CEPInfo cepinfo;

                        @Override
                        protected Void doInBackground(Void... voids) {

                            String cep = cepStr.replace("-", "");
                            cep = cep.replace(".", "");
                            cep = cep.replace(" ", "");

                            try {
                                cepinfo = OfertaDigitalRestClient.getRef().findCEP(cep);
                            } catch (Exception e) {
                                MyLogger.log(e);
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            if (cepinfo != null) {
                                textName.setText(cepinfo.getLogradouro());
                                textBairro.setText(cepinfo.getBairro());
                                textCidade.setText(cepinfo.getLocalidade());

                                textNumero.requestFocus();
                            } else {
                                textCEP.setError("CEP invalido ou não encontrado");
                            }
                        }
                    };
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        setTouchNClick(this.mainView.findViewById(R.id.btnConfirm));
        setHasOptionsMenu(true);
        setupView(this.mainView);
        return this.mainView;
    }


    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /* (non-Javadoc)
     * @see com.whatshere.custom.CustomFragment#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    /**
     * Setup the view components for this fragment. You write your code for
     * initializing the views, setting the adapters, touch and click listeners
     * etc.
     *
     * @param v the base view of fragment
     */
    private void setupView(View v) {
        loadDummyData();
    }


    private void loadDummyData() {
        ArrayList<Data> al = new ArrayList<Data>();
        al.add(new Data(new String[]{"Y.M.C. Spray Tee (Grey)", "$67",
                "Oi Polloi"}, new int[]{R.drawable.checking_img1}));
        al.add(new Data(new String[]{"Ally Capellino", "$94",
                "Ally Capellino"}, new int[]{R.drawable.checking_img2}));

        iList = new ArrayList<Data>(al);
        iList.addAll(al);
        iList.addAll(al);
        iList.addAll(al);
        iList.addAll(al);
        iList.addAll(al);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.cart, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onDestroyView() {
        MyLogger.info("onDestroyView: " + this);
        super.onDestroyView();

        try {
            this.unbindDrawables(this.mainView);
        } catch (Exception e) {
            MyLogger.log(e);
        }


        this.mainView = null;
    }

    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }
}
