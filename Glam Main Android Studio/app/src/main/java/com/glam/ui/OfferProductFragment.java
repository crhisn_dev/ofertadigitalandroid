package com.glam.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.glam.CheckoutActivity;
import com.glam.MainActivity;
import com.glam.ProductDetail;
import com.glam.R;
import com.glam.buycart.BuyCartRemoteImpl;
import com.glam.custom.CustomFragment;
import com.glam.dialog.BuyQtdeDialog;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.transport.ImagesArrayResponseData;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.NativeImage;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.ProductInfoNoEntity;
import br.com.innove.ofertadigital.persistence.PromotionInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;

public class OfferProductFragment extends CustomFragment {


    private DecimalFormat n_br;
    private RecyclerView recList;
    private ImageCache cache;
    private View mainView;
    private LinearLayout linlaHeaderProgress;

    public OfferProductFragment() {
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.mainView == null) {
            this.mainView = inflater.inflate(R.layout.pager_card_view, null);

            this.cache = ImageCache.getInstance(OfferProductFragment.this.getActivity());

            if (getActivity() instanceof MainActivity) {
                ((MainActivity) getActivity()).toolbar.setTitle("Oferta do Dia");
                ((MainActivity) getActivity()).toolbar.findViewById(
                        R.id.spinner_toolbar).setVisibility(View.GONE);
            } else {
                ((CheckoutActivity) getActivity()).getSupportActionBar().setTitle(
                        "Checkout");
            }


            this.linlaHeaderProgress = (LinearLayout) this.mainView.findViewById(R.id.linlaHeaderProgress);

		/*if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		}*/

            // setTouchNClick(mainView.findViewById(R.id.btnDone));
            setHasOptionsMenu(true);

            MyLogger.info("onCreateView: " + this);

            setupView(mainView);
        } else {
            this.refreshPromotions();
        }


        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLogger.info("onResume: " + this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_exp, menu);
        menu.findItem(R.id.menu_refresh).setVisible(true);
        menu.findItem(R.id.menu_buy).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);

        // minha ultima
        MenuItem menu_buy = menu.findItem(R.id.menu_buy);
        menu_buy.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setupContainer(1);

                return true;
            }
        });

        menu_buy.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setupContainer(1);

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                this.refreshPromotions();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        MyLogger.info("onDestroyView: " + this);
        super.onDestroyView();

        try {
            this.unbindDrawables(this.mainView);
        } catch (Exception e) {
            MyLogger.log(e);
        }

        this.recList = null;

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    private void setupView(View v) {


        this.recList = (RecyclerView) v.findViewById(R.id.cardList);
        this.recList.setHasFixedSize(true);
        this.recList.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.recList.setLayoutManager(llm);

        final OfferProductFragment.OffeProductAdapter ca = new OfferProductFragment.OffeProductAdapter(new ArrayList<PromotionInfo>());
        this.recList.setAdapter(ca);

        this.refreshPromotions();

    }

    private void refreshPromotions() {
        final Intent intent = OfferProductFragment.this.getActivity().getIntent();

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            private List<PromotionInfo> promotions = new ArrayList<PromotionInfo>();
            private boolean append = false;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    List<ProductInfoNoEntity> products = new ArrayList<>();
                    if (intent.hasExtra("promotions")) {
                        this.promotions = (List<PromotionInfo>) intent.getSerializableExtra("promotions");
                        this.append = true;
                    } else {
                        WeakReference<List<PromotionInfo>> weakref = OfertaDigitalRestClient.getRef().findPromotionList();
                        this.promotions = weakref.get();
                        this.append = false;
                    }

                    for (PromotionInfo promo : this.promotions) {
                        products.add(promo.getProduct());
                    }

                    requestImages(products);
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (recList != null) {
                    //   final OfferProductFragment.OffeProductAdapter ca = new OfferProductFragment.OffeProductAdapter(this.promotions);
                    OffeProductAdapter adapter = ((OffeProductAdapter) recList.getAdapter());
                    for (PromotionInfo promo : this.promotions) {
                        if (!adapter.getPromotions().contains(promo)) {
                            adapter.getPromotions().add(0, promo);
                        }
                    }
                    adapter.notifyDataSetChanged();
                }

                linlaHeaderProgress.setVisibility(View.GONE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private byte[] getImage(ProductInfoNoEntity prod) {
        MyLogger.info("prod: " + prod.getName());
        List<MediaDesc> medias = (prod.getMediasList() == null ? new ArrayList<MediaDesc>() : prod.getMediasList());
        for (MediaDesc media : medias) {
            MyLogger.info("media: " + media.getNativeimage_id());
            if (MediaDesc.MEDIA_TYPE.IMAGE.equals(media.getType()) && media.getNativeimage_id() != null) {
                return cache.getImage(media.getNativeimage_id());
            }
        }

        return null;
    }


    private boolean mustBeDownload(MediaDesc media) {
        if (MediaDesc.MEDIA_TYPE.IMAGE.equals(media.getType()) &&
                media.getNativeimage_id() != null &&
                !this.cache.isCached(media.getNativeimage_id())) {
            return true;
        }

        return false;
    }

    public void requestImages(List<ProductInfoNoEntity> products) throws Exception {
        MyLogger.info("start requestImages");
        List<String> nativeImagesIds = new ArrayList<>();
        for (ProductInfoNoEntity prod : products) {
            MyLogger.info("trying cache product: " + prod.getName());
            List<MediaDesc> medias = (prod.getMediasList() == null ? new ArrayList<MediaDesc>() : prod.getMediasList());
            for (MediaDesc media : medias) {
                if (this.mustBeDownload(media) && !nativeImagesIds.contains(media.getNativeimage_id())) {
                    nativeImagesIds.add(media.getNativeimage_id());
                }
            }
        }

        ImagesArrayResponseData images = OfertaDigitalRestClient.getRef().requestImages(nativeImagesIds);
        Collection<NativeImage> nativeImages = images.getImages().values();
        for (NativeImage nativeImage : nativeImages) {
            MyLogger.info("caching: " + nativeImage.getId());
            this.cache.cacheImage(nativeImage);
        }

        MyLogger.info("end called requestImages");
    }

    private class OffeProductAdapter extends
            RecyclerView.Adapter<OfferProductFragment.OffeProductAdapter.CardViewHolder> {

        private List<PromotionInfo> promotions;

        public OffeProductAdapter(List<PromotionInfo> promotions) {
            this.promotions = promotions;
        }

        public List<PromotionInfo> getPromotions() {
            return promotions;
        }

        public void setPromotions(List<PromotionInfo> promotions) {
            this.promotions = promotions;
        }

        @Override
        public int getItemCount() {
            return this.promotions.size();
        }


        @Override
        public void onBindViewHolder(OfferProductFragment.OffeProductAdapter.CardViewHolder vh, int i) {

            final PromotionInfo promo = this.promotions.get(i);

            vh.lbl1.setText(promo.getProduct().getName());
            vh.img.requestLayout();
            vh.img.setImageResource(R.drawable.imagem_nao_encontrada);

            byte[] image = getImage(promo.getProduct());

            Bitmap tmp_bitmap = null;
            if (image != null) {
                Bitmap imagebitmap = BitmapFactory.decodeByteArray(image, 0, image.length);

                if (imagebitmap.getHeight() > 250) {
                    double scale = Double.valueOf("250") / imagebitmap.getHeight();
                    MyLogger.info("w: " + imagebitmap.getWidth() + " h: " + imagebitmap.getHeight() + " scale: " + scale);
                    imagebitmap = Bitmap.createScaledBitmap(imagebitmap, (int) (imagebitmap.getWidth() * scale), (int) (imagebitmap.getHeight() * scale), true);

                    tmp_bitmap = imagebitmap;
                }

                vh.img.setImageBitmap(imagebitmap);
            }

            WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            Point size = new Point();
            display.getSize(size);

            try {
                vh.lbl3.setText("R$ " + n_br.format(promo.getProduct().getCurrentPrice().getValue1()));
            } catch (Exception e) {
                MyLogger.info("erro: " + e.getLocalizedMessage());
                vh.lbl3.setText("R$ 0.00");
            }


            final Bitmap final_bitmap = tmp_bitmap;

            vh.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final QuantityOrder qtde = new QuantityOrder();

                    final ProductInfo product = new ProductInfo();
                    product.setProdId(promo.getProduct().getProdId());
                    product.setName(promo.getProduct().getName());
                    product.setCurrentPrice(promo.getProduct().getCurrentPrice());


                    qtde.setBuyPrice(product.getCurrentPrice());
                    qtde.setBuyQuantity(Double.parseDouble("1"));

                    BuyQtdeDialog dialog = new BuyQtdeDialog(product, qtde, new CardAdapterForCategory.CardCallback() {
                        @Override
                        public void process(ProductInfo prod, QuantityOrder qtde) throws Exception {
                            BuyCartRemoteImpl.getRef().addProduct(prod, qtde);
                        }
                    }, final_bitmap);
                    dialog.show(OfferProductFragment.this.getActivity());
                }
            });

            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent startIntent = new Intent(OfferProductFragment.this.getActivity(), ProductDetail.class);

                    ProductInfoNoEntity clone = promo.getProduct();

                    ProductInfo product = new ProductInfo();
                    product.setName(clone.getName());
                    product.setCurrentPrice(clone.getCurrentPrice());
                    product.setMediasList(clone.getMediasList());

                    startIntent.putExtra("product", product);
                    OfferProductFragment.this.startActivity(startIntent);
                }
            });


        }


        @Override
        public OfferProductFragment.OffeProductAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.offer_product_item, viewGroup, false);


            return new OfferProductFragment.OffeProductAdapter.CardViewHolder(itemView);
        }

        public class CardViewHolder extends RecyclerView.ViewHolder {

            protected TextView lbl1, lbl3;
            protected ImageView img;
            protected TextView btnAdd;

            public CardViewHolder(View v) {
                super(v);
                lbl1 = (TextView) v.findViewById(R.id.lbl1);
                lbl3 = (TextView) v.findViewById(R.id.lbl3);
                img = (ImageView) v.findViewById(R.id.img);
                btnAdd = (TextView) v.findViewById(R.id.btnAdd);
            }
        }
    }

}
