package com.glam.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.glam.CheckoutActivity;
import com.glam.JornalViewerActivity;
import com.glam.MainActivity;
import com.glam.R;
import com.glam.YoutubeFullscreenActivity;
import com.glam.custom.CustomFragment;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.innove.ofertadigital.persistence.JornalStoreInfo;
import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.StoreInfo;
import br.com.innove.ofertadigital.persistence.VideoPromoInfo;

public class VideosFragment extends CustomFragment {


    private RecyclerView recList;
    private View mainView;

    private StoreInfo storeInfo;
    private LinearLayout linlaHeaderProgress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();
        if (args != null && args.containsKey("storeinfo")) {
            this.storeInfo = (StoreInfo) args.getSerializable("storeinfo");
        }

        this.mainView = inflater.inflate(R.layout.pager_card_view, null);


        this.linlaHeaderProgress = (LinearLayout) this.mainView.findViewById(R.id.linlaHeaderProgress);


        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).toolbar.setTitle("Videos");
            ((MainActivity) getActivity()).toolbar.findViewById(
                    R.id.spinner_toolbar).setVisibility(View.GONE);
        } else {
            ((CheckoutActivity) getActivity()).getSupportActionBar().setTitle(
                    "Videos");
        }

		/*if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		}*/

        // setTouchNClick(v.findViewById(R.id.btnDone));
        setHasOptionsMenu(true);
        setupView(this.mainView);

        MyLogger.info("onCreateView: " + this);
        return this.mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLogger.info("onResume: " + this);
    }

    private void setupView(View v) {
        final Intent intent = VideosFragment.this.getActivity().getIntent();

        this.recList = (RecyclerView) v.findViewById(R.id.cardList);
        this.recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        this.recList.setLayoutManager(llm);
        this.recList.setAdapter(new VideoAdapter(new ArrayList<VideoPromoInfo>()));


        final String store_id = this.storeInfo.getId();

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            public List<VideoPromoInfo> videos;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    this.videos = OfertaDigitalRestClient.getRef().findVideos(store_id);
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (this.videos != null) {
                    VideosFragment.this.recList.setAdapter(new VideoAdapter(this.videos));
                }

                try {
                    Runtime.getRuntime().gc();
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                linlaHeaderProgress.setVisibility(View.GONE);
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    @Override
    public void onDestroyView() {
        MyLogger.info("onDestroyView: " + this);
        super.onDestroyView();

        try {
            this.unbindDrawables(this.mainView);
        } catch (Exception e) {
            MyLogger.log(e);
        }

        this.recList = null;
        this.mainView = null;

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    public String extractYoutubeId(String url) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if (matcher.find()) {
            return matcher.group();
        }

        return null;
    }

    private class VideoAdapter extends
            RecyclerView.Adapter<VideoAdapter.CardViewHolder> {

        private boolean isImageFitToScreen;

        private List<VideoPromoInfo> videos;

        public VideoAdapter(List<VideoPromoInfo> videos) {
            this.videos = videos;
        }

        @Override
        public int getItemCount() {
            return videos.size();
        }


        @Override
        public void onBindViewHolder(final VideoAdapter.CardViewHolder vh, int i) {

            final VideoPromoInfo video = this.videos.get(i);

            if (video.getVideoDesc() != null) {
                vh.lbl_desc_video.setText(video.getVideoDesc());
            }

            List<MediaDesc> list = video.getMediasList();
            if (list != null && !list.isEmpty()) {
                MediaDesc mediaDesc = list.get(0);
                String youtubeUrl = mediaDesc.getMediaURL();
                final String youtubeID = extractYoutubeId(youtubeUrl);

                vh.youTubeView.initialize("AIzaSyD1LUSHNFaTfgC4cOixha6kuhJaA5cxIeU", new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                        youTubeThumbnailLoader.setVideo(youtubeID);
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });

                vh.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(VideosFragment.this.getActivity(), YoutubeFullscreenActivity.class);
                        intent.putExtra("youtubeId", youtubeID);
                        startActivity(intent);
                    }
                });
            }




        }


        @Override
        public VideoAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.video_item, viewGroup, false);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                }
            });

            return new VideoAdapter.CardViewHolder(itemView);
        }


        public class CardViewHolder extends RecyclerView.ViewHolder {


            protected TextView lbl_desc_video;

            protected YouTubeThumbnailView youTubeView;

            public CardViewHolder(View v) {
                super(v);
                lbl_desc_video = (TextView) v.findViewById(R.id.lbl_desc_video);
                youTubeView = (YouTubeThumbnailView) v.findViewById(R.id.youtubeplayerview);
            }
        }


    }

}
