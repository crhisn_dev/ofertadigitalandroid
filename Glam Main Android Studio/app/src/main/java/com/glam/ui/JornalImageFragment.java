package com.glam.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.glam.CheckoutActivity;
import com.glam.JornalViewerActivity;
import com.glam.MainActivity;
import com.glam.R;
import com.glam.custom.CustomFragment;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.model.Data;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;

import java.util.ArrayList;
import java.util.List;

import br.com.innove.ofertadigital.persistence.JornalStoreInfo;
import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.StoreInfo;

public class JornalImageFragment extends CustomFragment {


    private RecyclerView recList;
    private View mainView;

    private OfertaDigitalDataSource dao;
    private ImageCache cache;

    private StoreInfo storeInfo;

    private LinearLayout linlaHeaderProgress;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();
        if (args != null && args.containsKey("storeinfo")) {
            this.storeInfo = (StoreInfo) args.getSerializable("storeinfo");
        }

        this.mainView = inflater.inflate(R.layout.pager_card_view, null);


        this.dao = new OfertaDigitalDataSource(getActivity());
        this.cache = new ImageCache(this.dao);

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).toolbar.setTitle("Jornal");
            ((MainActivity) getActivity()).toolbar.findViewById(
                    R.id.spinner_toolbar).setVisibility(View.GONE);
        } else {
            ((CheckoutActivity) getActivity()).getSupportActionBar().setTitle(
                    "Checkout");
        }

        this.linlaHeaderProgress = (LinearLayout) this.mainView.findViewById(R.id.linlaHeaderProgress);
		/*if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		}*/

        // setTouchNClick(v.findViewById(R.id.btnDone));
        setHasOptionsMenu(true);
        setupView(this.mainView);

        MyLogger.info("onCreateView: " + this);
        return this.mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLogger.info("onResume: " + this);
    }

    private void setupView(View v) {
        final Intent intent = this.getActivity().getIntent();

        this.recList = (RecyclerView) v.findViewById(R.id.cardList);
        this.recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        this.recList.setLayoutManager(llm);
        this.recList.setAdapter(new JornalAdapter(new ArrayList<JornalStoreInfo>()));


        final String store_id = this.storeInfo.getId();

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            public List<JornalStoreInfo> jornals;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (intent.hasExtra("jornal")) {
                        this.jornals = new ArrayList<>();
                        this.jornals.add((JornalStoreInfo) intent.getSerializableExtra("jornal"));
                    } else {
                        this.jornals = OfertaDigitalRestClient.getRef().findJornals(store_id);
                    }

                    if (this.jornals != null) {
                        List<MediaDesc> firstImage = new ArrayList<>();

                        for (JornalStoreInfo jornal : this.jornals) {
                            if (!jornal.getMediasList().isEmpty()) {
                                firstImage.add(jornal.getMediasList().get(0));
                            }
                        }

                        cache.requestImages(firstImage);
                    }
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (this.jornals != null) {
                    JornalImageFragment.this.recList.setAdapter(new JornalAdapter(this.jornals));
                }

                try {
                    Runtime.getRuntime().gc();
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                linlaHeaderProgress.setVisibility(View.GONE);
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    @Override
    public void onDestroyView() {
        MyLogger.info("onDestroyView: " + this);
        super.onDestroyView();

        try {
            this.unbindDrawables(this.mainView);
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            this.dao.close();
            this.dao = null;
            this.cache = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }


        this.recList = null;
        this.mainView = null;

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    private class JornalAdapter extends
            RecyclerView.Adapter<JornalAdapter.CardViewHolder> {

        private boolean isImageFitToScreen;

        private List<JornalStoreInfo> jornals;

        public JornalAdapter(List<JornalStoreInfo> jornals) {
            this.jornals = jornals;
        }

        @Override
        public int getItemCount() {
            return jornals.size();
        }


        @Override
        public void onBindViewHolder(final JornalAdapter.CardViewHolder vh, int i) {

            final JornalStoreInfo jornal = this.jornals.get(i);

            vh.lbl1.setText(jornal.getDescription());

            vh.img.requestLayout();

            {
                MediaDesc media = jornal.getMediasList().get(0);
                byte[] image = cache.getImage(media.getNativeimage_id());

                if (image != null) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.RGB_565;

                    Bitmap imagebitmap = BitmapFactory.decodeByteArray(image, 0, image.length, options);

                    if (imagebitmap.getHeight() > 250) {
                        double scale = 250d / imagebitmap.getHeight();
                        MyLogger.info("w: " + imagebitmap.getWidth() + " h: " + imagebitmap.getHeight() + " scale: " + scale);
                        Bitmap imagebitmapscaled = Bitmap.createScaledBitmap(imagebitmap, (int) (imagebitmap.getWidth() * scale), (int) (imagebitmap.getHeight() * scale), true);
                        vh.img.setImageBitmap(imagebitmapscaled);
                        imagebitmap.recycle();
                    }


                } else {
                    vh.img.setImageResource(R.drawable.imagem_nao_encontrada);
                }
            }

            WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            Point size = new Point();
            display.getSize(size);

            vh.img.getLayoutParams().height = size.y / 3;

            vh.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(JornalImageFragment.this.getActivity(), JornalViewerActivity.class);
                    intent.putExtra("jornal", jornal);

                    startActivity(intent);
                }
            });


        }


        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onCreateViewHolder(android.view.ViewGroup, int)
         */
        @Override
        public JornalAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.jornal_image_item, viewGroup, false);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                }
            });

            return new JornalAdapter.CardViewHolder(itemView);
        }

        /**
         * The Class CardViewHolder is the View Holder class for Adapter views.
         */
        public class CardViewHolder extends RecyclerView.ViewHolder {

            /**
             * The lbl3.
             */
            protected TextView lbl1;

            // protected TextView btnAdd;

            /**
             * The img.
             */
            protected ImageView img;

            /**
             * Instantiates a new card view holder.
             *
             * @param v the v
             */
            public CardViewHolder(View v) {
                super(v);
                lbl1 = (TextView) v.findViewById(R.id.lbl1);
                img = (ImageView) v.findViewById(R.id.img);

            }
        }
    }

    public void toggleHideyBar() {

        // BEGIN_INCLUDE (get_current_ui_flags)
        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        int uiOptions = getActivity().getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        // END_INCLUDE (get_current_ui_flags)
        // BEGIN_INCLUDE (toggle_ui_flags)
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);


        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        // Immersive mode: Backward compatible to KitKat.
        // Note that this flag doesn't do anything by itself, it only augments the behavior
        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
        // all three flags are being toggled together.
        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
        // Sticky immersive mode differs in that it makes the navigation and status bars
        // semi-transparent, and the UI flag does not get cleared when the user interacts with
        // the screen.
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        getActivity().getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
        //END_INCLUDE (set_ui_flags)
    }

}
