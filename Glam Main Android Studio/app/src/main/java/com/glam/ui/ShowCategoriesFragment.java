package com.glam.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.glam.MainActivity;
import com.glam.ProductDetail;
import com.glam.R;
import com.glam.custom.CustomFragment;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.model.Data;
import com.glam.utils.MyLogger;

import java.util.ArrayList;
import java.util.List;

import br.com.innove.ofertadigital.persistence.CategoryInfo;

/**
 * The Class OnSale is the fragment that shows the products in GridView.
 */
public class ShowCategoriesFragment extends CustomFragment {

    /**
     * The product list.
     */
    private ArrayList<Data> iList;
    private RecyclerView recList;

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @SuppressLint({"InflateParams", "InlinedApi"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.show_categories, null);

        ((MainActivity) getActivity()).toolbar.setTitle("Produtos");
        ((MainActivity) getActivity()).toolbar.findViewById(
                R.id.spinner_toolbar).setVisibility(View.GONE);

		/*if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		}*/

        Button btnFabCat = (Button) v.findViewById(R.id.fabCart);
        btnFabCat.setVisibility(View.GONE);

        setHasOptionsMenu(true);
        setupView(v);
        return v;
    }

    /* (non-Javadoc)
     * @see com.whatshere.custom.CustomFragment#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    /**
     * Setup the view components for this fragment. You write your code for
     * initializing the views, setting the adapters, touch and click listeners
     * etc.
     *
     * @param v the base view of fragment
     */
    private void setupView(View v) {
        this.iList = new ArrayList<>();

        this.recList = (RecyclerView) v.findViewById(R.id.cardList);
        this.recList.setHasFixedSize(true);
        StaggeredGridLayoutManager llm = new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL);
        this.recList.setLayoutManager(llm);
        final ShowCategoriesFragment.CardAdapter ca = new ShowCategoriesFragment.CardAdapter();
        this.recList.setAdapter(ca);


        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                loadDummyData();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                ca.notifyItemRangeChanged(0, iList.size());
            }
        };

        task.execute();
    }

    /**
     * The Class CardAdapterForCategory is the adapter for showing products in Card format
     * inside the RecyclerView. It shows dummy product image and dummy contents,
     * so you need to display actual contents as per your need.
     */
    private class CardAdapter extends
            RecyclerView.Adapter<ShowCategoriesFragment.CardAdapter.CardViewHolder> {


        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#getItemCount()
         */
        @Override
        public int getItemCount() {
            return iList.size();
        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder, int)
         */
        @Override
        public void onBindViewHolder(ShowCategoriesFragment.CardAdapter.CardViewHolder vh, int i) {

            Data d = iList.get(i);
            vh.lbl1.setText(d.getTexts()[0].toUpperCase());
            vh.lbl2.setText(d.getTexts()[1]);
            vh.lbl3.setText(d.getTexts()[2]);
            vh.img.setImageResource(R.drawable.logo_acougue);

        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onCreateViewHolder(android.view.ViewGroup, int)
         */
        @Override
        public ShowCategoriesFragment.CardAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int type_view) {

            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.grid_item, viewGroup, false);
            itemView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    //startActivity(new Intent(getActivity(), ProductDetail.class));
                    int position = recList.getChildPosition(v);

                    MyLogger.info("clicked category: " + iList.get(position).getTexts()[0]);

                    MainFragment fragment = new MainFragment();

                    Bundle args = new Bundle();
                    args.putCharSequence("category", iList.get(position).getTexts()[0]);
                    fragment.setArguments(args);

                    ShowCategoriesFragment.this.getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, fragment, "added_fragment").addToBackStack("something").commit();

                    android.support.v7.app.AppCompatActivity actionbar = (android.support.v7.app.AppCompatActivity) ShowCategoriesFragment.this.getActivity();
                    if (actionbar.getSupportActionBar() != null) {
                        actionbar.getSupportActionBar().setTitle("Produtos");
                    }
                }
            });


            return new ShowCategoriesFragment.CardAdapter.CardViewHolder(itemView);
        }

        /**
         * The Class CardViewHolder is the View Holder class for Adapter views.
         */
        public class CardViewHolder extends RecyclerView.ViewHolder {

            /**
             * The lbl3.
             */
            protected TextView lbl1, lbl2, lbl3;

            /**
             * The img.
             */
            protected ImageView img;

            /**
             * Instantiates a new card view holder.
             *
             * @param v the v
             */
            public CardViewHolder(View v) {
                super(v);
                lbl1 = (TextView) v.findViewById(R.id.lbl1);
                lbl2 = (TextView) v.findViewById(R.id.lbl2);
                lbl3 = (TextView) v.findViewById(R.id.lbl3);
                img = (ImageView) v.findViewById(R.id.img);
            }
        }
    }

    /**
     * Load dummy product data for displaying on the RecyclerView. You need to
     * write your own code for loading real products from Web-service or API and
     * displaying them on RecyclerView.
     */
    private void loadDummyData() {

        try {
            List<CategoryInfo> categories = OfertaDigitalRestClient.getRef().findCategories();

            for (CategoryInfo c : categories) {
                Data data = new Data(new String[]{c.getName(), "Alguma descrição",
                        ""}, new int[]{R.drawable.on_sale_item1});
                iList.add(data);
            }

			/*
            ArrayList<Data> al = new ArrayList<Data>();
			al.add(new Data(new String[]{"Raven's Wing Field Note", "$200-$400",
					"Gear Patrol"}, new int[]{R.drawable.on_sale_item1}));
			al.add(new Data(new String[]{"Y.M.C. Spray Tee (Grey)", "$67",
					"Oi Polloi"}, new int[]{R.drawable.on_sale_item2}));
			al.add(new Data(new String[]{"Ally Capellino", "$94",
					"Ally Capellino"}, new int[]{R.drawable.on_sale_item3}));
			al.add(new Data(new String[]{"Mid-Length Wool Cashmere Trench Coat",
					"$42", "Burberry US"}, new int[]{R.drawable.on_sale_item4}));

			iList = new ArrayList<Data>(al);
			iList.addAll(al);
			iList.addAll(al);
			iList.addAll(al);
			iList.addAll(al);
			iList.addAll(al);*/
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateOptionsMenu(android.view.Menu, android.view.MenuInflater)
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_exp, menu);
        menu.findItem(R.id.menu_grid).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);

        final MenuItem item = menu.findItem(R.id.menu_search);
        SearchView searchView = new SearchView(getActivity());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MyLogger.info("onQueryTextSubmit: " + query);

                MainFragment fragment = new MainFragment();

                Bundle args = new Bundle();
                //  args.putCharSequence("category", "Resultado");
                args.putCharSequence("query", query);
                fragment.setArguments(args);

                ShowCategoriesFragment.this.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment, "added_fragment").addToBackStack("something").commit();

                AppCompatActivity actionbar = (AppCompatActivity) ShowCategoriesFragment.this.getActivity();
                if (actionbar.getSupportActionBar() != null) {
                    actionbar.getSupportActionBar().setTitle("Produtos");
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }
}
