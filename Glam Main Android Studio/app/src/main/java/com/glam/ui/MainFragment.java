    package com.glam.ui;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.glam.MainActivity;
import com.glam.R;
import com.glam.buycart.BuyCartRemoteImpl;
import com.glam.custom.CustomFragment;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.transport.ImagesArrayResponseData;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;

import br.com.innove.ofertadigital.persistence.CategoryInfo;
import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.NativeImage;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.persistence.StoreInfo;
import br.com.innove.ofertadigital.rest.transport.ResultQueryData;

/**
 * The Class MainFragment is the base fragment that shows the list of various
 * products. You can add your code to do whatever you want related to products
 * for your app.
 */
public class MainFragment extends CustomFragment {
    // private RelativeLayout linlaHeaderProgress;

    public static enum CALLED_FROM {BUYCART, BUYLIST}

    private OfertaDigitalDataSource dao;
    private PageAdapter pagerAdapter;
    private ViewPager pager;
    private String searchQuery;
    private CALLED_FROM called;
    private View mainView;

    private ImageCache cache;
    private StoreInfo storeInfo;


    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle args = getArguments();
        if (args != null && args.containsKey("storeinfo")) {
            this.storeInfo = (StoreInfo) args.getSerializable("storeinfo");
        }

        this.mainView = inflater.inflate(R.layout.main_container, null);

        //this.linlaHeaderProgress = (RelativeLayout) this.mainView.findViewById(R.id.linlaHeaderProgress);

        this.dao = new OfertaDigitalDataSource(getActivity());
        this.cache = new ImageCache(this.dao);

        /*Spinner spinner = (Spinner) ((MainActivity) getActivity()).toolbar.findViewById(
                R.id.spinner_toolbar);

        // filtro por tipo
        spinner.setVisibility(View.GONE);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                MyLogger.info("selected: " + adapterView.getItemAtPosition(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        if (getArguments() != null) {
            called = (CALLED_FROM) getArguments().getSerializable("called");
        }


        setHasOptionsMenu(true);
        setupView(this.mainView);

        MyLogger.info("onCreateView: " + this);
        return this.mainView;
    }

    /* (non-Javadoc)
     * @see com.whatshere.custom.CustomFragment#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    /**
     * Setup the view components for this fragment. You write your code for
     * initializing the views, setting the adapters, touch and click listeners
     * etc.
     *
     * @param v the base view of fragment
     */
    private void setupView(View v) {
        initPager(v);
    }

    /**
     * Inits the pager view.
     *
     * @param v the root view
     */
    private void initPager(View v) {
        this.pager = (ViewPager) v.findViewById(R.id.pager);
        this.pager.setPageMargin(5);
        this.pager.setOffscreenPageLimit(1);
        this.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                MyLogger.info("position: " + position + " positionOffset: " + positionOffset + " positionOffsetPixels: " + positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        String query = getArguments() != null ?
                (getArguments().containsKey("query") ? getArguments().getString("query") : null) :
                null;
        List<CategoryInfo> categories = new ArrayList<>();

        if (query != null) {
            CategoryInfo cc = new CategoryInfo();
            cc.setName("Resultado");
            categories.add(cc);
        }

        this.pagerAdapter = new PageAdapter(query, categories, called);
        this.pager.setAdapter(this.pagerAdapter);

        if (query == null) {
            this.pager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    populateTabs();
                }
            }, 500);
        } else {
            this.searchQuery = query;
        }


    }

    @Override
    public void onDestroyView() {
        MyLogger.info("onDestroyView: " + this);
        super.onDestroyView();

        try {
            this.unbindDrawables(this.mainView);
        } catch (Exception e) {
            MyLogger.log(e);
        }

        this.pager = null;
        this.mainView = null;
        this.cache = null;

        try {
            this.dao.close();
            this.dao = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }


    }

    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    public void populateTabs() {
        // mycode
        {

            final String category = getArguments() != null ?
                    (getArguments().containsKey("category") ? getArguments().getString("category") : null) :
                    null;

            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                public List<CategoryInfo> categories;

                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        categories = OfertaDigitalRestClient.getRef().findCategories();
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    ((PageAdapter) pager.getAdapter()).setCategories(this.categories);
                    pager.getAdapter().notifyDataSetChanged();

                    int counter = 0;

                    if (category != null) {

                        for (CategoryInfo categoryInfo : this.categories) {
                            if (categoryInfo.getName().equals(category)) {
                                break;
                            }
                            counter++;
                        }

                        pager.setCurrentItem(counter);
                    }

                    // linlaHeaderProgress.setVisibility(View.GONE);
                }

                @Override
                protected void onPreExecute() {
                    //  linlaHeaderProgress.setVisibility(View.VISIBLE);
                }
            };
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    /**
     * Load dummy product data for displaying on the RecyclerView. You need to
     * write your own code for loading real products from Web-service or API and
     * displaying them on RecyclerView.
     */
    /*private void loadDummyData222() {
        ArrayList<Data> al = new ArrayList<Data>();
        al.add(new Data(new String[]{null,
                "Ally Capellino Frank Brown - Fott Shop 2014", "$200-$400",
                "Shop.fott.com"}, new int[]{R.drawable.popularity_img1}));
        al.add(new Data(new String[]{"50%\nOFF", "Tap & DYE Legacy", "$67",
                "Tapanddye"}, new int[]{R.drawable.popularity_img2}));
        al.add(new Data(new String[]{null, "Piper Felt Hat by Brixton",
                "$94", "Tapanddye"}, new int[]{R.drawable.popularity_img3}));
        al.add(new Data(new String[]{null, "HIKE Abysss Stone", "$42",
                "Handwers"}, new int[]{R.drawable.popularity_img4}));
        al.add(new Data(new String[]{"20%\nOFF", "Lenovo Leather Belt",
                "$12", "Lenovo"}, new int[]{R.drawable.imagem_nao_encontrada}));

        // iList = new ArrayList<Data>(al);
        //iList.addAll(al);
        //iList.addAll(al);
        //iList.addAll(al);
        //iList.addAll(al);
        //iList.addAll(al);
    }*/

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateOptionsMenu(android.view.Menu, android.view.MenuInflater)
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_exp, menu);
        menu.findItem(R.id.menu_search).setVisible(true);
        menu.findItem(R.id.menu_buy).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);

        final MenuItem menuItemSearch = menu.findItem(R.id.menu_search);
        SearchView searchView = new SearchView(getActivity());

        MenuItemCompat.setShowAsAction(menuItemSearch, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(menuItemSearch, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MyLogger.info("MainFragment onQueryTextSubmit: " + query + " store: " + storeInfo);


                List<CategoryInfo> categories = new ArrayList<CategoryInfo>();
                CategoryInfo cc = new CategoryInfo();
                cc.setName("Resultado");
                categories.add(cc);

                MainFragment.this.pagerAdapter = new PageAdapter(query, categories, called);
                MainFragment.this.pager.setAdapter(MainFragment.this.pagerAdapter);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                        toggleSoftInput(InputMethodManager.SHOW_FORCED,
                                InputMethodManager.HIDE_IMPLICIT_ONLY);

            }
        });

        MenuItemCompat.setOnActionExpandListener(menuItemSearch,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem menuItem) {
                        MyLogger.info("onMenuItemActionExpand");
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                        MyLogger.info("onMenuItemActionCollapse");

                        MainFragment.this.pagerAdapter = new PageAdapter(null, new ArrayList<CategoryInfo>(), called);
                        MainFragment.this.pager.setAdapter(MainFragment.this.pagerAdapter);
                        MainFragment.this.populateTabs();


                        return true;
                    }
                });

        if (this.searchQuery != null) {
            menuItemSearch.expandActionView();
            searchView.setQuery(this.searchQuery, false);
        }

        MenuItem menu_buy = menu.findItem(R.id.menu_buy);
        menu_buy.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setupContainer(1);

                return true;
            }
        });

        menu_buy.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setupContainer(1);

            }
        });
    }

    /**
     * The Class PageAdapter is adapter class for ViewPager and it simply holds
     * a RecyclerView with dummy images. You need to write logic for loading
     * actual images.
     */
    private class PageAdapter extends PagerAdapter {

        private List<CategoryInfo> categories;
        private String productName;
        private CALLED_FROM called;

        public PageAdapter(String productName, List<CategoryInfo> categories, CALLED_FROM called) {
            this.productName = productName;
            this.categories = categories;
            this.called = called;
        }


        public void setCategories(List<CategoryInfo> categories) {
            this.categories = categories;
        }

        /* (non-Javadoc)
                         * @see android.support.v4.view.PagerAdapter#getCount()
                         */
        @Override
        public int getCount() {
            return this.categories.size();
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#instantiateItem(android.view.ViewGroup, int)
         */
        @Override
        public Object instantiateItem(ViewGroup container, final int pos) {

            final View v = getLayoutInflater(null).inflate(
                    R.layout.pager_card_view, null);

            final RecyclerView recList = (RecyclerView) v.findViewById(R.id.cardList);
            recList.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);

            recList.setLayoutManager(llm);

            final CardAdapterForCategory adapter = new CardAdapterForCategory(storeInfo, cache, MainFragment.this, this.productName, this.categories.get(pos).getName(), new ArrayList<ProductInfo>(), this.called);
            adapter.addCallback(new CardAdapterForCategory.CardCallback() {
                @Override
                public void process(ProductInfo product, QuantityOrder qtde) throws Exception {
                    // aqui
                    if (called.equals(CALLED_FROM.BUYLIST)) {
                        MyLogger.info("calling popback stack");
                        Intent intent = new Intent();
                        intent.putExtra("prod", product);
                        intent.putExtra("qtde", qtde);
                        MainFragment.this.getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                        MainFragment.this.getActivity().getSupportFragmentManager().popBackStack();

                        try {
                            Runtime.getRuntime().gc();
                        } catch (Exception e) {
                            MyLogger.log(e);
                        }
                    }

                    if (called.equals(CALLED_FROM.BUYCART)) {
                        BuyCartRemoteImpl.getRef().addProduct(product, qtde);
                    }
                }
            });
            recList.setAdapter(adapter);

            // recList.removeOnScrollListener();
            // my code
            {
                recList.setOnScrollListener(new EndlessRecyclerViewScrollListener(llm) {

                    private ProgressDialog pdialog = new ProgressDialog(MainFragment.this.getActivity());

                    private boolean end = false;

                    @Override
                    synchronized public void onLoadMore(int page, int totalItemsCount, final RecyclerView view) {
                        MyLogger.info("@@@@@ page: " + page + " totalItemsCount: " + totalItemsCount + " view: " + view);
                        end = false;
                        RecyclerView.Adapter adp = view.getAdapter();

                        final CardAdapterForCategory cadp = (CardAdapterForCategory) adp;
                        final List<ProductInfo> currentProducts = cadp.getCurrentProducts();
                        final int currentSize = currentProducts.size();

                        List<String> categories_tmp = new ArrayList<>();
                        categories_tmp.add(categories.get(pos).getName());

                        if (productName != null) {
                            categories_tmp = null;
                        }

                        final List<String> categories_final = categories_tmp;

                        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                            public ResultQueryData newProducts;

                            @Override
                            protected Void doInBackground(Void... params) {
                                try {
                                    WeakReference<ResultQueryData> response = OfertaDigitalRestClient.getRef().findProducts(storeInfo.getId(), productName, categories_final, null, null, currentSize, 20, false);
                                    this.newProducts = response.get();
                                } catch (Exception e) {
                                    MyLogger.log(e);
                                }

                                try {
                                    this.requestImages(this.newProducts.getProducts());
                                } catch (Exception e) {
                                    MyLogger.log(e);
                                }

                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                try {
                                    final int curSize = adapter.getItemCount();
                                    currentProducts.addAll(newProducts.getProducts());
                                    view.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            adapter.notifyItemRangeInserted(curSize, currentProducts.size() - 1);
                                        }
                                    });
                                } catch (Exception e) {
                                    MyLogger.log(e);
                                    view.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(MainFragment.this.getActivity(), "Erro carregando produtos.", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }

                              /*  view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        linlaHeaderProgress.setVisibility(View.GONE);
                                    }
                                });*/

                                /*view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //    Toast.makeText(MainFragment.this.getActivity(), "Carregando...", Toast.LENGTH_SHORT).show();
                                        pdialog.dismiss();
                                    }
                                });*/
                                end = true;
                            }

                            @Override
                            protected void onPreExecute() {


                            }

                            private boolean mustBeDownload(MediaDesc media) {
                                if (MediaDesc.MEDIA_TYPE.IMAGE.equals(media.getType()) &&
                                        media.getNativeimage_id() != null &&
                                        !cache.isCached(media.getNativeimage_id())) {
                                    return true;
                                }

                                return false;
                            }

                            public void requestImages(List<ProductInfo> products) throws Exception {
                                MyLogger.info("start requestImages");
                                List<String> nativeImagesIds = new ArrayList<>();
                                for (ProductInfo prod : products) {
                                    MyLogger.info("trying cache product: " + prod.getName());
                                    List<MediaDesc> medias = (prod.getMediasList() == null ? new ArrayList<MediaDesc>() : prod.getMediasList());
                                    for (MediaDesc media : medias) {
                                        if (this.mustBeDownload(media) && !nativeImagesIds.contains(media.getNativeimage_id())) {
                                            nativeImagesIds.add(media.getNativeimage_id());
                                        }
                                    }
                                }

                                ImagesArrayResponseData images = OfertaDigitalRestClient.getRef().requestImages(nativeImagesIds);
                                Collection<NativeImage> nativeImages = images.getImages().values();
                                for (NativeImage nativeImage : nativeImages) {
                                    MyLogger.info("caching: " + nativeImage.getId());
                                    cache.cacheImage(nativeImage);
                                    nativeImage.setSourcestream(null);
                                }

                                try {
                                    Runtime.getRuntime().gc();
                                } catch (Exception e) {
                                    MyLogger.log(e);
                                }

                                MyLogger.info("end called requestImages");
                            }
                        };

                        pdialog.setIndeterminate(true);
                        pdialog.setCancelable(false);
                        pdialog.setMessage("Carregando produtos...");
                        //  pdialog.getWindow().setGravity(Gravity.BOTTOM);
                        pdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                        final Toast toast = Toast.makeText(MainFragment.this.getActivity(), "Carregando produtos...", Toast.LENGTH_SHORT);


                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                while (!end) {


                                    view.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            toast.show();
                                        }
                                    });

                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }

                                toast.cancel();


                            }
                        });
                        t.start();

                        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    }
                });
            }

            // products
            {

                ProgressDialog pdialog = new ProgressDialog(MainFragment.this.getActivity());

                AsyncTask<Void, Void, Void> queryTask = new AsyncTask<Void, Void, Void>() {


                    @Override
                    protected Void doInBackground(Void... voids) {
                        endLoadList = false;
                        MyLogger.info("### start called firstPopulateFromRest");
                        try {
                            adapter.firstPopulateFromRest();
                        } catch (Exception e) {
                            MyLogger.log(e);
                        }
                        MyLogger.info("### end called firstPopulateFromRest");

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        recList.post(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });

                        /*try {
                            MyLogger.info("### this.result: " + this.result + " ###");
                            ResultQueryData productss = this.result.get();
                            MyLogger.info("### this.result.get(): " + productss + "###");

                            if (productss != null) {

                                MyLogger.info("### this.result.getProducts().size(): " + productss.getProducts().size());
                                adapter.notifyItemRangeChanged(adapter.getItemCount(), productss.getProducts().size() - 1);
                                adapter.notifyDataSetChanged();

                                try {
                                    Thread.sleep(500);
                                } catch (Exception e) {

                                }

                                adapter.notifyItemRangeChanged(adapter.getItemCount(), productss.getProducts().size() - 1);
                                adapter.notifyDataSetChanged();

                                pagerAdapter.notifyDataSetChanged();

                                MyLogger.info("### called notifyItemRangeChanged");
                            }
                        } catch (Exception e) {
                            MyLogger.log(e);
                            MyLogger.info("### exception called");
                        }*/

                       /* recList.post(new Runnable() {
                            @Override
                            public void run() {
                                pdialog.dismiss();
                            }
                        });*/
                        endLoadList = true;

                    }

                    @Override
                    protected void onPreExecute() {
                        /*recList.post(new Runnable() {
                            @Override
                            public void run() {
                                linlaHeaderProgress.setVisibility(View.VISIBLE);
                            }
                        });*/

                    }
                };

                pdialog.setIndeterminate(true);
                pdialog.setCancelable(false);
                // pdialog.getWindow().setGravity(Gravity.BOTTOM);
                pdialog.setMessage("Carregando produtos...");
                pdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

               /* recList.post(new Runnable() {
                    @Override
                    public void run() {
                        // Toast.makeText(MainFragment.this.getActivity(), "Carregando...", Toast.LENGTH_SHORT).show();
                        pdialog.show();
                    }
                });*/

                final Toast toast = Toast.makeText(MainFragment.this.getActivity(), "Carregando produtos...", Toast.LENGTH_SHORT);


                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!endLoadList) {


                            recList.post(new Runnable() {
                                @Override
                                public void run() {
                                    toast.show();
                                }
                            });

                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        toast.cancel();


                    }
                });
                t.start();
                queryTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                //queryTask.execute();
            }


            //((MainActivity) getActivity()).enableActionBarAutoHide(recList);
            container.addView(v,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                    android.view.ViewGroup.LayoutParams.MATCH_PARENT);


            return v;
        }

        private boolean endLoadList = false;

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#destroyItem(android.view.ViewGroup, int, java.lang.Object)
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            MyLogger.info("destroyItem: " + object);
            ((ViewPager) container).removeView((View) object);

            if (object instanceof android.widget.RelativeLayout) {
                android.widget.RelativeLayout rr = (android.widget.RelativeLayout) object;
                for (int i = 0; i < rr.getChildCount(); i++) {
                    Object vv = rr.getChildAt(i);
                    if (vv instanceof android.support.v7.widget.RecyclerView) {
                        MyLogger.info("release RecyclerView: " + vv);
                        android.support.v7.widget.RecyclerView rview = (android.support.v7.widget.RecyclerView) vv;

                        RecyclerView.Adapter adapter = rview.getAdapter();
                        if (adapter instanceof CardAdapterForCategory) {
                            MyLogger.info("clear adapter: " + adapter);
                            CardAdapterForCategory cardAdapter = (CardAdapterForCategory) adapter;
                            cardAdapter.clearProducts();
                        }

                        rview.setAdapter(null);
                        rview.invalidate();
                    }

                }

            }

            try {
                Runtime.getRuntime().gc();
            } catch (Exception e) {
                MyLogger.log(e);
            }

        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#isViewFromObject(android.view.View, java.lang.Object)
         */
        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        /* (non-Javadoc)
         * @see android.support.v4.view.PagerAdapter#getPageTitle(int)
         */
        @Override
        public CharSequence getPageTitle(int position) {
            String label = this.categories.get(position).getName().toUpperCase();
            ;

            if ("PERECIVEIS".equals(label)) {
                label = "PERECÍVEIS";
            }

            if ("ACOUGUE".equals(label)) {
                label = "AÇOUGUE";
            }


            if ("PES_MAOS".equals(label)) {
                label = "PÉS E MÃOS";
            }

            if ("INFECCOES".equals(label)) {
                label = "INFLAMAÇÃO";
            }


            return label;
        }

    }

}
