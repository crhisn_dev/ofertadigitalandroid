package com.glam.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.glam.ProductDetail;
import com.glam.R;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.model.Data;
import com.glam.transport.ImagesArrayResponseData;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.NativeImage;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.rest.transport.ResultQueryData;

/**
 * The Class CardAdapterForCategory is the adapter for showing products in Card format
 * inside the RecyclerView. It shows dummy product image and dummy contents,
 * so you need to display actual contents as per your need.
 */
public class Copy_CardAdapterForCategory extends
        RecyclerView.Adapter<Copy_CardAdapterForCategory.CardViewHolder> {


    private MainFragment mainFragment;

    private String productName;
    private String category;

    private List<ProductInfo> currentProducts;
    private WeakReference<ResultQueryData> currentResults;
    // private int productsSize;

    private int total;
    private int startWith;
    private int pageSize;

    private ImageCache cache;
    private DecimalFormat n_br;

    private CardCallback callback;

    private MainFragment.CALLED_FROM called;

    public Copy_CardAdapterForCategory(MainFragment mainFragment, String productName, String category, List<ProductInfo> products, MainFragment.CALLED_FROM called) {
        this.mainFragment = mainFragment;

        this.called = called;
        this.productName = productName;
        this.category = category;
        this.currentProducts = products;

        this.startWith = 0;
        this.total = 0;
        this.pageSize = 5;

        this.cache = ImageCache.getInstance(mainFragment.getActivity());
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");
    }

    /* (non-Javadoc)
             * @see android.support.v7.widget.RecyclerView.Adapter#getItemCount()
             */
    @Override
    public int getItemCount() {
        return this.currentProducts.size();
    }


    public int sizeProducts() {
        return this.currentProducts.size();
    }


    private byte[] getImage(ProductInfo prod) {
        MyLogger.info("prod: " + prod.getName());
        List<MediaDesc> medias = (prod.getMediasList() == null ? new ArrayList<MediaDesc>() : prod.getMediasList());
        for (MediaDesc media : medias) {
            MyLogger.info("media: " + media.getNativeimage_id());
            if (MediaDesc.MEDIA_TYPE.IMAGE.equals(media.getType()) && media.getNativeimage_id() != null) {
                return cache.getImage(media.getNativeimage_id());
            }
        }

        return null;
    }

    /* (non-Javadoc)
     * @see android.support.v7.widget.RecyclerView.Adapter#onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder, int)
     */
    @Override
    public void onBindViewHolder(CardViewHolder vh, int i) {
        MyLogger.info("onBindViewHolder: " + i);

        try {
            final ProductInfo product = this.currentProducts.get(i);

            Data d = new Data(new String[]{"20%\nOFF", "Lenovo Leather Belt",
                    "$12", "Lenovo"}, new int[]{R.drawable.imagem_nao_encontrada});
            vh.lbl1.setText("");

            vh.lbl2.setText(product.getName());

            try {
                vh.lbl3.setText("R$ " + n_br.format(product.getCurrentPrice().getValue1()));
            } catch (Exception e) {
                MyLogger.info("erro: " + e.getLocalizedMessage());
                vh.lbl3.setText("R$ 0.00");
            }

            vh.lbl4.setText("");

            vh.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent startIntent = new Intent(mainFragment.getActivity(), ProductDetail.class);
                    startIntent.putExtra("product", product);
                    mainFragment.startActivity(startIntent);
                }
            });

            byte[] image = this.getImage(product);

            if (image != null) {
                Bitmap imagebitmap = BitmapFactory.decodeByteArray(image, 0, image.length);

                if (imagebitmap.getHeight() > 250) {
                    double scale = Double.valueOf("250") / imagebitmap.getHeight();
                    MyLogger.info("w: " + imagebitmap.getWidth() + " h: " + imagebitmap.getHeight() + " scale: " + scale);
                    imagebitmap = Bitmap.createScaledBitmap(imagebitmap, (int) (imagebitmap.getWidth() * scale), (int) (imagebitmap.getHeight() * scale), true);
                }

                vh.img.setImageBitmap(imagebitmap);
            } /*else {
                vh.img.setImageResource(R.drawable.imagem_nao_encontrada);
            }*/


            vh.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final QuantityOrder qtde = new QuantityOrder();

                    qtde.setBuyPrice(product.getCurrentPrice());
                    qtde.setBuyQuantity(Double.parseDouble("1"));

                    final Dialog dialog = new Dialog(mainFragment.getActivity());
                    //dialog.setTitle("Quantidade do Produto");
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                    dialog.setContentView(R.layout.picker_layout);
                    dialog.setCancelable(false);
                    Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
                    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                    final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.numberPicker1);

                    TextView productName = (TextView) dialog.findViewById(R.id.textNameProduct);
                    productName.setText(product.getName());

                    TextView price = (TextView) dialog.findViewById(R.id.textPrice);
                    if (product.getCurrentPrice() != null) {
                        price.setText("R$ " + n_br.format(product.getCurrentPrice().getValue1()));
                    }

                    final TextView subTotal = (TextView) dialog.findViewById(R.id.subtotal);
                    if (product.getCurrentPrice() != null) {
                        subTotal.setText("   Subtotal: R$ " + n_br.format(product.getCurrentPrice().getValue1()));
                    }

                    setNumberPickerTextColor(np, Color.BLACK);
                    np.setMaxValue(100);
                    np.setMinValue(1);

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    btnConfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(Copy_CardAdapterForCategory.this.mainFragment.getActivity(), "Producto:" + product.getName() + " Called: " + called, Toast.LENGTH_SHORT).show();
                            try {

                                if (callback != null) {
                                    callback.process(product, qtde);
                                }

                                dialog.dismiss();

                            } catch (Exception e) {
                                MyLogger.log(e);
                            }
                        }
                    });

                    np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {


                        @Override
                        public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                            try {
                                BigDecimal priceValue = product.getCurrentPrice().getValue1();
                                priceValue = priceValue.multiply(new BigDecimal(i1));

                                subTotal.setText("Subtotal:   R$ " + n_br.format(priceValue));

                                qtde.setBuyPrice(product.getCurrentPrice());
                                qtde.setBuyQuantity(Double.parseDouble(i1 + ""));


                                // subTotal.setText("i: " + i + " i1: " + i1);
                            } catch (Exception e) {
                                MyLogger.log(e);
                            }

                        }
                    });

                    dialog.show();
                }
            });
        } catch (Throwable t) {
            MyLogger.log(t);
        }
    }

    /* (non-Javadoc)
     * @see android.support.v7.widget.RecyclerView.Adapter#onCreateViewHolder(android.view.ViewGroup, int)
     */
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int iii) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cart_product_item, viewGroup, false);
        itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }

        });
        return new CardViewHolder(itemView);
    }

    private boolean mustBeDownload(MediaDesc media) {
        if (MediaDesc.MEDIA_TYPE.IMAGE.equals(media.getType()) &&
                media.getNativeimage_id() != null &&
                !this.cache.isCached(media.getNativeimage_id())) {
            return true;
        }

        return false;
    }

    public void requestImages(List<ProductInfo> products) throws Exception {
        MyLogger.info("start requestImages");
        List<String> nativeImagesIds = new ArrayList<>();
        for (ProductInfo prod : products) {
            MyLogger.info("trying cache product: " + prod.getName());
            List<MediaDesc> medias = (prod.getMediasList() == null ? new ArrayList<MediaDesc>() : prod.getMediasList());
            for (MediaDesc media : medias) {
                if (this.mustBeDownload(media) && !nativeImagesIds.contains(media.getNativeimage_id())) {
                    nativeImagesIds.add(media.getNativeimage_id());
                }
            }
        }

        ImagesArrayResponseData images = OfertaDigitalRestClient.getRef().requestImages(nativeImagesIds);
        Collection<NativeImage> nativeImages = images.getImages().values();
        for (NativeImage nativeImage : nativeImages) {
            MyLogger.info("caching: " + nativeImage.getId());
            this.cache.cacheImage(nativeImage);
        }

        MyLogger.info("end called requestImages");

    }

    private OfertaDigitalRestClient client = OfertaDigitalRestClient.getRef();

    public WeakReference<ResultQueryData> firstPopulateFromRest() throws Exception {
        List<String> categories = new ArrayList<>();

        if (this.category != null) {
            categories.add(this.category);
        }

        if (this.productName != null) {
            categories = null;
        }

        this.currentResults = this.client.findProducts("serradalto", productName, categories, null, null, this.startWith, this.pageSize, true);


        final ResultQueryData resultsRef = this.currentResults.get();

        if (resultsRef != null) {

            this.requestImages(resultsRef.getProducts());

            this.currentProducts = resultsRef.getProducts();

            this.total = resultsRef.getPagination().getTotalResult();

            MyLogger.info("setting total: " + this.total);
            MyLogger.info("firstPopulateFromRest: " + resultsRef.getProducts().size());
        } else {
            MyLogger.info("###################### resultRef null ##################");
        }

        return this.currentResults;
    }

    public WeakReference<ResultQueryData> nextPopulateFromRest() throws Exception {
        List<String> categories = new ArrayList<>();

        if (this.category != null) {
            categories.add(this.category);
        }

        if (this.productName != null) {
            categories = null;
        }

        startWith += pageSize;

        this.currentResults = OfertaDigitalRestClient.getRef().findProducts("serradalto", productName, categories, null, null, this.startWith, this.pageSize, false);

        if (this.currentResults.get() != null) {
            ResultQueryData resultsRef = this.currentResults.get();

            this.requestImages(resultsRef.getProducts());
            this.currentProducts.addAll(resultsRef.getProducts());

            MyLogger.info("nextPopulateFromRest: " + resultsRef.getProducts().size());
        }


        return this.currentResults;
    }


    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {

                } catch (IllegalAccessException e) {

                } catch (IllegalArgumentException e) {

                }
            }
        }
        return false;
    }

    /*public void setResult(ResultQueryData result) {
        this.result = result;
        this.products = this.result.getProducts();
        this.maxSize = this.result.getPagination().getMaxSize();
    }*/

    /**
     * The Class CardViewHolder is the View Holder class for Adapter views.
     */
    public class CardViewHolder extends RecyclerView.ViewHolder {

        /**
         * The textviews.
         */
        protected TextView lbl1, lbl2, lbl3, lbl4, btnAdd;

        /**
         * The img.
         */
        protected ImageView img;

        /**
         * Instantiates a new card view holder.
         *
         * @param v the v
         */
        public CardViewHolder(View v) {
            super(v);
            lbl1 = (TextView) v.findViewById(R.id.lbl1);
            lbl2 = (TextView) v.findViewById(R.id.lbl2);
            lbl3 = (TextView) v.findViewById(R.id.lbl3);
            lbl4 = (TextView) v.findViewById(R.id.lbl4);
            img = (ImageView) v.findViewById(R.id.img);
            btnAdd = (TextView) v.findViewById(R.id.btnAdd);

        }

        public View getItemView() {
            return itemView;
        }


    }

    public void clearProducts() {
        this.currentProducts = null;
    }

    public void addCallback(CardCallback callback) {
        this.callback = callback;
    }

    public static interface CardCallback {
        public void process(ProductInfo prod, QuantityOrder qtde) throws Exception;
    }


}
