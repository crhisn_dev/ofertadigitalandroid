package com.glam.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.glam.CheckoutActivity;
import com.glam.MainActivity;
import com.glam.R;
import com.glam.buycart.BuyCartRemoteImpl;
import com.glam.buycart.ItemProductIdQtde;
import com.glam.custom.CustomFragment;
import com.glam.dialog.BuyQtdeDialog;
import com.glam.dialog.MyDialogs;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.BuyListId;
import com.glam.persistence.BuyListInfo;
import com.glam.transport.BuyListOpRequestData;
import com.glam.transport.ProductListRequest;
import com.glam.utils.MyLogger;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import br.com.innove.ofertadigital.persistence.Address;
import br.com.innove.ofertadigital.persistence.AddressInfo;
import br.com.innove.ofertadigital.persistence.ItemId;
import br.com.innove.ofertadigital.persistence.ItemProductQuantityInfo;
import br.com.innove.ofertadigital.persistence.PaymentInfo;
import br.com.innove.ofertadigital.persistence.PriceLabelInfo;
import br.com.innove.ofertadigital.persistence.ProductId;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.ProductInfoNoEntity;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.persistence.StoreInfo;


public class BuyListFragment extends CustomFragment {

    private RecyclerView productsListView;
    private RecyclerView buyListView;
    private Button btnAddProdBuy;
    private BuyListInfo currentItem;
    private Button btnAddBuyList;
    private Button btnSaveList;

    private List<BuyListInfo> buyLists;
    private TextView textViewTotalRS;
    private TextView textViewQtde;

    private DecimalFormat n_br;

    private View mainView;
    private StoreInfo storeInfo;
    private LinearLayout linlaHeaderProgress;

    public BuyListFragment() {
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null && args.containsKey("storeinfo")) {
            this.storeInfo = (StoreInfo) args.getSerializable("storeinfo");
        }

        this.mainView = inflater.inflate(R.layout.buy_list_pager_view, null);

        try {
            if (getActivity() instanceof MainActivity) {
                ((MainActivity) getActivity()).toolbar.setTitle("Lista de Compras");
                ((MainActivity) getActivity()).toolbar.findViewById(
                        R.id.spinner_toolbar).setVisibility(View.GONE);
            } else {
                ((CheckoutActivity) getActivity()).getSupportActionBar().setTitle(
                        "Lista de Compras");
            }
        } catch (Throwable e) {
            MyLogger.info("caculate error: " + e.getLocalizedMessage());
        }

        this.linlaHeaderProgress = (LinearLayout) this.mainView.findViewById(R.id.linlaHeaderProgress);


        this.textViewTotalRS = (TextView) this.mainView.findViewById(R.id.textViewTotalRS);
        this.textViewQtde = (TextView) this.mainView.findViewById(R.id.textViewQtde);

        this.btnAddProdBuy = (Button) this.mainView.findViewById(R.id.btnAddProdBuyList);
        this.btnAddProdBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainFragment fragment = new MainFragment();

                fragment.setTargetFragment(BuyListFragment.this, 1001);

                Bundle args = new Bundle();
                //  args.putCharSequence("category", "Resultado");
                args.putSerializable("called", MainFragment.CALLED_FROM.BUYLIST);
                args.putSerializable("storeinfo", storeInfo);
                fragment.setArguments(args);


                BuyListFragment.this.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment, "added_fragment").addToBackStack("something").commit();

                AppCompatActivity actionbar = (AppCompatActivity) BuyListFragment.this.getActivity();
                if (actionbar.getSupportActionBar() != null) {
                    actionbar.getSupportActionBar().setTitle("Produtos");
                    // actionbar.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    // actionbar.getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_tab_indicator_material);
                }
            }
        });

        this.btnAddBuyList = (Button) this.mainView.findViewById(R.id.btnAddbuyList);
        this.btnAddBuyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(BuyListFragment.this.getActivity());

                //dialog.setTitle("Quantidade do Produto");
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                dialog.setContentView(R.layout.buy_list_name_layout);
                dialog.setCancelable(false);

                Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
                Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                final EditText textName = (EditText) dialog.findViewById(R.id.textListName);

                btnConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MyLogger.info("call create buy list");
                        final String buyListName = textName.getText().toString();

                        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                MyLogger.info("calling rest create buy list");
                                try {
                                    ProductListRequest prodList = new ProductListRequest();
                                    prodList.setSyncItens(new ArrayList<ProductListRequest.SyncItem>());
                                    OfertaDigitalRestClient.getRef().processBuyListOp(null, buyListName, "", prodList, BuyListOpRequestData.BUYLIST_OP.CREATE);
                                } catch (Exception e) {
                                    MyLogger.log(e);
                                }

                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                dialog.dismiss();
                                try {
                                    updateBuyList(buyListName);
                                } catch (Exception e) {
                                    MyLogger.log(e);
                                }
                            }
                        };
                        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });


                dialog.show();
            }
        });

        this.btnSaveList = (Button) this.mainView.findViewById(R.id.btnSaveBuyList);
        this.btnSaveList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            ProductListRequest listReq = new ProductListRequest();
                            List<ProductListRequest.SyncItem> syncItens = new ArrayList<ProductListRequest.SyncItem>();
                            for (ItemProductQuantityInfo item : currentItem.getItemsProduct()) {
                                ProductListRequest.SyncItem syncItem = new ProductListRequest.SyncItem();
                                syncItem.setProductId(item.getBuyProduct().getProdId());
                                syncItem.setQuantity(item.getBuyQuantity());
                                syncItem.setItemId(item.getItemId());

                                syncItens.add(syncItem);
                            }

                            listReq.setSyncItens(syncItens);
                            OfertaDigitalRestClient.getRef().processBuyListOp(currentItem.getBuyListId(), currentItem.getName(), currentItem.getDescription(), listReq, BuyListOpRequestData.BUYLIST_OP.SAVE);

                            MyLogger.info("call save op");
                        } catch (Exception e) {
                            MyLogger.log(e);
                        }
                        return null;

                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        btnSaveList.setVisibility(View.GONE);
                        btnAddProdBuy.setVisibility(View.GONE);
                    }
                };
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        // setTouchNClick(v.findViewById(R.id.btnDone));
        setHasOptionsMenu(true);

        initBuyList(this.mainView);
        initBuyListProducts(this.mainView);


        MyLogger.info("onCreateView: " + this);
        return this.mainView;
    }


    @Override
    public void onDestroyView() {
        MyLogger.info("onDestroyView: " + this);
        super.onDestroyView();

        try {
            this.unbindDrawables(this.mainView);
        } catch (Exception e) {
            MyLogger.log(e);
        }

        this.productsListView = null;
        this.buyListView = null;
        this.mainView = null;


        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    private void initBuyListProducts(View v) {
        //  this.iList = new ArrayList<>();

        MyLogger.info("initBuyListProducts: " + this.currentItem);

        this.productsListView = (RecyclerView) v.findViewById(R.id.buyListProducts);
        this.productsListView.setHasFixedSize(true);
        this.productsListView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.productsListView.setLayoutManager(llm);


        List<ItemProductQuantityInfo> items = new ArrayList<>();

        if (this.currentItem != null) {
            items = this.currentItem.getItemsProduct();
            btnSaveList.setVisibility(View.VISIBLE);
            btnAddProdBuy.setVisibility(View.VISIBLE);
        }

        BuyListFragment.BuyListAdapter ca = new BuyListFragment.BuyListAdapter(items);
        this.productsListView.setAdapter(ca);
    }


    private class BuyListAdapter extends
            RecyclerView.Adapter<BuyListFragment.BuyListAdapter.CardViewHolder> {

        private final DecimalFormat n_br;
        private List<ItemProductQuantityInfo> itemsProduct;

        public BuyListAdapter(List<ItemProductQuantityInfo> itemsProduct) {
            this.itemsProduct = itemsProduct;
            this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
            this.n_br.applyPattern("###,###,##0.00");

        }

        @Override
        public int getItemCount() {
            return itemsProduct.size();
        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder, int)
         */
        @Override
        public void onBindViewHolder(BuyListFragment.BuyListAdapter.CardViewHolder vh, final int i) {
            final ItemProductQuantityInfo item = itemsProduct.get(i);
            vh.lbl1.setText(item.getBuyProduct().getName());

            try {
                QuantityOrder qtde = item.getBuyQuantity();

                BigDecimal priceItem = item.getBuyProduct().getCurrentPrice().getValue1();
                priceItem = priceItem.multiply(new BigDecimal(qtde.getBuyQuantity()));

                vh.lbl3.setText("R$ " + n_br.format(priceItem));
            } catch (Exception e) {
                MyLogger.info("erro: " + e.getLocalizedMessage());
                vh.lbl3.setText("R$ 0.00");
            }

            vh.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (currentItem != null) {
                        currentItem.getItemsProduct().remove(item);

                        BuyListFragment.BuyListAdapter ca = new BuyListFragment.BuyListAdapter(currentItem.getItemsProduct());
                        productsListView.setAdapter(ca);
                    }
                }
            });

            vh.checkSel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CheckBox) v).isChecked()) {
                        currentItem.getItemsProduct().get(i).setItemStatus(ItemProductQuantityInfo.ITEM_STATUS.DONE);
                    } else {
                        currentItem.getItemsProduct().get(i).setItemStatus(ItemProductQuantityInfo.ITEM_STATUS.OTHER);
                    }

                    try {
                        BuyListFragment.this.updateTotal();
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }
                }
            });
            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProductInfo product = new ProductInfo();

                    product.setProdId(item.getBuyProduct().getProdId());
                    product.setCurrentPrice(item.getBuyProduct().getCurrentPrice());
                    product.setName(item.getBuyProduct().getName());

                    QuantityOrder quantity = item.getBuyQuantity();

                    BuyQtdeDialog dialog = new BuyQtdeDialog(product, quantity, new CardAdapterForCategory.CardCallback() {

                        @Override
                        public void process(ProductInfo prod, QuantityOrder qtde) throws Exception {
                            item.setBuyProduct(ProductInfoNoEntity.clone(prod));

                            item.setBuyQuantity(qtde);

                            updateTotal();

                            BuyListFragment.BuyListAdapter ca = (BuyListAdapter) productsListView.getAdapter();
                            ca.notifyItemChanged(i);
                        }
                    }, null);
                    dialog.show(BuyListFragment.this.getActivity());

                }
            });

            boolean isSel = (ItemProductQuantityInfo.ITEM_STATUS.DONE.equals(item.getItemStatus()) ? true : false);

            MyLogger.info("cheSel: " + isSel);
            vh.checkSel.setChecked(isSel);

        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onCreateViewHolder(android.view.ViewGroup, int)
         */
        @Override
        public BuyListFragment.BuyListAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.buy_list_product_item, viewGroup, false);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                }
            });

            return new BuyListFragment.BuyListAdapter.CardViewHolder(itemView);
        }

        public List<ItemProductQuantityInfo> getItemsProduct() {
            return itemsProduct;
        }

        public void clearAdapter() {
            this.itemsProduct = null;
        }

        /**
         * The Class CardViewHolder is the View Holder class for Adapter views.
         */
        public class CardViewHolder extends RecyclerView.ViewHolder {

            /**
             * The lbl3.
             */
            protected TextView lbl1, lbl3, btnRemove;
            protected CheckBox checkSel;

            // protected TextView btnAdd;

            /**
             * The img.
             */
            protected ImageView img;

            /**
             * Instantiates a new card view holder.
             *
             * @param v the v
             */
            public CardViewHolder(View v) {
                super(v);
                lbl1 = (TextView) v.findViewById(R.id.lbl1);
                lbl3 = (TextView) v.findViewById(R.id.lbl3);
                img = (ImageView) v.findViewById(R.id.img);
                btnRemove = (TextView) v.findViewById(R.id.btnRemove);
                checkSel = (CheckBox) v.findViewById(R.id.checkSel);

            }
        }

    }


    public void updateTotal() throws Exception {
        try {
            int counter = 0;
            BigDecimal total = new BigDecimal(0);
            List<ItemProductQuantityInfo> products = currentItem.getItemsProduct();
            for (ItemProductQuantityInfo product : products) {
                if (ItemProductQuantityInfo.ITEM_STATUS.DONE.equals(product.getItemStatus())) {

                    QuantityOrder qtde = product.getBuyQuantity();

                    BigDecimal priceItem = product.getBuyProduct().getCurrentPrice().getValue1();
                    priceItem = priceItem.multiply(new BigDecimal(qtde.getBuyQuantity()));

                    total = total.add(priceItem);
                    counter++;
                }
            }

            textViewTotalRS.setText("R$ " + n_br.format(total));
            textViewQtde.setText(counter + "");

        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    public void initBuyList(View v) {
        try {
            this.buyListView = (RecyclerView) v.findViewById(R.id.buyList);
            this.buyListView.setHasFixedSize(true);
            this.buyListView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            llm.setOrientation(LinearLayoutManager.HORIZONTAL);
            this.buyListView.setLayoutManager(llm);


            if (this.buyLists == null) {
                updateBuyList(null);
            } else {
                MyAdapter mAdapter = new MyAdapter(this.buyLists);
                buyListView.setAdapter(mAdapter);
            }

            Display display = this.getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;

            this.buyListView.setMinimumHeight(height / 3);
            this.buyListView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (currentItem != null) {
                            int counter = 0;
                            for (counter = 0; counter < buyLists.size(); counter++) {
                                if (buyLists.get(counter).getBuyListId().equals(currentItem.getBuyListId())) {
                                    break;
                                }
                            }
                            buyListView.findViewHolderForPosition(counter).itemView.performClick();
                        } else {
                            buyListView.findViewHolderForPosition(0).itemView.performClick();
                        }
                    } catch (Exception e) {

                    }
                }
            }, 300);

        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    public void updateBuyList(final String buyListName) throws Exception {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            public WeakReference<List<BuyListInfo>> buyLists = new WeakReference<List<BuyListInfo>>(new ArrayList<BuyListInfo>());

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    this.buyLists = OfertaDigitalRestClient.getRef().findBuyList();
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                BuyListFragment.this.buyLists = this.buyLists.get();

                if (BuyListFragment.this.buyLists == null) {
                    BuyListFragment.this.buyLists = new ArrayList<>();
                }

                MyAdapter mAdapter = new MyAdapter(BuyListFragment.this.buyLists);
                buyListView.setAdapter(mAdapter);


                buyListView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (buyListName != null) {
                                int counter = 0;
                                for (counter = 0; counter < BuyListFragment.this.buyLists.size(); counter++) {
                                    if (BuyListFragment.this.buyLists.get(counter).getName().equals(buyListName)) {
                                        break;
                                    }
                                }

                                buyListView.findViewHolderForPosition(counter).itemView.performClick();
                            } else {
                                buyListView.findViewHolderForPosition(0).itemView.performClick();
                            }
                        } catch (Exception e) {

                        }

                        linlaHeaderProgress.setVisibility(View.GONE);
                    }
                }, 300);


            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    public class MyAdapter extends RecyclerView.Adapter<BuyListViewHolder> {


        private DecimalFormat n_br;
        private List<BuyListInfo> buyListItems;
        private int selectedPosition;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder


        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<BuyListInfo> orderItems) {

            this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
            this.n_br.applyPattern("###,###,##0.00");
            this.buyListItems = orderItems;
        }


        private View lastView;

        // Create new views (invoked by the layout manager)
        // @Override
        public BuyListViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                    int viewType) {
            // create a new view
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.buy_list_item, viewGroup, false);

            itemView.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View v) {
                    if (lastView != null) {
                        if (lastView instanceof CardView) {
                            CardView cc = (CardView) lastView;
                            cc.setCardBackgroundColor(Color.WHITE);
                        }
                    }

                    lastView = v;
                    if (v instanceof CardView) {
                        CardView cc = (CardView) v;
                        cc.setCardBackgroundColor(Color.GRAY);
                    }

                    //btnAddProdBuy.setVisibility(View.VISIBLE);
                    //btnSaveList.setVisibility(View.VISIBLE);

                    int position = buyListView.getChildPosition(v);

                    selectedPosition = position;
                    BuyListInfo item = buyListItems.get(position);

                    BuyListFragment.this.currentItem = item;

                    {
                        try {
                            BuyListFragment.this.updateTotal();
                        } catch (Exception e) {
                            MyLogger.log(e);
                        }
                    }

                    {
                        RecyclerView.Adapter adapter = productsListView.getAdapter();
                        if (adapter instanceof BuyListAdapter) {
                            MyLogger.info("clear productsListView: " + adapter);
                            ((BuyListAdapter) adapter).clearAdapter();

                            try {
                                Runtime.getRuntime().gc();
                            } catch (Exception e) {
                                MyLogger.log(e);
                            }
                        }
                        productsListView.setAdapter(new BuyListAdapter(item.getItemsProduct()));
                    }
                    /*
                    {
                        btnSaveList.setVisibility(View.GONE);
                        btnAddProdBuy.setVisibility(View.GONE);
                    }
                    */
                }

            });


            BuyListViewHolder vh = new BuyListViewHolder(itemView);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final BuyListViewHolder holder, final int position) {

            final BuyListInfo buyListItem = this.buyListItems.get(position);
            holder.lbl2.setText(buyListItem.getName());


        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return this.buyListItems.size();
        }
    }

    public static class BuyListViewHolder extends RecyclerView.ViewHolder {

        /**
         * The textviews.
         */
        protected TextView lbl2, lbl3, lbl4;
        // protected TextView btnRemove, btnEdit, btnBuy;


        /**
         * Instantiates a new card view holder.
         *
         * @param v the v
         */
        public BuyListViewHolder(View v) {
            super(v);
            lbl2 = (TextView) v.findViewById(R.id.lbl2);
            lbl3 = (TextView) v.findViewById(R.id.lbl3);
            lbl4 = (TextView) v.findViewById(R.id.lbl4);
            // btnRemove = (TextView) v.findViewById(R.id.btnRemove);
            // btnEdit = (TextView) v.findViewById(R.id.btnEdit);
            // btnBuy = (TextView) v.findViewById(R.id.btnBuy);
            //  btnRemove.setVisibility(View.GONE);

        }

        public View getItemView() {
            return itemView;
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (data != null) {
                MyLogger.info("onActivityResult fragment data: " + data.getSerializableExtra("prod"));
                MyLogger.info("buy lists: " + this.buyLists);
                MyLogger.info("current buy list: " + this.currentItem);

                for (BuyListInfo buyList : this.buyLists) {
                    if (buyList.getBuyListId().equals(this.currentItem.getBuyListId())) {
                        this.currentItem = buyList;
                        break;
                    }
                }


                ItemProductQuantityInfo itemProd = new ItemProductQuantityInfo();
                itemProd.setBuyProduct(ProductInfoNoEntity.clone((ProductInfo) data.getSerializableExtra("prod")));
                itemProd.setBuyQuantity((QuantityOrder) data.getSerializableExtra("qtde"));
                ItemId itemId = new ItemId();
                itemId.setItemId("item" + UUID.randomUUID().toString());
                itemId.setUserId(OfertaDigitalRestClient.getRef().getUserId());
                itemProd.setItemId(itemId);
                itemProd.setItemStatus(ItemProductQuantityInfo.ITEM_STATUS.OTHER);

                this.currentItem.getItemsProduct().add(itemProd);

                for (ItemProductQuantityInfo q : this.currentItem.getItemsProduct()) {
                    MyLogger.info("itemStatus: " + q.getItemStatus());
                }

                MyLogger.info("new current buy list: " + this.currentItem);


            }
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.list_op_menu, menu);

        // super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            OfertaDigitalRestClient.getRef().processBuyListOp(BuyListFragment.this.currentItem.getBuyListId(), BuyListFragment.this.currentItem.getName(), BuyListFragment.this.currentItem.getDescription(), null, BuyListOpRequestData.BUYLIST_OP.DELETE);
                        } catch (Exception e) {
                            MyLogger.log(e);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        currentItem = null;
                        try {
                            updateBuyList(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };

                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                return true;

            case R.id.menu_edit:
                btnSaveList.setVisibility(View.VISIBLE);
                btnAddProdBuy.setVisibility(View.VISIBLE);

                return true;

            case R.id.menu_buy:
                try {
                    BuyCartRemoteImpl.getRef().clearBuyCart();
                    RecyclerView.Adapter adapter = productsListView.getAdapter();
                    if (adapter instanceof BuyListFragment.BuyListAdapter) {
                        BuyListFragment.BuyListAdapter adp1 = (BuyListFragment.BuyListAdapter) adapter;
                        List<ItemProductQuantityInfo> items = adp1.getItemsProduct();

                        for (ItemProductQuantityInfo item1 : items) {
                            if (!ItemProductQuantityInfo.ITEM_STATUS.DONE.equals(item1.getItemStatus())) {
                                continue;
                            }

                            ProductInfo product1 = new ProductInfo();

                            product1.setProdId(item1.getBuyProduct().getProdId());
                            product1.setName(item1.getBuyProduct().getName());
                            product1.setCurrentPrice(item1.getBuyProduct().getCurrentPrice());

                            QuantityOrder qtde1 = item1.getBuyQuantity();


                            BuyCartRemoteImpl.getRef().addProduct(product1, qtde1);
                        }
                    }

                    MyOnMenuItemClickListener listener = new MyOnMenuItemClickListener(BuyListFragment
                            .this, BuyListFragment
                            .this.linlaHeaderProgress, BuyListFragment.this.storeInfo);
                    listener.onMenuItemClick(null);

                    return true;
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                return false;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
