package com.glam.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.glam.creditcard.library.CreditCard;
import com.glam.creditcard.library.CreditCardForm;
import com.github.chrisbanes.photoview.PhotoView;
import com.glam.MainActivity;
import com.glam.R;
import com.glam.custom.CustomFragment;
import com.glam.dialog.AddressRegisterDialog;
import com.glam.dialog.MyDialogs;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.model.Data;
import com.glam.transport.LocalConvenioInfo;
import com.glam.utils.MyLogger;

import br.com.innove.ofertadigital.persistence.Address;
import br.com.innove.ofertadigital.persistence.AddressInfo;
import br.com.innove.ofertadigital.persistence.PaymentInfo;

/**
 * The Class Settings is the fragment that shows various settings options.
 */
public class Settings extends CustomFragment {

    /**
     * The category list.
     */
    private ArrayList<Data> iList;
    private RecyclerView addrList;
    private RecyclerView cardList;
    private RecyclerView convenioList;
    private LinearLayout linlaHeaderProgress;

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.settings, null);


        this.linlaHeaderProgress = (LinearLayout) v.findViewById(R.id.linlaHeaderProgress);


        ((MainActivity) getActivity()).toolbar.setTitle("Preferências");
        ((MainActivity) getActivity()).toolbar.findViewById(
                R.id.spinner_toolbar).setVisibility(View.GONE);
        {
            this.addrList = (RecyclerView) v.findViewById(R.id.addrList);
            this.addrList.setHasFixedSize(true);

            LinearLayoutManager llm = new LinearLayoutManager(this.getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            this.addrList.setLayoutManager(llm);

            AddressListAdapter adapter = new AddressListAdapter(new ArrayList<AddressInfo>());
            this.addrList.setAdapter(adapter);
        }
        {
            this.cardList = (RecyclerView) v.findViewById(R.id.cardList);
            this.cardList.setHasFixedSize(true);

            LinearLayoutManager llm = new LinearLayoutManager(this.getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            this.cardList.setLayoutManager(llm);

            CreditCardListAdapter adapter = new CreditCardListAdapter(new ArrayList<PaymentInfo>());
            this.cardList.setAdapter(adapter);
        }

        {
            this.convenioList = (RecyclerView) v.findViewById(R.id.convenioList);
            this.convenioList.setHasFixedSize(true);

            LinearLayoutManager llm = new LinearLayoutManager(this.getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            this.convenioList.setLayoutManager(llm);

            ConvenioListAdapter adapter = new ConvenioListAdapter(new ArrayList<LocalConvenioInfo>());
            this.convenioList.setAdapter(adapter);
        }


        {
            TextView btnAdd = (TextView) v.findViewById(R.id.btnAdd);
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AddressInfo addr = new AddressInfo();
                    addr.setAddress(new Address());
                    final AddressRegisterDialog dialog = new AddressRegisterDialog(addr);
                    dialog.setListener(new AddressRegisterDialog.AddressListener() {

                        @Override
                        public void process(final AddressInfo addressInfo) {
                            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                                public Exception e;

                                @Override
                                protected Void doInBackground(Void... voids) {
                                    try {
                                        OfertaDigitalRestClient.getRef().addAddress(addressInfo);
                                    } catch (Exception e) {
                                        MyLogger.log(e);
                                        this.e = e;
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    if (this.e == null) {
                                        dialog.close();
                                        updateList();
                                    }
                                }
                            };
                            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    });
                    dialog.show(Settings.this.getActivity());
                }
            });
        }

        {
            Button btnAddCard = (Button) v.findViewById(R.id.btnAddCard);
            btnAddCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v1) {
                    final CreditCardForm creditcard = (CreditCardForm) v.findViewById(R.id.credit_card_form);
                    EditText holder = (EditText) v.findViewById(R.id.textHolder);

                    CreditCard card = creditcard.getCreditCard();

                    if (!creditcard.isCreditCardValid()) {
                        MyDialogs.showError(Settings.this.getContext(), "Cadastro de cartão", "Numero de cartão invalido!");

                        MyDialogs.showConfirm(Settings.this.getContext(), "Cadastro de cartão", "Numero de cartão invalido!", new MyDialogs.Callback() {
                            @Override
                            public void processYes() {

                            }

                            @Override
                            public void processNo() {

                            }
                        });
                        return;
                    }


                    final PaymentInfo payment = new PaymentInfo();
                    payment.setCardNumber(card.getCardNumber());
                    payment.setCardCvv(card.getSecurityCode());
                    payment.setCardDd(card.getExpDate());
                    payment.setCardHolder(holder.getText().toString());
                    payment.setTypePay(PaymentInfo.PAY_TYPE.CARD);

                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                        public Exception e;

                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                OfertaDigitalRestClient.getRef().opPaymemt(payment, "add");
                            } catch (Exception e) {
                                MyLogger.log(e);
                                this.e = e;
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if (this.e == null) {
                                creditcard.clearForm();
                                updateList();
                            }
                            linlaHeaderProgress.setVisibility(View.GONE);
                        }


                        @Override
                        protected void onPreExecute() {
                            linlaHeaderProgress.setVisibility(View.VISIBLE);
                        }
                    };
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });
        }


        {
            Button btnAddConvenio = (Button) v.findViewById(R.id.btnAddConvenio);
            final EditText textEmpresa = (EditText) v.findViewById(R.id.textEmpresa);
            final EditText textRegistro = (EditText) v.findViewById(R.id.textRegistro);

            btnAddConvenio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v1) {
                    final LocalConvenioInfo convenio = new LocalConvenioInfo();

                    convenio.setCreatedAt(Calendar.getInstance().getTime());
                    convenio.setEmpresa(textEmpresa.getText().toString());
                    convenio.setRegistro(textRegistro.getText().toString());

                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                        public Exception e;

                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                OfertaDigitalRestClient.getRef().opConvenio(convenio, "add");
                            } catch (Exception e) {
                                MyLogger.log(e);
                                this.e = e;
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if (this.e == null) {
                                textEmpresa.setText("");
                                textRegistro.setText("");
                                updateList();
                            }else{
                                MyLogger.log(e);
                            }
                            linlaHeaderProgress.setVisibility(View.GONE);
                        }


                        @Override
                        protected void onPreExecute() {
                            linlaHeaderProgress.setVisibility(View.VISIBLE);
                        }
                    };
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });
        }
        {
            updateList();
        }
        return v;
    }


    public void updateList() {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            public List<AddressInfo> addresses;
            public List<PaymentInfo> payments;
            public List<LocalConvenioInfo> convenios;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    this.addresses = OfertaDigitalRestClient.getRef().getAddress();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                try {
                    this.payments = OfertaDigitalRestClient.getRef().getPayments();
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                try {
                    this.convenios = OfertaDigitalRestClient.getRef().getConvenios();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (this.addresses != null) {
                    AddressListAdapter adp = (AddressListAdapter) addrList.getAdapter();
                    adp.setAddresses(this.addresses);
                    adp.notifyDataSetChanged();
                }

                if (this.payments != null) {
                    CreditCardListAdapter adp = (CreditCardListAdapter) cardList.getAdapter();
                    adp.setPayments(this.payments);
                    adp.notifyDataSetChanged();
                }

                if (this.convenios != null) {
                    ConvenioListAdapter adp = (ConvenioListAdapter) convenioList.getAdapter();
                    adp.setPayments(this.convenios);
                    adp.notifyDataSetChanged();
                }

                linlaHeaderProgress.setVisibility(View.GONE);
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /* (non-Javadoc)
     * @see com.whatshere.custom.CustomFragment#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        super.onClick(v);
    }


    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    public class AddressListAdapter extends
            RecyclerView.Adapter<Settings.AddressViewHolder> {

        private List<AddressInfo> addresses;

        public AddressListAdapter(List<AddressInfo> addresses) {
            this.addresses = addresses;
        }

        @Override
        public AddressViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.address_item, viewGroup, false);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });


            return new AddressViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(AddressViewHolder holder, int position) {
            final AddressInfo addr = this.addresses.get(position);
            holder.lblLgr.setText(addr.getAddress().getXLgr() + ", " + addr.getAddress().getNro());
            holder.lbl3.setText("Bairro: " + addr.getAddress().getXBairro() + " CEP: " + addr.getAddress().getCEP());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final AddressRegisterDialog dialog = new AddressRegisterDialog(addr);
                    dialog.setListener(new AddressRegisterDialog.AddressListener() {
                        @Override
                        public void process(final AddressInfo addressInfo) {
                            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    try {
                                        OfertaDigitalRestClient.getRef().addAddress(addressInfo);
                                    } catch (Exception e) {
                                        MyLogger.log(e);
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    dialog.close();
                                }
                            };
                            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        }
                    });
                    dialog.show(Settings.this.getActivity());


                    MyLogger.info(addr.getAddress().getXLgr());
                }
            });
            holder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                        public Exception e;

                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                OfertaDigitalRestClient.getRef().removeAddress(addr);
                            } catch (Exception e) {
                                MyLogger.log(e);
                                this.e = e;
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if (this.e == null) {
                                updateList();
                            }
                            linlaHeaderProgress.setVisibility(View.GONE);
                        }

                        @Override
                        protected void onPreExecute() {
                            linlaHeaderProgress.setVisibility(View.VISIBLE);
                        }
                    };
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });
        }

        public List<AddressInfo> getAddresses() {
            return addresses;
        }

        public void setAddresses(List<AddressInfo> addresses) {
            this.addresses = addresses;
        }

        @Override
        public int getItemCount() {
            return this.addresses.size();
        }
    }

    public class AddressViewHolder extends RecyclerView.ViewHolder {
        protected TextView lblLgr, lbl3;
        protected TextView btnRemove;
        protected ImageView img;

        public AddressViewHolder(View v) {
            super(v);
            lblLgr = (TextView) v.findViewById(R.id.lblLgr);
            btnRemove = (TextView) v.findViewById(R.id.btnRemove);
            lbl3 = (TextView) v.findViewById(R.id.lbl3);
        }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    public class CreditCardListAdapter extends
            RecyclerView.Adapter<Settings.CreditCardViewHolder> {

        private List<PaymentInfo> payments;

        public CreditCardListAdapter(List<PaymentInfo> payments) {
            this.payments = payments;
        }

        public void setPayments(List<PaymentInfo> payments) {
            this.payments = payments;
        }

        @Override
        public CreditCardViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.payment_item, viewGroup, false);

            return new CreditCardViewHolder(itemView);
        }

        private String hideCreditCardInfo(PaymentInfo payment) {

            String[] n_parts = payment.getCardNumber().split(" ");

            if (n_parts.length == 4) {
                return "**** **** **** " + n_parts[3] + " Venc: " + payment.getCardDd();
            } else {
                return "**** **** **** ****";
            }
        }

        @Override
        public void onBindViewHolder(CreditCardViewHolder holder, int position) {
            final PaymentInfo payment = this.payments.get(position);
            holder.lblCardNumber.setText(hideCreditCardInfo(payment));
            holder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                        public Exception e;

                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                OfertaDigitalRestClient.getRef().opPaymemt(payment, "del");
                            } catch (Exception e) {
                                MyLogger.log(e);
                                this.e = e;
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if (this.e == null) {
                                updateList();
                            }
                            linlaHeaderProgress.setVisibility(View.GONE);
                        }

                        @Override
                        protected void onPreExecute() {
                            linlaHeaderProgress.setVisibility(View.VISIBLE);
                        }
                    };
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });
        }

        @Override
        public int getItemCount() {
            return this.payments.size();
        }
    }

    public class CreditCardViewHolder extends RecyclerView.ViewHolder {
        protected TextView lblCardNumber;
        protected TextView btnRemove;

        public CreditCardViewHolder(View v) {
            super(v);
            lblCardNumber = (TextView) v.findViewById(R.id.lblCardNumber);
            btnRemove = (TextView) v.findViewById(R.id.btnRemove);
        }

    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    public class ConvenioListAdapter extends
            RecyclerView.Adapter<Settings.ConvenioViewHolder> {

        private List<LocalConvenioInfo> convenios;

        public ConvenioListAdapter(List<LocalConvenioInfo> payments) {
            this.convenios = payments;
        }

        public void setPayments(List<LocalConvenioInfo> payments) {
            this.convenios = payments;
        }

        @Override
        public ConvenioViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.payment_item, viewGroup, false);

            return new ConvenioViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(ConvenioViewHolder holder, int position) {
            final LocalConvenioInfo convenio = this.convenios.get(position);
            holder.lblCardNumber.setText(convenio.getEmpresa());
            holder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                        public Exception e;

                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                OfertaDigitalRestClient.getRef().opConvenio(convenio, "del");
                            } catch (Exception e) {
                                MyLogger.log(e);
                                this.e = e;
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if (this.e == null) {
                                updateList();
                            }
                            linlaHeaderProgress.setVisibility(View.GONE);
                        }

                        @Override
                        protected void onPreExecute() {
                            linlaHeaderProgress.setVisibility(View.VISIBLE);
                        }
                    };
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });
        }

        @Override
        public int getItemCount() {
            return this.convenios.size();
        }
    }

    public class ConvenioViewHolder extends RecyclerView.ViewHolder {
        protected TextView lblCardNumber;
        protected TextView btnRemove;

        public ConvenioViewHolder(View v) {
            super(v);
            lblCardNumber = (TextView) v.findViewById(R.id.lblCardNumber);
            btnRemove = (TextView) v.findViewById(R.id.btnRemove);
        }

    }
}
