package com.glam.ui;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.Nullable;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import android.support.v7.widget.SearchView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.glam.CheckoutActivity;
import com.glam.MainActivity;
import com.glam.R;
import com.glam.buycart.BuyCartRemoteImpl;
import com.glam.buycart.ItemProductIdQtde;
import com.glam.custom.CustomFragment;
import com.glam.dialog.BuyQtdeDialog;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.model.Data;
import com.glam.utils.MyLogger;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.innove.ofertadigital.persistence.Address;
import br.com.innove.ofertadigital.persistence.AddressInfo;
import br.com.innove.ofertadigital.persistence.CategoryInfo;
import br.com.innove.ofertadigital.persistence.PaymentInfo;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.persistence.StoreInfo;


/**
 * The Class Checkout is the fragment that shows the list products for checkout
 * and show the credit card details as well. You need to load and display actual
 * contents.
 */
public class ShowCategoriesHorizontalFragment extends CustomFragment {

    /**
     * The product list.
     */
    private ArrayList<Data> iList;
    private RecyclerView recList;
    private RecyclerView orderList;
    private DecimalFormat n_br;
    private int position;
    private int lastCompVisiblePosition;
    private int firstCompVisiblePosition;
    private int categorySize;
    private Button btnRight;
    private Button btnLeft;
    private View mainView;

    private StoreInfo storeInfo;
    private LinearLayout linlaHeaderProgress;
    private boolean show_categories;
    private String title;

    public ShowCategoriesHorizontalFragment() {
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");
        this.position = 0;
        this.show_categories = true;
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    @SuppressLint({"InflateParams", "InlinedApi"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle args = getArguments();
        if (args != null && args.containsKey("storeinfo")) {
            this.storeInfo = (StoreInfo) args.getSerializable("storeinfo");
        }

        if (args != null && args.containsKey("show_categories")) {
            this.show_categories = args.getBoolean("show_categories", true);
        }

        if (args != null && args.containsKey("title")) {
            this.title = args.getString("title");
        }

        this.mainView = inflater.inflate(R.layout.show_categories_horizontal, null);

        try {
            if (getActivity() instanceof MainActivity) {
                ((MainActivity) getActivity()).toolbar.setTitle(this.title);
                ((MainActivity) getActivity()).toolbar.findViewById(
                        R.id.spinner_toolbar).setVisibility(View.GONE);
            } else {
                ((CheckoutActivity) getActivity()).getSupportActionBar().setTitle(
                        "Produtos");
            }
        } catch (Throwable e) {
            MyLogger.info("calculate error: " + e.getLocalizedMessage());
        }

        this.linlaHeaderProgress = (LinearLayout) this.mainView.findViewById(R.id.linlaHeaderProgress);


		/*if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP)
        {
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
			getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		}*/

        // setTouchNClick(this.mainView.findViewById(R.id.btnDone));
        setHasOptionsMenu(true);
        setupView(this.mainView);
        initOrderList(this.mainView);

        this.btnLeft = (Button) this.mainView.findViewById(R.id.btnLeft);
        this.btnLeft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (firstCompVisiblePosition > 0) {
                    recList.smoothScrollToPosition(firstCompVisiblePosition - 1);
                }
            }
        });
        // this.btnLeft.getBackground().setAlpha(64);
        this.btnLeft.setVisibility(View.GONE);

        this.btnRight = (Button) this.mainView.findViewById(R.id.btnRight);
        this.btnRight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lastCompVisiblePosition < categorySize) {
                    recList.smoothScrollToPosition(lastCompVisiblePosition + 1);
                }
            }
        });
        // this.btnRight.getBackground().setAlpha(64);

        /*Button btnDone = (Button) this.mainView.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/


        MyLogger.info("onCreateView: " + this);

        return this.mainView;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        MyLogger.info("onViewStateRestored: " + this);
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLogger.info("onResume: " + this);
    }

    @Override
    public void onDestroyView() {
        MyLogger.info("onDestroyView: " + this);
        super.onDestroyView();

        try {
            this.unbindDrawables(this.mainView);
        } catch (Exception e) {
            MyLogger.log(e);
        }

        this.orderList = null;
        this.recList = null;

        this.mainView = null;

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    /* (non-Javadoc)
             * @see com.whatshere.custom.CustomFragment#onClick(android.view.View)
             */
    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    /**
     * Setup the view components for this fragment. You write your code for
     * initializing the views, setting the adapters, touch and click listeners
     * etc.
     *
     * @param v the base view of fragment
     */
    private void setupView(View v) {
        this.iList = new ArrayList<>();

        this.recList = (RecyclerView) v.findViewById(R.id.cardList);

        /*{
            RelativeLayout panel = (RelativeLayout) v.findViewById(R.id.category_panel);
            panel.setVisibility(this.show_categories ? View.VISIBLE : View.GONE);
        }*/

        this.recList.setHasFixedSize(true);
        this.recList.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        this.recList.setLayoutManager(llm);
        final ShowCategoriesHorizontalFragment.CardAdapter ca = new ShowCategoriesHorizontalFragment.CardAdapter();
        this.recList.setAdapter(ca);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                MyLogger.info("doInBackground called");
                loadDummyData();
                MyLogger.info("doInBackground: iList.size(): " + iList.size());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                try {
                    MyLogger.info("notify change size: " + iList.size());
                    final ShowCategoriesHorizontalFragment.CardAdapter ca = new ShowCategoriesHorizontalFragment.CardAdapter();
                    ShowCategoriesHorizontalFragment.this.recList.setAdapter(ca);

                    try {
                        updateTotal();
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }

                    linlaHeaderProgress.setVisibility(View.GONE);
                }catch (Exception e){
                    MyLogger.log(e);
                }
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        MyLogger.info("task called: " + task);

        this.recList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // MyLogger.info("scroll listener called");

                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = ((LinearLayoutManager) recList.getLayoutManager());
                firstCompVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                lastCompVisiblePosition = layoutManager.findLastVisibleItemPosition();

                if (lastCompVisiblePosition >= (categorySize - 1)) {
                    btnRight.setVisibility(View.GONE);
                } else {
                    btnRight.setVisibility(View.VISIBLE);
                }

                if (firstCompVisiblePosition <= 0) {
                    btnLeft.setVisibility(View.GONE);
                } else {
                    btnLeft.setVisibility(View.VISIBLE);
                }
                // position = (int) ((lastCompVisiblePosition + firstCompVisiblePosition) / 2);
            }
        });


    }


    public void initOrderList(View v) {
        try {
            this.orderList = (RecyclerView) v.findViewById(R.id.orderList);
            this.orderList.setHasFixedSize(true);
            this.orderList.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            this.orderList.setLayoutManager(llm);


            MyAdapter mAdapter = new MyAdapter(new ArrayList<ItemProductIdQtde>());
            this.orderList.setAdapter(mAdapter);

            // setUpItemTouchHelper(orderList);

        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    public void updateTotal() throws Exception {
        try {
            MyLogger.info("updateTotal");
            TextView textTotalRS = (TextView) getView().findViewById(R.id.textViewTotalRS);
            TextView textQtde = (TextView) getView().findViewById(R.id.textViewQtde);

            BuyCartRemoteImpl.getRef().calculateTotal();

            textTotalRS.setText("Total: R$ " + n_br.format(BuyCartRemoteImpl.getRef().getTotalBuy()));
            textQtde.setText("Produtos: " + BuyCartRemoteImpl.getRef().getItemList().size() + "");

            List<ItemProductIdQtde> orderItems = BuyCartRemoteImpl.getRef().getItemList();
            this.orderList.setAdapter(new MyAdapter(orderItems));
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MyLogger.info("ShowCategoriesHorizontalFragment onOptionsItemSelected: " + item.getItemId());
        switch (item.getItemId()) {
            case android.R.id.home:
                ShowCategoriesHorizontalFragment.this.getActivity().getSupportFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * The Class CardAdapterForCategory is the adapter for showing products in Card format
     * inside the RecyclerView. It shows dummy product image and dummy contents,
     * so you need to display actual contents as per your need.
     */
    private class CardAdapter extends
            RecyclerView.Adapter<CardAdapter.CardViewHolder> {

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#getItemCount()
         */
        @Override
        public int getItemCount() {
            return iList.size();
        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder, int)
         */
        @Override
        public void onBindViewHolder(CardViewHolder vh, int i) {

            Data d = iList.get(i);
            String label = d.getTexts()[0].toUpperCase();

            if ("PERECIVEIS".equals(label)) {
                label = "PERECÍVEIS";
            }

            if ("ACOUGUE".equals(label)) {
                label = "AÇOUGUE";
            }

            if("PES_MAOS".equals(label)){
                label = "PES E MAOS";
            }

            if("INFECCOES".equals(label)){
                label = "INFLAMAÇÃO";
            }


            vh.lbl1.setText(label);
            vh.lbl2.setText(d.getTexts()[1]);
            //  vh.lbl3.setText(d.getTexts()[2]);

            try {
                Context context = vh.img.getContext();

                String rc_name = "cat_" + d.getTexts()[0].toLowerCase();

                int id = context.getResources().getIdentifier(rc_name, "drawable", context.getPackageName());
                MyLogger.info("cat name: " + rc_name + " id cat img: " + id);
                if (id != 0) {
                    //vh.img.getLayoutParams().height = 100;
                    vh.img.setImageResource(id);
                    vh.lbl2.setTextColor(Color.WHITE);
                } else {
                    vh.img.setImageResource(R.drawable.imagem_nao_encontrada);
                    vh.lbl2.setTextColor(Color.BLACK);
                }
            } catch (Exception e) {
                MyLogger.log(e);
            }
        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onCreateViewHolder(android.view.ViewGroup, int)
         */
        @Override
        public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.simple_categ_item, viewGroup, false);
            itemView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    //startActivity(new Intent(getActivity(), ProductDetail.class));
                    int position = recList.getChildPosition(v);

                    MyLogger.info("clicked category: " + iList.get(position).getTexts()[0]);

                    MainFragment fragment = new MainFragment();

                    Bundle args = new Bundle();
                    args.putCharSequence("category", iList.get(position).getTexts()[0]);
                    args.putSerializable("called", MainFragment.CALLED_FROM.BUYCART);
                    args.putSerializable("storeinfo", storeInfo);
                    fragment.setArguments(args);

                    ShowCategoriesHorizontalFragment.this.getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, fragment, "added_fragment").addToBackStack("something").commit();

                    AppCompatActivity actionbar = (AppCompatActivity) ShowCategoriesHorizontalFragment.this.getActivity();
                    if (actionbar.getSupportActionBar() != null) {
                        actionbar.getSupportActionBar().setTitle("Produtos");
                        // actionbar.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        // actionbar.getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_tab_indicator_material);
                    }
                }
            });

            return new CardViewHolder(itemView);
        }

        /**
         * The Class CardViewHolder is the View Holder class for Adapter views.
         */
        public class CardViewHolder extends RecyclerView.ViewHolder {

            /**
             * The lbl3.
             */
            protected TextView lbl1, lbl2;//, lbl3;

            // protected TextView btnAdd;

            /**
             * The img.
             */
            protected ImageView img;

            /**
             * Instantiates a new card view holder.
             *
             * @param v the v
             */
            public CardViewHolder(View v) {
                super(v);
                lbl1 = (TextView) v.findViewById(R.id.lbl1);
                lbl2 = (TextView) v.findViewById(R.id.lbl2);
                //  lbl3 = (TextView) v.findViewById(R.id.lbl3);
                img = (ImageView) v.findViewById(R.id.img);
                // btnAdd = (TextView) v.findViewById(R.id.btnAdd);
                /*btnAdd.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MyLogger.info("called");
                        Toast.makeText(ShowCategoriesHorizontalFragment.this.getActivity(), "texto", Toast.LENGTH_SHORT).show();
                    }
                });*/
            }
        }
    }

    /**
     * Load dummy product data for displaying on the RecyclerView. You need to
     * write your own code for loading real products from Web-service or API and
     * displaying them on RecyclerView.
     */
    private void loadDummyData() {
        try {
            List<CategoryInfo> categories = OfertaDigitalRestClient.getRef().findCategories();

            for (CategoryInfo c : categories) {
                if (c.getName().toLowerCase().equals("categoria")) {
                    continue;
                }

                Data data = new Data(new String[]{c.getName(), "Alguma descrição ",
                        ""}, new int[]{R.drawable.imagem_nao_encontrada});

                iList.add(data);
            }

            categorySize = categories.size();
            MyLogger.info("loaded data: " + iList.size());
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }


    protected void setMenuBackground() {


        getActivity().getLayoutInflater().setFactory(new LayoutInflater.Factory() {

            @Override
            public View onCreateView(String name, Context context, AttributeSet attrs) {

                if (name.equalsIgnoreCase("com.android.internal.view.menu.IconMenuItemView")) {

                    try { // Ask our inflater to create the view
                        LayoutInflater f = getActivity().getLayoutInflater();
                        final View view = f.createView(name, null, attrs);
                            /*
                             * The background gets refreshed each time a new item is added the options menu.
                             * So each time Android applies the default background we need to set our own
                             * background. This is done using a thread giving the background change as runnable
                             * object
                             */
                        new Handler().post(new Runnable() {
                            public void run() {
                                view.setBackgroundResource(R.drawable.button_like);
                            }
                        });
                        return view;
                    } catch (Exception e) {

                    }

                }
                return null;
            }
        });
    }

    private Bitmap resizeBitmapImageFn(
            Bitmap bmpSource, int maxResolution) {
        int iWidth = bmpSource.getWidth();
        int iHeight = bmpSource.getHeight();
        int newWidth = iWidth;
        int newHeight = iHeight;
        float rate = 0.0f;

        if (iWidth > iHeight) {
            if (maxResolution < iWidth) {
                rate = maxResolution / (float) iWidth;
                newHeight = (int) (iHeight * rate);
                newWidth = maxResolution;
            }
        } else {
            if (maxResolution < iHeight) {
                rate = maxResolution / (float) iHeight;
                newWidth = (int) (iWidth * rate);
                newHeight = maxResolution;
            }
        }

        return Bitmap.createScaledBitmap(
                bmpSource, iWidth * 2, iHeight * 2, true);
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateOptionsMenu(android.view.Menu, android.view.MenuInflater)
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_exp, menu);
        // setMenuBackground();

        menu.findItem(R.id.menu_grid).setVisible(false);
        menu.findItem(R.id.menu_search).setVisible(true);
        menu.findItem(R.id.menu_buy_done).setVisible(true);
        menu.findItem(R.id.menu_buy).setVisible(false);


        // super.onCreateOptionsMenu(menu, inflater);

       /*{
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.btn_comprar); //Converting drawable into bitmap
            Bitmap new_icon = resizeBitmapImageFn(icon, 100); //resizing the bitmap
            Drawable d = new BitmapDrawable(getResources(), new_icon); //Converting bitmap into drawable

            menu.findItem(R.id.menu_buy_done).setIcon(d);
        }*/

        final MenuItem item = menu.findItem(R.id.menu_search);
        SearchView searchView = new SearchView(getActivity());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MyLogger.info("ShowCategoriesFragment onQueryTextSubmit: " + query + " store: " + storeInfo);

                MainFragment fragment = new MainFragment();

                Bundle args = new Bundle();
                //  args.putCharSequence("category", "Resultado");
                args.putCharSequence("query", query);
                args.putSerializable("called", MainFragment.CALLED_FROM.BUYCART);
                args.putSerializable("storeinfo", storeInfo);
                fragment.setArguments(args);

                ShowCategoriesHorizontalFragment.this.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment, "added_fragment").addToBackStack("something").commit();

                android.support.v7.app.AppCompatActivity actionbar = (android.support.v7.app.AppCompatActivity) ShowCategoriesHorizontalFragment.this.getActivity();
                if (actionbar.getSupportActionBar() != null) {
                    actionbar.getSupportActionBar().setTitle("Produtos");
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        MenuItem buy_done = menu.findItem(R.id.menu_buy_done);
        buy_done.setOnMenuItemClickListener(new MyOnMenuItemClickListener(this, linlaHeaderProgress, storeInfo));
        buy_done.getActionView().setOnClickListener(new MyOnMenuItemClickListener(this, linlaHeaderProgress, storeInfo));

        MenuItem menu_buy = menu.findItem(R.id.menu_buy);
        menu_buy.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setupContainer(1);

                return true;
            }
        });
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        private final TextView btnRemove;
        /**
         * The textviews.
         */
        protected TextView lbl2, lbl3, lbl4;


        /**
         * Instantiates a new card view holder.
         *
         * @param v the v
         */
        public OrderViewHolder(View v) {
            super(v);
            lbl2 = (TextView) v.findViewById(R.id.lbl2);
            lbl3 = (TextView) v.findViewById(R.id.lbl3);
            lbl4 = (TextView) v.findViewById(R.id.lbl4);
            btnRemove = (TextView) v.findViewById(R.id.btnRemove);

        }

        public View getItemView() {
            return itemView;
        }


    }

    public class MyAdapter extends RecyclerView.Adapter<OrderViewHolder> {


        private DecimalFormat n_br;
        private List<ItemProductIdQtde> orderItems;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder


        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<ItemProductIdQtde> orderItems) {
            this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
            this.n_br.applyPattern("###,###,##0.00");
            this.orderItems = orderItems;
        }


        public OrderViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                  int viewType) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.order_item, viewGroup, false);


            //itemView.setOnClickListener();
            OrderViewHolder vh = new OrderViewHolder(itemView);
            return vh;
        }

        @Override
        public void onBindViewHolder(OrderViewHolder holder, int position) {
            final ItemProductIdQtde itemQtde = this.orderItems.get(position);

            ProductInfo product = itemQtde.getProduct();
            holder.lbl2.setText(product.getName() + "");

            BigDecimal priceValue = itemQtde.getQtde().getBuyPrice().getValue1();
            priceValue = priceValue.multiply(new BigDecimal(itemQtde.getQtde().getBuyQuantity()));

            holder.lbl3.setText("R$ " + n_br.format(priceValue));
            holder.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    BuyQtdeDialog dialog = new BuyQtdeDialog(itemQtde.getProduct(), itemQtde.getQtde(), new CardAdapterForCategory.CardCallback() {

                        @Override
                        public void process(ProductInfo prod, QuantityOrder qtde) throws Exception {
                            itemQtde.setProduct(prod);
                            itemQtde.setQtde(qtde);

                            updateTotal();
                        }
                    }, null);
                    dialog.show(ShowCategoriesHorizontalFragment.this.getActivity());
                }

            });
            holder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        orderItems.remove(itemQtde);

                        updateTotal();
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return this.orderItems.size();
        }
    }


}
