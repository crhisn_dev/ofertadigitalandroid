package com.glam.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.glam.R;
import com.glam.buycart.BuyCartRemoteImpl;
import com.glam.custom.CustomFragment;
import com.glam.dialog.MyDialogs;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.transport.LocalConvenioInfo;
import com.glam.utils.MyLogger;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

import br.com.innove.ofertadigital.persistence.Address;
import br.com.innove.ofertadigital.persistence.AddressInfo;
import br.com.innove.ofertadigital.persistence.PaymentInfo;
import br.com.innove.ofertadigital.persistence.StoreInfo;


public class MyOnMenuItemClickListener implements MenuItem.OnMenuItemClickListener, android.view.View.OnClickListener {
    private CustomFragment contentFragment;

    private StoreInfo store;
    private LinearLayout progressBar;
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);


    public static int generateViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public MyOnMenuItemClickListener(CustomFragment showCategoriesHorizontalFragment, LinearLayout progressBar, StoreInfo store) {
        this.contentFragment = showCategoriesHorizontalFragment;
        this.progressBar = progressBar;
        this.store = store;
    }

    @Override
    public void onClick(View v) {
        startBuy();
    }

    @Override
    public boolean onMenuItemClick(MenuItem xxx_menuItem) {
        return startBuy();
    }


    public boolean startBuy() {
        try {
            if (BuyCartRemoteImpl.getRef().getItemList().isEmpty()) {
                MyDialogs.showError(contentFragment.getActivity(), "Fechar Compra", "Carrinho está vazio!");
                return true;
            }
        } catch (Exception e) {
            MyLogger.log(e);
        }


        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            public List<AddressInfo> addresses;
            public List<PaymentInfo> payments;
            public List<LocalConvenioInfo> convenios;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    this.addresses = OfertaDigitalRestClient.getRef().getAddress();
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                try {
                    this.payments = OfertaDigitalRestClient.getRef().getPayments();
                } catch (Exception e) {
                    MyLogger.log(e);
                }

                try {
                    this.convenios = OfertaDigitalRestClient.getRef().getConvenios();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                progressBar.setVisibility(View.GONE);
                if (this.addresses != null) {

                    {
                        AddressInfo addr = new AddressInfo();
                        addr.setId(1001);
                        addr.setAddress(new Address());
                        addr.getAddress().setXLgr("Retirar na loja");
                        addr.getAddress().setNro("");

                        this.addresses.add(addr);
                    }
                    {
                        AddressInfo addr = new AddressInfo();
                        addr.setId(1002);
                        addr.setAddress(new Address());
                        addr.getAddress().setXLgr("Entrega em domicilio");
                        addr.getAddress().setNro("");

                        this.addresses.add(addr);
                    }


                    final Dialog dialog = new Dialog(contentFragment.getActivity());
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                    dialog.setContentView(R.layout.delivery_address_layout);
                    dialog.setCancelable(false);

                    RadioGroup addrGroup = (RadioGroup) dialog.findViewById(R.id.addrGroup);

                    boolean first = true;
                    for (final AddressInfo addr : this.addresses) {
                        MyLogger.info("adicionando endereco: " + addr.getAddress().getXLgr());
                        RadioButton rb = new RadioButton(contentFragment.getActivity());
                        rb.setButtonDrawable(R.drawable.my_checkbox);
                        rb.setText(addr.getAddress().getXLgr() + " " + addr.getAddress().getNro());
                        rb.setTextColor(Color.BLACK);
                        rb.setId(MyOnMenuItemClickListener.generateViewId());

                        rb.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                BuyCartRemoteImpl.getRef().addAddress(addr);


                            }
                        });

                        if (first) {
                            // rb.setSelected(true);
                            first = false;
                        }
                        addrGroup.addView(rb);
                    }

                    if (addrGroup.getChildCount() > 0) {
                        addrGroup.check(addrGroup.getChildAt(0).getId());
                        BuyCartRemoteImpl.getRef().addAddress(this.addresses.get(0));
                    }

                    Button confirm = (Button) dialog.findViewById(R.id.btnConfirm);
                    confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            MyLogger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

                            AsyncTask<Void, Void, Void> taskkk = new AsyncTask<Void, Void, Void>() {

                                private boolean isok = true;

                                @Override
                                protected Void doInBackground(Void... params) {

                                    try {
                                        double distance = OfertaDigitalRestClient.getRef().calculateDistance(BuyCartRemoteImpl.getRef().getAddress());
                                        MyLogger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%:" + distance);
                                        if (distance > (Double.parseDouble(store.getDistance()) * 1000f)) {
                                            contentFragment.getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    MyDialogs.showError(contentFragment.getActivity(), "Endereço de Entrega", "A loja que está logado não faz entrega no endereço escolhido.");

                                                }
                                            });
                                            this.isok = false;
                                            return null;
                                        }
                                    } catch (Exception e) {
                                        MyLogger.log(e);
                                    }
                                    this.isok = true;
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    if (!this.isok) {
                                        return;
                                    }
                                    dialog.dismiss();

                                    MyLogger.info("onPostExecute buycart");

                                    final Dialog dialog = new Dialog(contentFragment.getActivity());
                                    //dialog.setTitle("Quantidade do Produto");
                                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                                    dialog.setContentView(R.layout.payment_type_layout);
                                    dialog.setCancelable(false);

                                    final RadioGroup rgr = (RadioGroup) dialog.findViewById(R.id.typePay);

                                    Button confirm = (Button) dialog.findViewById(R.id.btnConfirm);
                                    confirm.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();

                                            int radioButtonID = rgr.getCheckedRadioButtonId();
                                            View radioButton = rgr.findViewById(radioButtonID);
                                            int idx = rgr.indexOfChild(radioButton);

                                            RadioButton r = (RadioButton) rgr.getChildAt(idx);


                                            MyLogger.info("radio: " + r.getText() + " sel: " + r.isChecked());

                                            PaymentInfo payInfo = new PaymentInfo();

                                            if (r.getId() == R.id.radio_cartao && r.isChecked()) {
                                                payInfo.setTypePay(PaymentInfo.PAY_TYPE.CARD);

                                                showCreditCards(payments);
                                            }

                                            if (r.getId() == R.id.radio_cash && r.isChecked()) {
                                                payInfo.setTypePay(PaymentInfo.PAY_TYPE.CASH);
                                                BuyCartRemoteImpl.getRef().setPayInfo(payInfo);

                                                AddressInfo addr = BuyCartRemoteImpl.getRef().getAddress();
                                                if (addr.getAddress().getXLgr().equals("Retirar na loja")) {
                                                    showConfirmBuy();
                                                } else {
                                                    showChangeQtde();
                                                }


                                                //  showConfirmBuy();

                                            }

                                            if (r.getId() == R.id.radio_convenio && r.isChecked()) {
                                                payInfo.setTypePay(PaymentInfo.PAY_TYPE.CHECK);

                                                BuyCartRemoteImpl.getRef().setPayInfo(payInfo);

                                                showConvenios(convenios);
                                            }
                                        }
                                    });

                                    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                                    btnCancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                        }
                                    });

                                    dialog.show();
                                }
                            };
                            taskkk.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                        }
                    });

                    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return true;
    }

    private String hideCreditCardInfo(PaymentInfo payment) {

        String[] n_parts = payment.getCardNumber().split(" ");

        if (n_parts.length == 4) {
            return "**** **** **** " + n_parts[3] + " Venc: " + payment.getCardDd();
        } else {
            return "**** **** **** ****";
        }
    }

    public void showChangeQtde() {
        final Dialog dialog = new Dialog(contentFragment.getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.change_money_layout);
        dialog.setCancelable(false);

        final EditText textChangeQtde = (EditText) dialog.findViewById(R.id.textChangeQtde);

        textChangeQtde.addTextChangedListener(new TextWatcher() {

            private String current = "";
            private Locale ptBr = new Locale("pt", "BR");

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    textChangeQtde.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,.]", "").replaceAll("[R$,.]", "");

                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(ptBr).format((parsed / 100));

                    current = formatted;
                    textChangeQtde.setText(formatted);
                    textChangeQtde.setSelection(formatted.length());

                    textChangeQtde.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Button confirm = (Button) dialog.findViewById(R.id.btnConfirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String money_change_str = textChangeQtde.getText().toString();


                money_change_str = money_change_str.toString().replace("R$", "").replace("$", "").replace(".", "").replace(",", ".");

                MyLogger.info("money_change_str: " + money_change_str);

                BigDecimal money_change = new BigDecimal(money_change_str);

                BuyCartRemoteImpl.getRef().calculateTotal();

                BigDecimal cart_total = BuyCartRemoteImpl.getRef().getTotalBuy();

                MyLogger.info("money_change: " + money_change.toString() + " cart_total: " + cart_total.toString());

                if (money_change.compareTo(cart_total) == -1) {
                    MyDialogs.showError(contentFragment.getActivity(), "Finalizar Compra", "O troco solicitado é menor que valor da compra");
                    return;
                }

                dialog.dismiss();

                BuyCartRemoteImpl.getRef().getPayInfo().setC_a(money_change_str);
                showConfirmBuy();
            }
        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                showConfirmBuy();
            }
        });
        dialog.show();

    }


    public void showCreditCards(List<PaymentInfo> payments) {

        final Dialog dialog = new Dialog(contentFragment.getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.creditcard_layout);
        dialog.setCancelable(false);

        RadioGroup payGroup = (RadioGroup) dialog.findViewById(R.id.cardGroup);

        for (final PaymentInfo payment : payments) {

            RadioButton rb = new RadioButton(contentFragment.getActivity());
            rb.setButtonDrawable(R.drawable.my_checkbox);
            rb.setText(hideCreditCardInfo(payment));

            rb.setTextColor(Color.BLACK);
            rb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BuyCartRemoteImpl.getRef().setPayInfo(payment);
                }
            });

            payGroup.addView(rb);
        }

        if (payGroup.getChildCount() > 0) {
            payGroup.check(payGroup.getChildAt(0).getId());
            BuyCartRemoteImpl.getRef().setPayInfo(payments.get(0));
        } else {
            PaymentInfo payInfo = new PaymentInfo();
            payInfo.setTypePay(PaymentInfo.PAY_TYPE.CARD);
            BuyCartRemoteImpl.getRef().setPayInfo(payInfo);
        }

        Button confirm = (Button) dialog.findViewById(R.id.btnConfirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmBuy();
                dialog.dismiss();
            }
        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void showConfirmBuy() {

        MyDialogs.showConfirm(contentFragment.getContext(), "Finalizar Compra", "Deseja finalizar a compra?", new MyDialogs.Callback() {
            @Override
            public void processYes() {


                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                    private Exception e;

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            BuyCartRemoteImpl.getRef().createBuyCart();
                            BuyCartRemoteImpl.getRef().finishBuy(null);
                            BuyCartRemoteImpl.getRef().clearBuyCart();

                            Runtime.getRuntime().gc();
                        } catch (Exception e) {
                            MyLogger.log(e);
                            this.e = e;
                        }

                        MyLogger.info("finishing background");
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        if (this.e != null) {
                            MyDialogs.showError(contentFragment.getContext(), "Finalizar Compra", "Problemas efetuando o pedido:" + this.e.getLocalizedMessage());
                        } else {
                            MyDialogs.showInformationCenter(contentFragment.getContext(), "Finalizar Compra", "Pedido Efetuado com sucesso! \n OS PRODUTOS ANUNCIADOS EM PROMOÇÃO COM PAGAMENTO COM CARTÃO DA LOJA OU LEVE E PAGUE, OS DESCONTOS SERÃO APLICADOS AUTOMATICAMENTE APÓS A SEPARAÇÃO DO PEDIDO.");
                            try {
                                if (contentFragment instanceof ShowCategoriesHorizontalFragment) {
                                    ShowCategoriesHorizontalFragment fragment = (ShowCategoriesHorizontalFragment) contentFragment;
                                    fragment.updateTotal();
                                }
                            } catch (Exception e) {
                                MyLogger.log(e);
                            }
                        }
                    }
                };
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }

            @Override
            public void processNo() {

            }
        });
    }

    //////////////////////////////////////////////

    public void showConvenios(List<LocalConvenioInfo> convenios) {

        final Dialog dialog = new Dialog(contentFragment.getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.convenio_layout);
        dialog.setCancelable(false);

        RadioGroup payGroup = (RadioGroup) dialog.findViewById(R.id.cardGroup);

        for (final LocalConvenioInfo convenio : convenios) {

            RadioButton rb = new RadioButton(contentFragment.getActivity());
            rb.setButtonDrawable(R.drawable.my_checkbox);
            rb.setText(convenio.getEmpresa());

            rb.setTextColor(Color.BLACK);
            rb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BuyCartRemoteImpl.getRef().setConvenio(convenio);
                }
            });

            payGroup.addView(rb);
        }

        if (payGroup.getChildCount() > 0) {
            payGroup.check(payGroup.getChildAt(0).getId());
            BuyCartRemoteImpl.getRef().setConvenio(convenios.get(0));
        }

        Button confirm = (Button) dialog.findViewById(R.id.btnConfirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmBuy();
                dialog.dismiss();
            }
        });

        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }
}
