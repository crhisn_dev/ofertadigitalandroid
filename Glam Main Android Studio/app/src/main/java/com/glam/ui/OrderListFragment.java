package com.glam.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.glam.CheckoutActivity;
import com.glam.MainActivity;
import com.glam.R;
import com.glam.buycart.BuyCartRemoteImpl;
import com.glam.custom.CustomFragment;
import com.glam.dialog.ProductListDialog;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.BuyListInfo;
import com.glam.transport.BuyListOpRequestData;
import com.glam.transport.ProductListRequest;
import com.glam.utils.MyLogger;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import br.com.innove.ofertadigital.persistence.ItemId;
import br.com.innove.ofertadigital.persistence.ItemProductQuantityInfo;
import br.com.innove.ofertadigital.persistence.OrderInfo;
import br.com.innove.ofertadigital.persistence.OrderStatusInfo;
import br.com.innove.ofertadigital.persistence.OrderStatusNotif;
import br.com.innove.ofertadigital.persistence.ProductInfo;
import br.com.innove.ofertadigital.persistence.ProductInfoNoEntity;
import br.com.innove.ofertadigital.persistence.QuantityOrder;


public class OrderListFragment extends CustomFragment {


    private RecyclerView orderListView;

    private DecimalFormat n_br;
    private OrderStatusNotif orderStatus;
    private LinearLayout linlaHeaderProgress;

    public OrderListFragment() {
        this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
        this.n_br.applyPattern("###,###,##0.00");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.store_select_layout, null);

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).toolbar.setTitle("Pedidos Efetuados");
            ((MainActivity) getActivity()).toolbar.findViewById(
                    R.id.spinner_toolbar).setVisibility(View.GONE);
        } else {
            ((CheckoutActivity) getActivity()).getSupportActionBar().setTitle(
                    "Lista de Compras");
        }


        this.linlaHeaderProgress = (LinearLayout) v.findViewById(R.id.linlaHeaderProgress);

        final Intent intent = this.getActivity().getIntent();
        if (intent.hasExtra("order_status")) {
            this.orderStatus = (OrderStatusNotif) intent.getSerializableExtra("order_status");
            intent.removeExtra("order_status");
        } else {
            this.orderStatus = null;
        }


        // setTouchNClick(v.findViewById(R.id.btnDone));
        setHasOptionsMenu(true);


        initOrderListProducts(v);


        MyLogger.info("onCreateView: " + this);
        return v;
    }

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    private void initOrderListProducts(View v) {
        //  this.iList = new ArrayList<>();

        this.orderListView = (RecyclerView) v.findViewById(R.id.storeList);
        this.orderListView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.orderListView.setLayoutManager(llm);

        List<OrderInfo> items = new ArrayList<>();

        OrderListFragment.OrderListAdapter ca = new OrderListFragment.OrderListAdapter(items);
        this.orderListView.setAdapter(ca);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            public WeakReference<List<OrderInfo>> weakOrders;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    this.weakOrders = OfertaDigitalRestClient.getRef().findOrderList();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                List<OrderInfo> orders = this.weakOrders.get();
                orderListView.setAdapter(new OrderListFragment.OrderListAdapter(orders));

                if (orderStatus != null) {
                    MyLogger.info("atualizando pedido: " + orderStatus.getOrderId().getOrderId() + " status: " + orderStatus.getCurrentStatus());

                    try {
                        for (int i = 0; i < orders.size(); i++) {
                            OrderInfo order = orders.get(i);

                            if (order.getOrderId().equals(orderStatus.getOrderId())) {
                                ProductListDialog dialog = new ProductListDialog(order);
                                dialog.show(OrderListFragment.this.getActivity());


                                orderListView.scrollToPosition(i);
                                break;
                            }
                        }
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }
                }

                linlaHeaderProgress.setVisibility(View.GONE);
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private class OrderListAdapter extends
            RecyclerView.Adapter<OrderListFragment.OrderListAdapter.CardViewHolder> {

        private final DecimalFormat n_br;
        private List<OrderInfo> itemsProduct;
        private SimpleDateFormat sdf;

        public OrderListAdapter(List<OrderInfo> itemsProduct) {
            this.itemsProduct = itemsProduct;
            if (this.itemsProduct == null) {
                this.itemsProduct = new ArrayList<>();
            }
            this.n_br = (DecimalFormat) DecimalFormat.getInstance(new Locale("pt", "BR"));
            this.n_br.applyPattern("###,###,##0.00");
            this.sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        }

        @Override
        public int getItemCount() {
            return itemsProduct.size();
        }


        @Override
        public void onBindViewHolder(OrderListFragment.OrderListAdapter.CardViewHolder vh, final int i) {
            final OrderInfo item = itemsProduct.get(i);
            vh.lblLabel.setText(item.getOrderId().getOrderId().split("-")[0]);
            vh.lblDesc.setText("Qtde Produtos: " + item.getItemsProduct().size() + "\n" + "Data: " + sdf.format(item.getOrderDate()));
            vh.textViewTotalRS.setText("R$ " + n_br.format(item.getTotal().getValue1()));

            String status = "";

            switch (item.getCurrentStatus()) {
                case REQUESTED:
                    status = "SOLICITADO";
                    break;

                case FINISHED:
                    status = "FINALIZADO";
                    break;

                case PENDENT:
                    status = "PENDENTE";
                    break;

                case CURRENT:
                    status = "ATUAL";
                    break;

                default:
                    status = "";
            }


            vh.textViewStatus.setText(item.getCurrentStatus().getDesc());


            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProductListDialog dialog = new ProductListDialog(item);
                    dialog.show(OrderListFragment.this.getActivity());
                }
            });

        }

        /* (non-Javadoc)
         * @see android.support.v7.widget.RecyclerView.Adapter#onCreateViewHolder(android.view.ViewGroup, int)
         */
        @Override
        public OrderListFragment.OrderListAdapter.CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.order_list_item, viewGroup, false);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                }
            });

            return new OrderListFragment.OrderListAdapter.CardViewHolder(itemView);
        }


        public void clearAdapter() {
            this.itemsProduct = null;
        }

        public class CardViewHolder extends RecyclerView.ViewHolder {

            protected TextView lblLabel, lblDesc, textViewTotalRS, textViewStatus;

            public CardViewHolder(View v) {
                super(v);
                lblLabel = (TextView) v.findViewById(R.id.lblLabel);
                lblDesc = (TextView) v.findViewById(R.id.lblDesc);
                textViewTotalRS = (TextView) v.findViewById(R.id.textViewTotalRS);
                textViewStatus = (TextView) v.findViewById(R.id.textViewStatus);
            }
        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_exp, menu);
        menu.findItem(R.id.menu_refresh).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                this.refreshOrders();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public  void refreshOrders(){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            public WeakReference<List<OrderInfo>> weakOrders;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    this.weakOrders = OfertaDigitalRestClient.getRef().findOrderList();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                List<OrderInfo> orders = this.weakOrders.get();
                orderListView.setAdapter(new OrderListFragment.OrderListAdapter(orders));

            /*    if (orderStatus != null) {
                    MyLogger.info("atualizando pedido: " + orderStatus.getOrderId().getOrderId() + " status: " + orderStatus.getCurrentStatus());

                    try {
                        for (int i = 0; i < orders.size(); i++) {
                            OrderInfo order = orders.get(i);

                            if (order.getOrderId().equals(orderStatus.getOrderId())) {
                                ProductListDialog dialog = new ProductListDialog(order);
                                dialog.show(OrderListFragment.this.getActivity());


                                orderListView.scrollToPosition(i);
                                break;
                            }
                        }
                    } catch (Exception e) {
                        MyLogger.log(e);
                    }
                }*/

                linlaHeaderProgress.setVisibility(View.GONE);
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
