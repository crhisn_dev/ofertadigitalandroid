package com.glam.ui;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.glam.CheckoutActivity;
import com.glam.MainActivity;
import com.glam.R;
import com.glam.custom.CustomFragment;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.utils.ImageCache;
import com.glam.utils.MyLogger;

import java.util.ArrayList;
import java.util.List;

import br.com.innove.ofertadigital.persistence.DeliveryInfo;
import br.com.innove.ofertadigital.persistence.MediaDesc;
import br.com.innove.ofertadigital.persistence.RecipeInfo;
import br.com.innove.ofertadigital.persistence.StoreInfo;


public class RecipentInfoFragment extends CustomFragment {
    private OfertaDigitalDataSource dao;
    private ImageCache cache;

    private RecipeInfo recipe;
    private MediaDesc media;
    private StoreInfo storeInfo;

    private List<Bitmap> bitmaps;
    private View mainView;
    private LinearLayout linlaHeaderProgress;

    @SuppressLint({"InflateParams", "InlinedApi"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle args = getArguments();
        if (args != null && args.containsKey("storeinfo")) {
            this.storeInfo = (StoreInfo) args.getSerializable("storeinfo");
        }

        this.dao = new OfertaDigitalDataSource(this.getActivity());
        this.cache = new ImageCache(this.dao);
        this.bitmaps = new ArrayList<>();


        this.mainView = inflater.inflate(R.layout.recipe_detail, null);

        this.linlaHeaderProgress = (LinearLayout) this.mainView.findViewById(R.id.linlaHeaderProgress);


        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).toolbar.setTitle("Dicas de Saúde");
            ((MainActivity) getActivity()).toolbar.findViewById(
                    R.id.spinner_toolbar).setVisibility(View.GONE);
        } else {
            ((CheckoutActivity) getActivity()).getSupportActionBar().setTitle(
                    "Dicas de Saúde");
        }


        final ImageView imageView_recipe = (ImageView) this.mainView.findViewById(R.id.imageView_recipe);
        final TextView lbl_name_recipe = (TextView) this.mainView.findViewById(R.id.lbl_name_recipe);
        final TextView lbl_desc_recipe = (TextView) this.mainView.findViewById(R.id.lbl_desc_recipe);


        final String store_id = this.storeInfo.getId();

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            public RecipeInfo recipe1;
            public MediaDesc mediaDesc;

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    List<RecipeInfo> dds = OfertaDigitalRestClient.getRef().findRecipe(store_id);
                    if (dds != null && !dds.isEmpty()) {
                        this.recipe1 = dds.get(0);

                        List<MediaDesc> medias = new ArrayList<>();
                        if (!this.recipe1.getMediasList().isEmpty()) {
                            mediaDesc = this.recipe1.getMediasList().get(0);
                            medias.add(mediaDesc);
                        }
                        cache.requestImages(medias);
                    }

                } catch (Exception e) {
                    MyLogger.log(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (mediaDesc != null) {
                    byte[] image = cache.getImage(mediaDesc.getNativeimage_id());

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.RGB_565;

                    Bitmap imagebitmap = BitmapFactory.decodeByteArray(image, 0, image.length, options);

                    double scale = 0.9d;
                    Bitmap imagebitmapscaled = Bitmap.createScaledBitmap(imagebitmap, (int) (imagebitmap.getWidth() * scale), (int) (imagebitmap.getHeight() * scale), true);
                    imageView_recipe.setImageBitmap(imagebitmapscaled);

                    imagebitmap.recycle();

                    bitmaps.add(imagebitmapscaled);
                }

                if (this.recipe1 != null) {
                    lbl_name_recipe.setText(this.recipe1.getNameRecipe());
                    String textDesc = "";
                    if (!this.recipe1.getDescriptions().isEmpty()) {
                        textDesc = this.recipe1.getDescriptions().get(0);
                    }

                    lbl_desc_recipe.setText(textDesc);
                }

                try {
                    Runtime.getRuntime().gc();
                } catch (Exception e) {
                    MyLogger.log(e);
                }
                linlaHeaderProgress.setVisibility(View.GONE);
            }

            @Override
            protected void onPreExecute() {
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        return this.mainView;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.cart, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    private void unbindDrawables(View view) throws Exception {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    @Override
    public void onDestroyView() {
        MyLogger.info("onDestroyView: " + this);
        super.onDestroyView();

        try {
            this.unbindDrawables(this.mainView);
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            this.dao.close();
            this.dao = null;
            this.cache = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            for (Bitmap b : bitmaps) {
                MyLogger.info("calling recycle bitmap");
                b.recycle();
                //  b = null;
            }
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            bitmaps.clear();
            bitmaps = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }

        try {
            Runtime.getRuntime().gc();
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }
}
