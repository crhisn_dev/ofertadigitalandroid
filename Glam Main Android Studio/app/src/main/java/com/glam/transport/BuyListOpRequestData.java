package com.glam.transport;


import com.glam.persistence.BuyListId;

import java.io.Serializable;
import java.util.Date;

import br.com.innove.ofertadigital.persistence.ItemId;
import br.com.innove.ofertadigital.persistence.ProductId;
import br.com.innove.ofertadigital.persistence.QuantityOrder;

public class BuyListOpRequestData implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum BUYLIST_OP {
		SAVE, DELETE, CREATE
	}

	private BuyListId buyListId;

	private String name;
	private String description;

	// individual info
	private ItemId itemId;
	private ProductId productId;
	private QuantityOrder quantity;
	private BUYLIST_OP op;

	// list info
	private ProductListRequest prodList;

	// data
	private long timestamp;
	private Date opDate;

	public BuyListId getBuyListId() {
		return buyListId;
	}

	public void setBuyListId(BuyListId buyListId) {
		this.buyListId = buyListId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ItemId getItemId() {
		return itemId;
	}

	public void setItemId(ItemId itemId) {
		this.itemId = itemId;
	}

	public ProductId getProductId() {
		return productId;
	}

	public void setProductId(ProductId productId) {
		this.productId = productId;
	}

	public QuantityOrder getQuantity() {
		return quantity;
	}

	public void setQuantity(QuantityOrder quantity) {
		this.quantity = quantity;
	}

	public BUYLIST_OP getOp() {
		return op;
	}

	public void setOp(BUYLIST_OP op) {
		this.op = op;
	}

	public ProductListRequest getProdList() {
		return prodList;
	}

	public void setProdList(ProductListRequest prodList) {
		this.prodList = prodList;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public Date getOpDate() {
		return opDate;
	}

	public void setOpDate(Date opDate) {
		this.opDate = opDate;
	}

}
