package com.glam.transport;

import java.io.Serializable;
import java.util.Hashtable;

import br.com.innove.ofertadigital.persistence.NativeImage;

public class ImagesArrayResponseData implements Serializable {

	private static final long serialVersionUID = 1L;

	private Hashtable<String, NativeImage> images;

	public ImagesArrayResponseData() {
		this.images = new Hashtable<>();
	}

	public Hashtable<String, NativeImage> getImages() {
		return images;
	}

	public void setImages(Hashtable<String, NativeImage> images) {
		this.images = images;
	}

}
