package com.glam.transport;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Temporal;

import br.com.innove.ofertadigital.persistence.MediaDesc;

@Entity
public class OfferStoreInfo implements Serializable {

	public static enum PROMO_STATUS {
		SENT, PENDENT
	}

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private OfferId promo_id;

	private String namePromo;

	@OneToMany(fetch = FetchType.EAGER)
	private List<MediaDesc> mediasList;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date validateDate;

	public OfferId getPromo_id() {
		return promo_id;
	}

	public void setPromo_id(OfferId promo_id) {
		this.promo_id = promo_id;
	}

	public List<MediaDesc> getMediasList() {
		return mediasList;
	}

	public void setMediasList(List<MediaDesc> mediasList) {
		this.mediasList = mediasList;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getValidateDate() {
		return validateDate;
	}

	public void setValidateDate(Date validateDate) {
		this.validateDate = validateDate;
	}

	public String getNamePromo() {
		return namePromo;
	}

	public void setNamePromo(String namePromo) {
		this.namePromo = namePromo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((promo_id == null) ? 0 : promo_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfferStoreInfo other = (OfferStoreInfo) obj;
		if (promo_id == null) {
			if (other.promo_id != null)
				return false;
		} else if (!promo_id.equals(other.promo_id))
			return false;
		return true;
	}

}
