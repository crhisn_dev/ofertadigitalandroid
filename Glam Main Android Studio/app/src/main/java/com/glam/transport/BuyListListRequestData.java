package com.glam.transport;

import com.glam.persistence.BuyListInfo.BUYLIST_STATUS;

import java.io.Serializable;
import java.util.Date;

public class BuyListListRequestData implements Serializable {

	private BUYLIST_STATUS status;
	private Date date;

	public BUYLIST_STATUS getStatus() {
		return status;
	}

	public void setStatus(BUYLIST_STATUS status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
