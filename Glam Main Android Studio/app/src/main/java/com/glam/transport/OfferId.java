package com.glam.transport;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class OfferId implements Serializable {
	private String promo_id;
	private String store_id;

	public String getPromo_id() {
		return promo_id;
	}

	public void setPromo_id(String promo_id) {
		this.promo_id = promo_id;
	}

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((promo_id == null) ? 0 : promo_id.hashCode());
		result = prime * result + ((store_id == null) ? 0 : store_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfferId other = (OfferId) obj;
		if (promo_id == null) {
			if (other.promo_id != null)
				return false;
		} else if (!promo_id.equals(other.promo_id))
			return false;
		if (store_id == null) {
			if (other.store_id != null)
				return false;
		} else if (!store_id.equals(other.store_id))
			return false;
		return true;
	}

}
