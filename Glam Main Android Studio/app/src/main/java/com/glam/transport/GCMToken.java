package com.glam.transport;

import java.io.Serializable;
import java.util.Date;

public class GCMToken implements Serializable {

		private static final long serialVersionUID = 1L;

		private String registerId;
		private Date regDate;
		private String login;

		public String getRegisterId() {
			return registerId;
		}

		public void setRegisterId(String registerId) {
			this.registerId = registerId;
		}

		public Date getRegDate() {
			return regDate;
		}

		public void setRegDate(Date regDate) {
			this.regDate = regDate;
		}

		public String getLogin() {
			return login;
		}

		public void setLogin(String login) {
			this.login = login;
		}


}
