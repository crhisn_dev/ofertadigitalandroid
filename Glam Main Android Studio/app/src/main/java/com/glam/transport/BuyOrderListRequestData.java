package com.glam.transport;

import java.io.Serializable;
import java.util.Date;

import br.com.innove.ofertadigital.persistence.BuyListInfo.BUYLIST_STATUS;
import br.com.innove.ofertadigital.persistence.OrderStatusInfo.ORDER_STATUS;

public class BuyOrderListRequestData implements Serializable {

	private ORDER_STATUS status;
	private Date date;

	public ORDER_STATUS getStatus() {
		return status;
	}

	public void setStatus(ORDER_STATUS status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
