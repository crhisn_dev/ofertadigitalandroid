package com.glam.transport;

import java.io.Serializable;
import java.util.Date;

public class JornalDeliveryListRequestData implements Serializable {

	private String store_id;
	private Date dataQuery;

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	public Date getDataQuery() {
		return dataQuery;
	}

	public void setDataQuery(Date dataQuery) {
		this.dataQuery = dataQuery;
	}

}
