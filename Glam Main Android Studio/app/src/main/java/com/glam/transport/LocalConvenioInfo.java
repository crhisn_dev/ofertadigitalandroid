package com.glam.transport;


import java.io.Serializable;
import java.util.Date;

import br.com.innove.ofertadigital.persistence.UserId;

public class LocalConvenioInfo implements Serializable {

    public static enum CONV_STATUS {
        ALL("Todos"), NEW("Novo"), AUTORIZED("Autorizado"), DENIED("Denegado"), DISABLE("Desabilitado	");

        private String desc;

        private CONV_STATUS(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    private String id;

    private UserId userId;

    private Date createdAt;

    private String registro;

    private String empresa;

    private String other1;

    private String other2;

    private CONV_STATUS estado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getOther1() {
        return other1;
    }

    public void setOther1(String other1) {
        this.other1 = other1;
    }

    public String getOther2() {
        return other2;
    }

    public void setOther2(String other2) {
        this.other2 = other2;
    }

    public CONV_STATUS getEstado() {
        return estado;
    }

    public void setEstado(CONV_STATUS estado) {
        this.estado = estado;
    }


}
