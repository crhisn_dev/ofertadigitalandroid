package com.glam.transport;

import java.io.Serializable;
import java.util.Hashtable;

public class Credentials implements Serializable {

	private static final long serialVersionUID = 1L;

	private String username;
	private String role;
	private String password;
	private String store_id;
	private String fbToken;
	private Hashtable<String, String> fbInfo;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	public String getFbToken() {
		return fbToken;
	}

	public void setFbToken(String fbToken) {
		this.fbToken = fbToken;
	}

	public Hashtable<String, String> getFbInfo() {
		return fbInfo;
	}

	public void setFbInfo(Hashtable<String, String> fbInfo) {
		this.fbInfo = fbInfo;
	}
	
	

}
