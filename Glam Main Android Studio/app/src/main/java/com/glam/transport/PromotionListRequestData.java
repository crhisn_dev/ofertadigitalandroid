package com.glam.transport;

import java.io.Serializable;
import java.util.Date;

import br.com.innove.ofertadigital.persistence.BuyListInfo.BUYLIST_STATUS;
import br.com.innove.ofertadigital.persistence.OrderStatusInfo.ORDER_STATUS;

public class PromotionListRequestData implements Serializable {

	private Date date;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
