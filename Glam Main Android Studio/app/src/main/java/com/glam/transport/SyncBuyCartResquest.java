package com.glam.transport;

import java.io.Serializable;
import java.util.List;

import br.com.innove.ofertadigital.persistence.ItemId;
import br.com.innove.ofertadigital.persistence.ProductId;
import br.com.innove.ofertadigital.persistence.QuantityOrder;

public class SyncBuyCartResquest implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SyncItem> syncItens;

	public List<SyncItem> getSyncItens() {
		return syncItens;
	}

	public void setSyncItens(List<SyncItem> syncItens) {
		this.syncItens = syncItens;
	}

	public static class SyncItem implements Serializable {
		private ItemId itemId;
		private ProductId productId;
		private QuantityOrder quantity;

		public ItemId getItemId() {
			return itemId;
		}

		public void setItemId(ItemId itemId) {
			this.itemId = itemId;
		}

		public ProductId getProductId() {
			return productId;
		}

		public void setProductId(ProductId productId) {
			this.productId = productId;
		}

		public QuantityOrder getQuantity() {
			return quantity;
		}

		public void setQuantity(QuantityOrder quantity) {
			this.quantity = quantity;
		}

	}
}
