package com.glam.transport;

import java.io.Serializable;
import java.util.List;

public class ImagesRequestData implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<String> nativeImagesIds;

	public List<String> getNativeImagesIds() {
		return nativeImagesIds;
	}

	public void setNativeImagesIds(List<String> nativeImagesIds) {
		this.nativeImagesIds = nativeImagesIds;
	}
	
	
}
