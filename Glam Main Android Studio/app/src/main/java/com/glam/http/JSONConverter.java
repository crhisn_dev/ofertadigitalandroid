package com.glam.http;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class JSONConverter {
    public String toJSON(Object object) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(object);

        return json;
    }

    public String toJSON(Object object, Class<?> classs) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writerWithType(classs).withDefaultPrettyPrinter().writeValueAsString(object);
        return s;
    }

    public <T> String toJSON(Object object, TypeReference<T> ref) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writerWithType(ref).withDefaultPrettyPrinter().writeValueAsString(object);

        return s;
    }

    public <T> T toObject(String json, TypeReference<T> ref) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Object tt = mapper.readValue(json, ref);
        return (T) tt;
    }

	/*
	 * public Object toObject(String json) throws Exception { ObjectMapper
	 * mapper = new ObjectMapper(); Object tt = mapper.readValue( return tt; }
	 */

    public JsonNode extractNode(String json, String nodeName) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(json);

        return node.get(nodeName);
    }

    public <T> T toObject(String json, Class<T> classs) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Object tt = mapper.readValue(json, classs);
        return (T) tt;
    }

}
