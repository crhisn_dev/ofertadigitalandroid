package com.glam.http;

import com.fasterxml.jackson.core.type.TypeReference;
import com.glam.persistence.BuyListId;
import com.glam.persistence.BuyListInfo;
import com.glam.transport.BuyListListRequestData;
import com.glam.transport.BuyListOpRequestData;
import com.glam.transport.BuyOrderListRequestData;
import com.glam.transport.CEPInfo;
import com.glam.transport.Credentials;
import com.glam.transport.GCMToken;
import com.glam.transport.ImagesArrayResponseData;
import com.glam.transport.ImagesRequestData;
import com.glam.transport.JornalDeliveryListRequestData;
import com.glam.transport.LocalConvenioInfo;
import com.glam.transport.ProductListRequest;
import com.glam.transport.PromotionListRequestData;
import com.glam.transport.SyncBuyCartResquest;
import com.glam.utils.MyLogger;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import br.com.innove.ofertadigital.persistence.Address;
import br.com.innove.ofertadigital.persistence.AddressInfo;
import br.com.innove.ofertadigital.persistence.CategoryInfo;
import br.com.innove.ofertadigital.persistence.DeliveryInfo;
import br.com.innove.ofertadigital.persistence.JornalStoreInfo;
import br.com.innove.ofertadigital.persistence.OrderInfo;
import br.com.innove.ofertadigital.persistence.OrderStatusInfo;
import br.com.innove.ofertadigital.persistence.PaymentInfo;
import br.com.innove.ofertadigital.persistence.ProductId;
import br.com.innove.ofertadigital.persistence.PromotionInfo;
import br.com.innove.ofertadigital.persistence.QuantityOrder;
import br.com.innove.ofertadigital.persistence.RecipeInfo;
import br.com.innove.ofertadigital.persistence.StoreInfo;
import br.com.innove.ofertadigital.persistence.UserId;
import br.com.innove.ofertadigital.persistence.UserInfo;
import br.com.innove.ofertadigital.persistence.VideoPromoInfo;
import br.com.innove.ofertadigital.rest.transport.BuyCartOpRequestData;
import br.com.innove.ofertadigital.rest.transport.BuyCartResponseData;

import br.com.innove.ofertadigital.rest.transport.LoginResponseData;
import br.com.innove.ofertadigital.rest.transport.PaginationData;
import br.com.innove.ofertadigital.rest.transport.QueryProductData;
import br.com.innove.ofertadigital.rest.transport.ResultQueryData;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.EntityUtils;


public class OfertaDigitalRestClient {

    // public static String HOST_ENDPOINT = "http://ofertadigital.ddns.net/ofertadigitalrest";

    // public static String HOST_ENDPOINT = "http://ofertadigital.hopto.org/ofertadigitalrest";
    // public static String HOST_ENDPOINT = "http://contifarma.hopto.org/ofertadigitalrest";

    public static String HOST_ENDPOINT = "http://danifarma.hopto.org/ofertadigitalrest";
    // public static String HOST_ENDPOINT = "http://201.73.145.11/ofertadigitalrest";

    private static OfertaDigitalRestClient instance;


    public static OfertaDigitalRestClient getRef() {
        if (instance == null) {
            instance = new OfertaDigitalRestClient();
        }
        return instance;
    }


    public static OfertaDigitalRestClient createNewRef2() {
        OfertaDigitalRestClient restClient = new OfertaDigitalRestClient();
        restClient.setTokenAutenthication(getRef().getTokenAutenthication());
        return restClient;
    }


    public static void recreate() {
        instance = null;
        getRef();
    }

    //  private org.apache.http.client.HttpClient client;

    private JSONConverter conv;
    private String tokenAutenthication;
    private UserId userId;
    private String userName;
    private Date lastLogin;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setTokenAutenthication(String tokenAutenthication) {
        this.tokenAutenthication = tokenAutenthication;


    }

    public String getTokenAutenthication() {
        return tokenAutenthication;
    }

    private OfertaDigitalRestClient() {
        this.conv = new JSONConverter();
        this.userName = "";
        //  this.client = HttpClientBuilder.create().build();


        //  HttpParams httpParameters = new BasicHttpParams();
        // int timeoutConnection = 3000;
        // HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        // int timeoutSocket = 5000;
        // HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
    }

    public UserId getUserId() {
        return userId;
    }

    public LoginResponseData autenthicateWithFB(String username, String store_id, String fbToken, Hashtable<String, String> fbInfo) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/fb_autenthication");


        Credentials credentials = new Credentials();
        credentials.setUsername(username);
        credentials.setStore_id(store_id);
        credentials.setFbInfo(fbInfo);
        credentials.setFbToken(fbToken);

        this.userId = new UserId();
        this.userId.setUsername(username);
        this.userId.setStore_id(store_id);

        String jsonRequest = this.conv.toJSON(credentials);
        StringEntity entity = new StringEntity(jsonRequest);

        entity.setContentType("application/json");
        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        MyLogger.info("status code: " + response.getStatusLine().getStatusCode());

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
            MyLogger.info("login response:" + jsonResponse);

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }

            LoginResponseData loginResponse = conv.toObject(jsonResponse, LoginResponseData.class);
            this.tokenAutenthication = loginResponse.getToken();
            this.lastLogin = loginResponse.getLastLogin();

            String message = loginResponse.getMessage();
            String[] message_split = message.split("\\|");

            if (message_split.length == 2) {
                this.userName = message_split[1];
            }

            EntityUtils.consume(response.getEntity());
            return loginResponse;

        } else {
            this.checkAutorization(response);

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }

            return null;
        }

    }

    public LoginResponseData autenthicate(String username, String password, String store_id, String role) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/autenthication");


        Credentials credentials = new Credentials();
        credentials.setUsername(username);
        credentials.setStore_id(store_id);
        credentials.setPassword(password);

        this.userId = new UserId();
        this.userId.setUsername(username);
        this.userId.setStore_id(store_id);

        String jsonRequest = this.conv.toJSON(credentials);
        StringEntity entity = new StringEntity(jsonRequest);

        entity.setContentType("application/json");
        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        MyLogger.info("status code: " + response.getStatusLine().getStatusCode());

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
            MyLogger.info("login response:" + jsonResponse);

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }

            LoginResponseData loginResponse = conv.toObject(jsonResponse, LoginResponseData.class);
            this.tokenAutenthication = loginResponse.getToken();
            this.lastLogin = loginResponse.getLastLogin();

            EntityUtils.consume(response.getEntity());
            return loginResponse;

        } else {
            this.checkAutorization(response);

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }

            return null;
        }


    }

    private void checkAutorization(HttpResponse response) throws Exception {
        if (response.getStatusLine().getStatusCode() == 401) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
            throw new Exception("erro na autenticacao: " + jsonResponse);
        }
    }

    public WeakReference<ResultQueryData> findProducts(String store_id, String productName, List<String> categories, Double lowPrice,
                                                       Double upperPrice, int startWith, int maxSize, boolean calculateTotal) throws Exception {

        MyLogger.info("making Rest query: " + store_id + " productName: " + productName + " categories: " + categories);

        MyGet get = new MyGet(HOST_ENDPOINT + "/products/search");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        QueryProductData query = new QueryProductData();
        query.setStore_id(store_id);
        query.setProductName(productName);
        query.setCategories(categories);
        query.setLowPrice(lowPrice);
        query.setUpperPrice(upperPrice);

        PaginationData pagination = new PaginationData();
        pagination.setMaxSize(maxSize);
        pagination.setStartWith(startWith);
        pagination.setCalculateTotal(calculateTotal);
        query.setPagination(pagination);

        JSONConverter convv = new JSONConverter();
        String jsonQuery = convv.toJSON(query);

        StringEntity entity = new StringEntity(jsonQuery);
        entity.setContentType("application/json");
        get.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);


        this.checkAutorization(response);


        String jsonResponse = EntityUtils.toString(response.getEntity(), "ISO-8859-1");

        // MyLogger.info("before:" + jsonResponse);

        jsonResponse = new String(jsonResponse.getBytes("ISO-8859-1"), "UTF-8");

        // MyLogger.info("after:" + jsonResponse);

        ResultQueryData result = convv.toObject(jsonResponse, ResultQueryData.class);

        try {
            response.getEntity().consumeContent();
        } catch (Exception e) {
            MyLogger.log(e);
        }

        return new WeakReference<ResultQueryData>(result);
    }


    public void createNewBuyCart() throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/processbuycartop");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        BuyCartOpRequestData request = new BuyCartOpRequestData();
        request.setOp(BuyCartOpRequestData.BUYCART_OP.CREATE_BUYCART);
        request.setOpDate(Calendar.getInstance().getTime());

        String jsonRequest = this.conv.toJSON(request);
        StringEntity entity = new StringEntity(jsonRequest);
        entity.setContentType("application/json");
        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    public void addProductToBuyCart(ProductId productId, QuantityOrder quantity) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/processbuycartop");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        BuyCartOpRequestData request = new BuyCartOpRequestData();

        request.setProductId(productId);
        request.setQuantity(quantity);
        request.setOp(BuyCartOpRequestData.BUYCART_OP.ADD);

        String jsonRequest = this.conv.toJSON(request);
        StringEntity entity = new StringEntity(jsonRequest);
        entity.setContentType("application/json");
        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }

            BuyCartResponseData responseData = this.conv.toObject(jsonResponse, BuyCartResponseData.class);

        } else {

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }
        }
    }

    public void syncBuyCart(List<ProductListRequest.SyncItem> syncItens) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/syncbuycart");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        ProductListRequest request = new ProductListRequest();
        request.setSyncItens(syncItens);

        JSONConverter convv = new JSONConverter();
        String jsonRequest = convv.toJSON(request);

        StringEntity entity = new StringEntity(jsonRequest, "UTF-8");
        entity.setContentType("application/json");
        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
            MyLogger.info("syncBuyCart json response: " + jsonResponse);

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }

            BuyCartResponseData responseData = convv.toObject(jsonResponse, BuyCartResponseData.class);

        } else {
            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }
        }
        MyLogger.info("return syncBuyCart");
    }

    public void finalizeBuy(AddressInfo addrInfo, PaymentInfo payInfo) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/processbuycartop");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        BuyCartOpRequestData request = new BuyCartOpRequestData();
        request.setAddrInfo(addrInfo);
        request.setPayInfo(payInfo);
        request.setOp(BuyCartOpRequestData.BUYCART_OP.FINALIZE);
        request.setOpDate(Calendar.getInstance().getTime());


        JSONConverter convv = new JSONConverter();
        String jsonRequest = convv.toJSON(request);
        StringEntity entity = new StringEntity(jsonRequest, "UTF-8");
        entity.setContentType("application/json");
        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
            MyLogger.info("finalizeBuy json response: " + jsonResponse);

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }

            // BuyCartResponseData responseData = convv.toObject(jsonResponse, BuyCartResponseData.class);

        } else {
            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }
        }

        MyLogger.info("return finalizeBuy");
    }

    private List<CategoryInfo> categories;

    private String[] cat_names = new String[]{"infeccoes", "dores", "colesterol", "alimentos", "diagnosticos", "maquiagem", "cabelos", "pes_maos", "dermo", "higiene", "infantil"};

    public List<CategoryInfo> findCategories() throws Exception {
        if (this.categories == null || this.categories.isEmpty()) {
            this.categories = new ArrayList<>();
            for (String cat : cat_names) {

                CategoryInfo category = new CategoryInfo();
                category.setName(cat.toUpperCase());

                this.categories.add(category);
            }
        }

        return this.categories;
    }

    public List<CategoryInfo> findCategoriesServer() throws Exception {
        if (this.categories == null || this.categories.isEmpty()) {
            MyGet get = new MyGet(HOST_ENDPOINT + "/products/categories");
            get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

            HttpResponse response = HttpClientBuilder.create().build().execute(get);


            this.checkAutorization(response);

            String jsonResponse = EntityUtils.toString(response.getEntity(), "iso-8859-1");

            jsonResponse = jsonResponse.replace("Ã£", "ã");

            MyLogger.info("jsonResponse: " + jsonResponse);

            JSONConverter convv = new JSONConverter();
            this.categories = convv.toObject(jsonResponse, new TypeReference<List<CategoryInfo>>() {
            });

            try {
                EntityUtils.consume(response.getEntity());
            } catch (Exception e) {
                MyLogger.log(e);
            }
        }

        return this.categories;
    }

    public List<StoreInfo> findStores() throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/findstores");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        // alterei
        String jsonResponse = EntityUtils.toString(response.getEntity(), "ISO-8859-1");

        jsonResponse = new String(jsonResponse.getBytes("ISO-8859-1"), "UTF-8");

        MyLogger.info("stores: " + jsonResponse);

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }

        return this.conv.toObject(jsonResponse, new TypeReference<List<StoreInfo>>() {
        });

    }

    public List<JornalStoreInfo> findJornals(String store_id) throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/findjornals");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        JornalDeliveryListRequestData req = new JornalDeliveryListRequestData();
        req.setStore_id(store_id);

        StringEntity entity = new StringEntity(this.conv.toJSON(req));
        entity.setContentType("application/json");

        get.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);


        String jsonResponse = EntityUtils.toString(response.getEntity(), "ISO-8859-1");

        jsonResponse = new String(jsonResponse.getBytes("ISO-8859-1"), "UTF-8");

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }

        return this.conv.toObject(jsonResponse, new TypeReference<List<JornalStoreInfo>>() {
        });
    }

    public List<DeliveryInfo> findDeliver(String store_id) throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/finddelivery");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        JornalDeliveryListRequestData req = new JornalDeliveryListRequestData();
        req.setStore_id(store_id);

        StringEntity entity = new StringEntity(this.conv.toJSON(req));
        entity.setContentType("application/json");

        get.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }

        return this.conv.toObject(jsonResponse, new TypeReference<List<DeliveryInfo>>() {
        });

    }


    public List<RecipeInfo> findRecipe(String store_id) throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/findrecipe");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        JornalDeliveryListRequestData req = new JornalDeliveryListRequestData();
        req.setStore_id(store_id);

        StringEntity entity = new StringEntity(this.conv.toJSON(req));
        entity.setContentType("application/json");

        get.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity(), "utf-8");

        System.out.println(jsonResponse);

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }

        return this.conv.toObject(jsonResponse, new TypeReference<List<RecipeInfo>>() {
        });

    }

    public List<VideoPromoInfo> findVideos(String store_id) throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/findvideos");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        JornalDeliveryListRequestData req = new JornalDeliveryListRequestData();
        req.setStore_id(store_id);

        StringEntity entity = new StringEntity(this.conv.toJSON(req));
        entity.setContentType("application/json");

        get.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        return this.conv.toObject(jsonResponse, new TypeReference<List<VideoPromoInfo>>() {
        });

    }

    public ImagesArrayResponseData requestImages(List<String> nativesIds) throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/products/requestimages");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        ImagesRequestData imageReq = new ImagesRequestData();
        imageReq.setNativeImagesIds(nativesIds);

        JSONConverter convv = new JSONConverter();
        String jsonRequest = convv.toJSON(imageReq);

        StringEntity entity = new StringEntity(jsonRequest);
        entity.setContentType("application/json");

        get.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }

        ImagesArrayResponseData imageResponse = convv.toObject(jsonResponse, ImagesArrayResponseData.class);
        return imageResponse;
    }

    public void processBuyListOp(BuyListId buyListId, String name, String description, ProductListRequest prodList,
                                 BuyListOpRequestData.BUYLIST_OP op) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/processbuylistop");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        BuyListOpRequestData request = new BuyListOpRequestData();
        request.setBuyListId(buyListId);
        request.setName(name);
        request.setDescription(description);
        request.setProdList(prodList);


        request.setOp(op);

        StringEntity entity = new StringEntity(this.conv.toJSON(request));
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        System.out.println(EntityUtils.toString(response.getEntity()));

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    public WeakReference<List<BuyListInfo>> findBuyList() throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/findbuylist");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        BuyListListRequestData req = new BuyListListRequestData();
        req.setStatus(BuyListInfo.BUYLIST_STATUS.OK);

        JSONConverter convv = new JSONConverter();
        StringEntity entity = new StringEntity(convv.toJSON(req));
        entity.setContentType("application/json");

        get.setEntity(entity);
        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        InputStream mResponseStream = new ByteArrayInputStream(jsonResponse.getBytes(HTTP.UTF_8));
        BufferedReader br = new BufferedReader(new InputStreamReader(mResponseStream));
        String newJson = "";
        String line = "";
        while ((line = br.readLine()) != null) {
            line = line.replaceAll("[\r\n]", "");
            newJson += line;
        }

        MyLogger.info("newJson: " + newJson);

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }


        List<BuyListInfo> buyList = convv.toObject(newJson, new TypeReference<List<BuyListInfo>>() {
        });

        return new WeakReference<List<BuyListInfo>>(buyList);
    }


    public WeakReference<List<PromotionInfo>> findPromotionList() throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/findpromotions");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        PromotionListRequestData req = new PromotionListRequestData();
        req.setDate(new Date(Calendar.getInstance().getTimeInMillis() - (3 * 24 * 60 * 60 * 1000l)));

        JSONConverter convv = new JSONConverter();
        StringEntity entity = new StringEntity(convv.toJSON(req));
        entity.setContentType("application/json");

        get.setEntity(entity);
        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        // String jsonResponse = EntityUtils.toString(response.getEntity());

        // alterei
        String jsonResponse = EntityUtils.toString(response.getEntity(), "ISO-8859-1");
        jsonResponse = new String(jsonResponse.getBytes("ISO-8859-1"), "UTF-8");

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }

        List<PromotionInfo> promotions = convv.toObject(jsonResponse, new TypeReference<List<PromotionInfo>>() {
        });

        return new WeakReference<List<PromotionInfo>>(promotions);

    }

    public WeakReference<List<OrderInfo>> findOrderList() throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/findorderlist");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        BuyOrderListRequestData req = new BuyOrderListRequestData();
        // req.setStatus(OrderStatusInfo.ORDER_STATUS.REQUESTED);

        JSONConverter convv = new JSONConverter();
        StringEntity entity = new StringEntity(convv.toJSON(req));
        entity.setContentType("application/json");

        get.setEntity(entity);
        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }

        List<OrderInfo> orderList = convv.toObject(jsonResponse, new TypeReference<List<OrderInfo>>() {
        });

        return new WeakReference<List<OrderInfo>>(orderList);

    }

    public void sendGCMToken(String token) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/registergcmtoken");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        GCMToken tokenReq = new GCMToken();
        tokenReq.setRegisterId(token);
        tokenReq.setRegDate(Calendar.getInstance().getTime());
        tokenReq.setLogin(this.userId.getUsername() + "@" + this.userId.getStore_id());

        StringEntity entity = new StringEntity(this.conv.toJSON(tokenReq));
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }


    }

    public void registerStoreInfo(StoreInfo storeinfo) throws Exception {

        storeinfo.setAddress(null);
        storeinfo.setDescription(null);
        storeinfo.setNomeFantasia(null);

        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/storeinfo");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        StringEntity entity = new StringEntity(this.conv.toJSON(storeinfo));
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        try {
            EntityUtils.consume(response.getEntity());
        } catch (Exception e) {
            MyLogger.log(e);
        }
    }

    public CEPInfo findCEP(String cep) throws Exception {
        cep = cep.replace("-", "");
        cep = cep.replace(".", "");
        cep = cep.replace(" ", "");

        HttpGet get = new HttpGet("https://viacep.com.br/ws/" + cep + "/json/");
        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
            JSONConverter conv = new JSONConverter();
            return conv.toObject(jsonResponse, new TypeReference<CEPInfo>() {
            });
        } else {
            return null;
        }

    }

    public void registerUser(UserInfo userInfo) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/registeruser");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        String jsonRequest = this.conv.toJSON(userInfo);

        StringEntity entity = new StringEntity(jsonRequest);
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
        } else {
            String res = EntityUtils.toString(response.getEntity());
            throw new Exception(res);
        }
    }

    public void addAddress(AddressInfo address) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/addaddress");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        String jsonRequest = this.conv.toJSON(address);

        StringEntity entity = new StringEntity(jsonRequest, "UTF-8");
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
        } else {

        }
    }

    public void removeAddress(AddressInfo address) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/removeaddress");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        String jsonRequest = this.conv.toJSON(address);

        StringEntity entity = new StringEntity(jsonRequest, "UTF-8");
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
        } else {

        }
    }

    public List<AddressInfo> getAddress() throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/getaddress");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);


        String jsonResponse = EntityUtils.toString(response.getEntity(), "ISO-8859-1");

        jsonResponse = new String(jsonResponse.getBytes("ISO-8859-1"), "UTF-8");


        System.out.println(jsonResponse);
        List<AddressInfo> result = this.conv.toObject(jsonResponse, new TypeReference<List<AddressInfo>>() {
        });
        return result;
    }

    public void opConvenio(LocalConvenioInfo convenio, String op) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/convenioinfo/" + op);
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        String jsonRequest = this.conv.toJSON(convenio);

        StringEntity entity = new StringEntity(jsonRequest, "UTF-8");
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
        } else {

        }
    }


    public void opPaymemt(PaymentInfo payment, String op) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/paymentinfo/" + op);
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        String jsonRequest = this.conv.toJSON(payment);

        StringEntity entity = new StringEntity(jsonRequest, "UTF-8");
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
        } else {

        }
    }

    public List<PaymentInfo> getPayments() throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/getcards");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        List<PaymentInfo> result = this.conv.toObject(jsonResponse, new TypeReference<List<PaymentInfo>>() {
        });
        return result;

    }

    public List<LocalConvenioInfo> getConvenios() throws Exception {
        MyGet get = new MyGet(HOST_ENDPOINT + "/users/getconvenios");
        get.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        HttpResponse response = HttpClientBuilder.create().build().execute(get);

        this.checkAutorization(response);

        String jsonResponse = EntityUtils.toString(response.getEntity());

        List<LocalConvenioInfo> result = this.conv.toObject(jsonResponse, new TypeReference<List<LocalConvenioInfo>>() {
        });
        return result;

    }

    public double calculateDistance(AddressInfo adr) throws Exception {
        HttpPost post = new HttpPost(HOST_ENDPOINT + "/users/calculatedistance");
        post.addHeader("Authorization", "Bearer " + this.tokenAutenthication);

        String jsonRequest = this.conv.toJSON(adr);

        StringEntity entity = new StringEntity(jsonRequest, "UTF-8");
        entity.setContentType("application/json");

        post.setEntity(entity);

        HttpResponse response = HttpClientBuilder.create().build().execute(post);

        this.checkAutorization(response);

        if (response.getStatusLine().getStatusCode() == 200) {
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return Double.parseDouble(jsonResponse);
        }
        return -100;
    }
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////

    public static class MyGet extends HttpPost {
        public final static String METHOD_NAME = "GET";

        public MyGet(URI url) {
            super(url);
        }

        public MyGet(String url) {
            super(url);
        }

        @Override
        public String getMethod() {
            return METHOD_NAME;
        }
    }
}
