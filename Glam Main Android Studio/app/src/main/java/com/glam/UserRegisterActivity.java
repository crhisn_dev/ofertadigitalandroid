package com.glam;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.glam.custom.CustomActivity;
import com.glam.dialog.MyDialogs;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.utils.MyLogger;

import java.util.ArrayList;
import java.util.List;

import br.com.innove.ofertadigital.persistence.Address;
import br.com.innove.ofertadigital.persistence.AddressInfo;
import br.com.innove.ofertadigital.persistence.UserId;
import br.com.innove.ofertadigital.persistence.UserInfo;

public class UserRegisterActivity extends CustomActivity {

    private LinearLayout linlaHeaderProgress;
    private EditText email;
    private EditText nameText;
    private EditText passwordText;
    private EditText lgrText;
    private EditText nroText;
    private EditText complText;
    private EditText phoneText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_register_activity);

        this.linlaHeaderProgress = (LinearLayout) this.findViewById(R.id.linlaHeaderProgress);

        this.email = (EditText) this.findViewById(R.id.loginText);
        this.nameText = (EditText) this.findViewById(R.id.nameText);
        this.passwordText = (EditText) this.findViewById(R.id.passwordText2);
        this.lgrText = (EditText) this.findViewById(R.id.lgrText);
        this.nroText = (EditText) this.findViewById(R.id.nroText);
        this.complText = (EditText) this.findViewById(R.id.complText);
        this.phoneText = (EditText) this.findViewById(R.id.phoneText);


        Button btnCancel = (Button) this.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button btnRegister = (Button) this.findViewById(R.id.btnRegister);


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isValidEmail(email.getText())) {
                    MyDialogs.showError(UserRegisterActivity.this, "Cadastro Usuario", "Email invalido");
                    return;
                }

                if (TextUtils.isEmpty(nameText.getText())) {
                    MyDialogs.showError(UserRegisterActivity.this, "Cadastro Usuario", "Nome do usuário requerido");
                    return;
                }

                if (TextUtils.isEmpty(passwordText.getText())) {
                    MyDialogs.showError(UserRegisterActivity.this, "Cadastro Usuario", "Senha do usuário requerido");
                    return;
                }

                if (TextUtils.isEmpty(lgrText.getText())) {
                    MyDialogs.showError(UserRegisterActivity.this, "Cadastro Usuario", "Endereço requerido");
                    return;
                }

                if (TextUtils.isEmpty(nroText.getText())) {
                    MyDialogs.showError(UserRegisterActivity.this, "Cadastro Usuario", "Numero do endereço requerido");
                    return;
                }

                if (TextUtils.isEmpty(phoneText.getText())) {
                    MyDialogs.showError(UserRegisterActivity.this, "Cadastro Usuario", "Telefone do usuário requerido");
                    return;
                }


                final UserInfo user = new UserInfo();
                {
                    UserId userId = new UserId();

                    String email_addr = email.getText().toString();
                    email_addr = email_addr.replace("@","");

                    MyLogger.info("email_addr: " + email_addr);

                    userId.setUsername(email_addr);
                    user.setUserId(userId);
                }

                user.setEmail(email.getText().toString());
                user.setName(nameText.getText().toString());
                user.setPassword(passwordText.getText().toString());
                {
                    AddressInfo addr = new AddressInfo();

                    Address address = new Address();
                    address.setXLgr(lgrText.getText().toString());
                    address.setNro(nroText.getText().toString());
                    address.setXCpl(complText.getText().toString());
                    address.setFone(phoneText.getText().toString());
                    addr.setAddress(address);

                    List<AddressInfo> ll = new ArrayList<AddressInfo>();
                    ll.add(addr);
                    user.setAddresses(ll);
                }

                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                    private Exception e;

                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            OfertaDigitalRestClient.getRef().registerUser(user);
                        } catch (Exception e) {
                            MyLogger.log(e);
                            this.e = e;
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        if (this.e != null) {
                            MyDialogs.showError(UserRegisterActivity.this, "Cadastro Usuario", "Erro no cadastro: " + this.e.getLocalizedMessage());
                        } else {

                            new AlertDialog.Builder(UserRegisterActivity.this, R.style.MyAlertDialogStyleBlue)
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .setTitle("Cadastro Usuario")
                                    .setMessage("Cadastro efetuado com sucesso")
                                    .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }

                                    })
                                    .show();
                        }
                        linlaHeaderProgress.setVisibility(View.GONE);
                    }

                    @Override
                    protected void onPreExecute() {
                        linlaHeaderProgress.setVisibility(View.VISIBLE);
                    }
                };
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });


    }

    public boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void validateText() throws Exception {

    }

    private void cleanText() {
        this.email.setText("");
        this.nameText.setText("");
        this.passwordText.setText("");
        this.lgrText.setText("");
        this.nroText.setText("");
        this.complText.setText("");
        this.phoneText.setText("");
    }
}
