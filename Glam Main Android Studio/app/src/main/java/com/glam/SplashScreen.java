package com.glam;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.glam.http.JSONConverter;
import com.glam.http.OfertaDigitalRestClient;
import com.glam.persistence.OfertaDigitalDataSource;
import com.glam.utils.MyLogger;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Hashtable;

import br.com.innove.ofertadigital.persistence.StoreInfo;
import br.com.innove.ofertadigital.rest.transport.LoginResponseData;

/**
 * The Class SplashScreen will launched at the start of the application. It will
 * be displayed for 3 seconds and than finished automatically and it will also
 * start the next activity of app.
 */
public class SplashScreen extends Activity {

    private OfertaDigitalDataSource dao;

    /**
     * Check if the app is running.
     */
    private boolean isRunning;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        setContentView(R.layout.splash);

        this.dao = new OfertaDigitalDataSource(this);

        isRunning = true;

        startSplash();

    }

    /**
     * Starts the count down timer for 3-seconds. It simply sleeps the thread
     * for 3-seconds.
     */
    private void startSplash() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    Thread.sleep(1500);

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            doFinish();
                        }
                    });
                }
            }
        }).start();
    }

    /**
     * If the app is still running than this method will start the Login
     * activity and finish the Splash.
     */
    private synchronized void doFinish() {


        if (isRunning) {
            isRunning = false;

            if (AccessToken.getCurrentAccessToken() != null) {
                try {

                    // LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email, public_profile"));
                    MyLogger.info("permissions:" + AccessToken.getCurrentAccessToken().getPermissions());
                    MyLogger.info("name: " + Profile.getCurrentProfile().getFirstName());

                    String token = AccessToken.getCurrentAccessToken().getToken();

                    GraphRequest request = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(
                                        JSONObject object,
                                        GraphResponse response) {

                                    try {
                                        // Application code
                                        MyLogger.info("json: " + object);
                                        MyLogger.info("response: " + response.getRawResponse());
                                        MyLogger.info("permissions: " + AccessToken.getCurrentAccessToken().getPermissions());

                                        final String username = (object.isNull("email") ? object.getString("id") : object.getString("email"));

                                        final String id = object.getString("id");
                                        final String store_id = "serradalto";
                                        final Hashtable<String, String> fbInfo = new Hashtable<String, String>();
                                        fbInfo.put("object", object.toString());
                                        fbInfo.put("name", Profile.getCurrentProfile().getName());

                                        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                                            @Override
                                            protected Void doInBackground(Void... voids) {
                                                try {
                                                    OfertaDigitalRestClient.getRef().autenthicateWithFB(username, store_id, AccessToken.getCurrentAccessToken().getToken(), fbInfo);
                                                    StoreInfo storeInfo = dao.loadObject("storeinfo", StoreInfo.class);

                                                    if (storeInfo == null) {
                                                        Intent i = new Intent(SplashScreen.this, StoreSelectActivity.class);
                                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        startActivity(i);
                                                        finish();
                                                    } else {
                                                        Intent i = new Intent(SplashScreen.this, MainActivity.class);
                                                        i.putExtra("storeinfo", storeInfo);
                                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        startActivity(i);
                                                        finish();
                                                    }

                                                    return null;
                                                } catch (Exception e) {
                                                    MyLogger.log(e);
                                                }

                                                return null;
                                            }
                                        };
                                        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                                    } catch (Exception e) {
                                        MyLogger.log(e);
                                    }
                                }

                            });

                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "email");
                    request.setParameters(parameters);
                    request.executeAsync();

                } catch (Exception e) {
                    MyLogger.log(e);
                }

            } else {
                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            LoginResponseData login = dao.loadObject("loginfo", LoginResponseData.class);
                            String password = dao.loadObject("pwdinfo", String.class);
                            StoreInfo storeInfo = dao.loadObject("storeinfo", StoreInfo.class);


                            if (login != null && password != null) {
                                LoginResponseData loginresponse = OfertaDigitalRestClient.getRef().autenthicate(login.getUsername(), password, "serradalto", null);

                                if (dao.existRegisterWithId("loginfo")) {
                                    dao.updateObject("loginfo",loginresponse);
                                } else {
                                    dao.persitObject("loginfo", loginresponse);
                                }

                            /*Intent i = new Intent(SplashScreen.this, MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();*/

                                MyLogger.info("storeInfo: " + storeInfo);

                                if (storeInfo == null) {
                                    Intent i = new Intent(SplashScreen.this, StoreSelectActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                                    i.putExtra("storeinfo", storeInfo);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                    finish();
                                }

                                return null;
                            }
                        } catch (Exception e) {
                            MyLogger.log(e);
                        }

                        // goto login
                        Intent i = new Intent(SplashScreen.this, Login.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                        return null;
                    }
                };
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            isRunning = false;
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        MyLogger.info("onDestroy: " + this);
        super.onDestroy();

        try {
            this.dao.close();
            this.dao = null;
        } catch (Exception e) {
            MyLogger.log(e);
        }


    }
}